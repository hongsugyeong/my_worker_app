import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/business/manual/business_manual_regist_request.dart';
import 'package:my_worker_app/pages/business_management/business_manual_manage/page_business_manual_list.dart';
import 'package:my_worker_app/repository/business/repo_business_manual.dart';

class RealTest extends StatefulWidget {
  const RealTest({super.key});

  @override
  State<RealTest> createState() => _RealTestState();
}

class _RealTestState extends State<RealTest> {
  final _formKey = GlobalKey<FormBuilderState>();
  File? _image;
  final ImagePicker _picker = ImagePicker();
  final Dio _dio = Dio();

  // 갤러리에서 이미지 선택
  Future<void> _pickImage() async {
    final XFile? pickedFile =
        await _picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });
    }
  }

  // 이미지 선택 후 서버에 업로드
  Future<void> _uploadImage() async {
    if (_image != null) {
      String fileName = _image!.path.split('/').last;
      FormData formData = FormData.fromMap({
        "file": await MultipartFile.fromFile(_image!.path, filename: fileName),
      });

      try {
        var response = await _dio
            .post('http://versuss.store:8080/v1/manual/new/', data: formData);
        if (response.statusCode == 200) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text('성공')));
        } else {
          throw Exception(Text('실패'));
        }
      } catch (e) {
        print(e);
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text('실패')));
      }
    }
  }

  // 원래 있던 함수
  Future<void> sendManual(BusinessManualRegistRequest request) async {
    await RepoBusinessManual()
        .setManual(request)
        // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      request.title = '';
      request.content = '';
      request.multipartFile = '';
      print("성공");
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const PageBusinessManualList()));
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          '매뉴얼 작성',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: [
          Container(
            child: Image.asset(
              'assets/img/logo_2.png',
              width: 50,
              height: 50,
            ),
            margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
          ),
        ],
        backgroundColor: Color.fromRGBO(17, 45, 78, 1.0),
      ),
      body: _content()
    );
  }

  Widget _content() {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              child: Column(
                children: [
                  Container(
                    child: FormBuilderTextField(
                      name: 'title',
                      maxLines: null,
                      maxLength: 30,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: '제목을 입력해 주세요.',
                        counterText: '',
                      ),
                    ),
                    margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                    height: 50,
                  ),
                  Divider(
                    color: colorLight,
                  ),
                  Container(
                    child: FormBuilderTextField(
                      name: 'content',
                      maxLines: null,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: '내용을 입력해 주세요',
                      ),
                    ),
                    margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                    height: 400,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _image != null ? Image.file(_image!) : Text('이미지 선택해주세요.'),
                      ElevatedButton(onPressed: _pickImage, child: Text('이미지 선택')),
                      ElevatedButton(onPressed: _uploadImage, child: Text('이미지 업로드')),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.saveAndValidate()) {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text(
                                  '등록하시겠습니까?',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0)),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text(
                                      '취소',
                                      style: TextStyle(
                                          color: Color.fromRGBO(
                                              63, 114, 175, 1.0)),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      BusinessManualRegistRequest request =
                                          BusinessManualRegistRequest(
                                        _formKey.currentState!.fields['title']!.value,
                                        _formKey.currentState!.fields['content']!.value,
                                              _image.toString()
                                      );
                                      sendManual(request);
                                    },
                                    child: const Text('확인',
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                17, 45, 78, 1.0))),
                                  ),
                                ],
                                backgroundColor: Colors.white,
                              ),
                            );
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            onPrimary: Color.fromRGBO(17, 45, 78, 1.0),
                            side: BorderSide(
                                color: Color.fromRGBO(17, 45, 78, 1.0),
                                width: 2.0)),
                        child: const Text('등록'),
                      ),
                    ),
                    margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      onPressed: () {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              '취소하시겠습니까?',
                              style: TextStyle(
                                  color: Color.fromRGBO(64, 64, 64, 1.0)),
                            ),
                            // content: const Text('AlertDialog description'),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () =>
                                    Navigator.pop(context, 'Cancel'),
                                child: const Text(
                                  '취소',
                                  style: TextStyle(color: colorNormal),
                                ),
                              ),
                              TextButton(
                                onPressed: () {},
                                child: const Text('확인',
                                    style: TextStyle(
                                        color:
                                            Color.fromRGBO(17, 45, 78, 1.0))),
                              ),
                            ],
                            backgroundColor: Colors.white,
                          ),
                        );
                      },
                      style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          onPrimary: colorNormal,
                          side: BorderSide(color: colorNormal, width: 2.0)),
                      child: const Text('취소'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
