import 'package:flutter/material.dart';
import 'package:my_worker_app/pages/business_management/PT_contract_manage/page_parttimer_contract_detail.dart';
import 'package:my_worker_app/pages/business_management/PT_contract_manage/page_parttimer_contract_list.dart';
import 'package:my_worker_app/pages/business_management/PT_contract_manage/page_parttimer_contract_regist.dart';
import 'package:my_worker_app/pages/business_management/PT_info_manage/page_parttimer_info_detail.dart';
import 'package:my_worker_app/pages/business_management/PT_info_manage/page_parttimer_info_list.dart';
import 'package:my_worker_app/pages/business_management/PT_join_manage/page_parttimer_join.dart';
import 'package:my_worker_app/pages/business_management/PT_pay_manage/page_parttimer_pay_detail.dart';
import 'package:my_worker_app/pages/business_management/PT_pay_manage/page_parttimer_pay_list.dart';
import 'package:my_worker_app/pages/business_management/PT_schedule_manage/page_parttimer_schedule_list.dart';
import 'package:my_worker_app/pages/business_management/PT_info_manage/page_parttimer_info_schedule_regist.dart';
import 'package:my_worker_app/pages/business_management/business_manage/page_business_add.dart';
import 'package:my_worker_app/pages/business_management/business_manage/page_business_detail.dart';
import 'package:my_worker_app/pages/business_management/business_manual_manage/page_business_manual_detail.dart';
import 'package:my_worker_app/pages/business_management/business_manual_manage/page_business_manual_list.dart';
import 'package:my_worker_app/pages/business_management/business_manual_manage/page_business_manual_regist.dart';
import 'package:my_worker_app/pages/business_management/business_product_manage/page_business_product_list.dart';
import 'package:my_worker_app/pages/business_management/business_transition_manage/page_business_transition_list.dart';
import 'package:my_worker_app/pages/business_management/page_business_menu_list.dart';
import 'package:my_worker_app/pages/join/page_join1.dart';
import 'package:my_worker_app/pages/join/page_join.dart';
import 'package:my_worker_app/pages/login/page_login.dart';
import 'package:my_worker_app/pages/login/page_login.dart';
import 'package:my_worker_app/pages/main_page/ask/page_ask_detail.dart';
import 'package:my_worker_app/pages/main_page/ask/page_ask_list.dart';
import 'package:my_worker_app/pages/main_page/ask/page_ask_regist.dart';
import 'package:my_worker_app/pages/main_page/board/board_one/page_board_one_regist.dart';
import 'package:my_worker_app/pages/main_page/board/board_together/page_board_regist.dart';
import 'package:my_worker_app/pages/main_page/notice/page_notice_detail.dart';
import 'package:my_worker_app/pages/main_page/notice/page_notice_list.dart';
import 'package:my_worker_app/pages/page_index.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/pages/test/test.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Color.fromRGBO(255, 255, 255, 1.0)),
        useMaterial3: true,
        fontFamily: 'Pretendard',
      ),
      home: PageLogin(),
    );
  }
}
