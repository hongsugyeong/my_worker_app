import 'package:my_worker_app/model/home/info/my_info.dart';

class MyInfoResult {
  String msg;
  num code;
  MyInfo data;

  MyInfoResult(
      this.msg,
      this.code,
      this.data
      );

  factory MyInfoResult.fromJson(Map<String, dynamic> json) {
    return MyInfoResult(
        json['msg'],
        json['code'],
        MyInfo.fromJson(json['data'])
    );
  }
}