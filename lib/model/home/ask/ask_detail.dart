import 'package:my_worker_app/model/home/ask/ask_answer_comment.dart';

class AskDetail {
  num id;
  String title; // 문의 제목
  String content; // 문의 내용
  String questionImgUrl; // 문의 첨부 이미지
  String dateMemberAsk; // 문의 등록 날짜 시간

  AskAnswerComment? comment;

  AskDetail (
      this.id,
      this.title,
      this.content,
      this.questionImgUrl,
      this.dateMemberAsk,
      this.comment,
      );

  factory AskDetail.fromJson(Map<String, dynamic> json) {
    return AskDetail(
      json['id'],
      json['title'],
      json['content'],
      json['questionImgUrl'],
      json['dateMemberAsk'],
      AskAnswerComment.fromJson(json['comment']),
    );
  }
}