class AskAnswerComment {
  String? answerTitle;
  String? answerContent;
  String answerImgUrl;
  String? dateAnswer;

  AskAnswerComment(this.answerTitle, this.answerContent, this.answerImgUrl,
      this.dateAnswer);

  factory AskAnswerComment.fromJson(Map<String, dynamic> json) {
    return AskAnswerComment(
      json['answerTitle'],
      json['answerContent'],
      json['answerImgUrl'],
      json['dateAnswer'],
    );
  }
}