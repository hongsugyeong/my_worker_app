class BusinessItem {
  String name;
  String ownerName;
  String businessType;
  String phoneNumber;
  String address;

  BusinessItem(
    this.name,
    this.ownerName,
    this.businessType,
    this.phoneNumber,
    this.address,
  );

  factory BusinessItem.fromJson(Map<String, dynamic> json) {
    return BusinessItem(
      json['name'],
      json['ownerName'],
      json['businessType'],
      json['phoneNumber'],
      json['address'],
    );
  }
}
