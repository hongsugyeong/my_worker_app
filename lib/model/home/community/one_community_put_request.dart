class OneCommunityPutRequest {
  num id;
  String? title;
  String? content;
  String? boardImgUrl;

  OneCommunityPutRequest(this.id, this.title, this.content, this.boardImgUrl);

  // 등록이므로 toJson으로 하기
  Map<String, dynamic> toJson() {

    // data에 값 넣어주기
    Map<String, dynamic> data = Map<String, dynamic>();

    // 어차피 해당 지역에 같이 있어서 this 생략 가능
    data['title'] = title;
    data['content'] = content;
    data['boardImgUrl'] = boardImgUrl;

    return data;
  }
}