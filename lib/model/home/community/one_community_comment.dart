class OneCommunityComment {
  String content;
  String dateComment;

  OneCommunityComment(
      this.content,
      this.dateComment
      );

  factory OneCommunityComment.fromJson(Map<String, dynamic> json) {
    return OneCommunityComment(
      json['content'],
      json['dateComment'],
    );
  }
}