class NoticeDetail {
  String title;
  String dateNotice;
  String content;
  String noticeImgUrl;
  
  NoticeDetail (
      this.dateNotice,
      this.title,
      this.noticeImgUrl,
      this.content
      );
  
  factory NoticeDetail.fromJson(Map<String, dynamic> json) {
    return NoticeDetail(
      json['dateNotice'],
      json['title'],
      json['noticeImgUrl'],
      json['content'],
    );
  }
}