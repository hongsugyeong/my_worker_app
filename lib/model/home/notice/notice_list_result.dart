import 'package:my_worker_app/model/home/notice/notice_list.dart';

class NoticeListResult{
  num totalCount;
  num totalPage;
  num currentPage;
  List<NoticeList>? list;

  NoticeListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory NoticeListResult.fromJson(Map<String, dynamic> json) {
    return NoticeListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? (json['list'] as List).map((e) => NoticeList.fromJson(e)).toList() : [],
    );
  }
}