import 'package:my_worker_app/model/home/notice/notice_detail.dart';

class NoticeDetailResult {
  String msg;
  num code;
  NoticeDetail data;

  NoticeDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory NoticeDetailResult.fromJson(Map<String, dynamic> json) {
    return NoticeDetailResult(
        json['msg'],
        json['code'],
        NoticeDetail.fromJson(json['data'])
    );
  }
}