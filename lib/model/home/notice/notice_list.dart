class NoticeList {
  num id;
  String dateNotice;
  String title;
  // String noticeImgUrl;

  NoticeList(
      this.id,
      this.title,
      this.dateNotice,
      // this.noticeImgUrl
      );

  factory NoticeList.fromJson(Map<String, dynamic> json) {
    return NoticeList(
      json['id'],
      json['title'],
      json['dateNotice'],
      // json['noticeImgUrl'],
    );
  }
}