class JoinRequest {
  String? memberType;
  String? name;
  String? username;
  String? password;
  String? passwordRe;
  String? isMan;
  String? dateBirth;
  String? phoneNumber;
  String? address;

  JoinRequest({this.memberType, this.name, this.username, this.password, this.passwordRe, this.isMan, this.dateBirth, this.phoneNumber, this.address});

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['memberType'] = memberType;
    data['name'] = name;
    data['username'] = username;
    data['password'] = password;
    data['passwordRe'] = passwordRe;
    data['isMan'] = isMan;
    data['dateBirth'] = dateBirth;
    data['phoneNumber'] = phoneNumber;
    data['address'] = address;

    return data;
  }
}