class BusinessTransitionDetail {
  String content;

  BusinessTransitionDetail(this.content);

  factory BusinessTransitionDetail.fromJson(Map<String, dynamic> json) {
    return BusinessTransitionDetail(
      json['content']
    );
  }
}