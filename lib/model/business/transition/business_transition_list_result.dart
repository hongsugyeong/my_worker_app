import 'package:my_worker_app/model/business/transition/business_transition_list.dart';

class BusinessTransitionListResult {
  String msg;
  num code;
  List<BusinessTransitionList>? list;

  BusinessTransitionListResult(
      this.msg,
      this.code,
      this.list
      );

  factory BusinessTransitionListResult.fromJson(Map<String, dynamic> json) {
    return BusinessTransitionListResult(
        json['msg'],
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => BusinessTransitionList.fromJson(e)).toList() : [],
    );
  }
}