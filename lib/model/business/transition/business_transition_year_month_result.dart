import 'package:my_worker_app/model/business/transition/business_transition_year_month.dart';

class BusinessTransitionYearMonthResult {
  String msg;
  num code;
  List<BusinessTransitionYearMonth>? list;

  BusinessTransitionYearMonthResult(
      this.msg,
      this.code,
      this.list
      );

  factory BusinessTransitionYearMonthResult.fromJson(Map<String, dynamic> json) {
    return BusinessTransitionYearMonthResult(
        json['msg'],
        json['code'],
        json['list'] != null ? (json['list'] as List).map((e) => BusinessTransitionYearMonth.fromJson(e)).toList() : [],
    );
  }
}