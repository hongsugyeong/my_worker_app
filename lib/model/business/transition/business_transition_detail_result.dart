import 'package:my_worker_app/model/business/transition/business_transition_detail.dart';

class BusinessTransitionDetailResult {
  String msg;
  num code;
  BusinessTransitionDetail data;

  BusinessTransitionDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory BusinessTransitionDetailResult.fromJson(Map<String, dynamic> json) {
    return BusinessTransitionDetailResult(
        json['msg'],
        json['code'],
        BusinessTransitionDetail.fromJson(json['data'])
    );
  }
}