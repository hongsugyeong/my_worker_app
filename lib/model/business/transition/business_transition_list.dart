class BusinessTransitionList {
  num id;
  String memberName;
  String dateTransition;
  String content;
  String? isFinish;

  BusinessTransitionList(
      this.id,
      this.memberName,
      this.dateTransition,
      this.content,
      this.isFinish
  );

  factory BusinessTransitionList.fromJson(Map<String, dynamic> json) {
    return BusinessTransitionList(
      json['id'],
      json['memberName'],
      json['dateTransition'],
      json['content'],
      json['isFinish'],
    );
  }
}