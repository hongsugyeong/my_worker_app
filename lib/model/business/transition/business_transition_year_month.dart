class BusinessTransitionYearMonth {
  int year;
  int month;

  BusinessTransitionYearMonth (this.year, this.month);

  factory BusinessTransitionYearMonth.fromJson(Map<String, dynamic> json) {
    return BusinessTransitionYearMonth(
      json['year'],
      json['month']
    );
  }
}