class ParttimerContractList {
  num id;
  String memberName;
  String position;
  String dateIn;

  ParttimerContractList(
      this.id,
      this.memberName,
      this.position,
      this.dateIn,
  );

  factory ParttimerContractList.fromJson(Map<String, dynamic> json) {
    return ParttimerContractList(
      json['id'],
      json['memberName'],
      json['position'],
      json['dateIn'],
    );
  }
}