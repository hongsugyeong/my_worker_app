import 'package:my_worker_app/model/business/parttimer/contract/parttimer_contract_list.dart';

class ParttimerContractListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<ParttimerContractList>? list;

  ParttimerContractListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory ParttimerContractListResult.fromJson(Map<String, dynamic> json) {
    return ParttimerContractListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? (json['list'] as List).map((e) => ParttimerContractList.fromJson(e)).toList() : [],
    );
  }
}