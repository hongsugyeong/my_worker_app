import 'package:my_worker_app/model/business/parttimer/contract/parttimer_contract_detail.dart';

class ParttimerContractDetailResult {
  String msg;
  num code;
  ParttimerContractDetail data;

  ParttimerContractDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory ParttimerContractDetailResult.fromJson(Map<String, dynamic> json) {
    return ParttimerContractDetailResult(
        json['msg'],
        json['code'],
        ParttimerContractDetail.fromJson(json['data'])
    );
  }
}