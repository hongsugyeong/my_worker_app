class ParttimerContractDetail {
  String businessMemberName; // 이름
  String dateContract; // 계약한 날짜
  String payType; // 월급 유형 선택
  num payPrice; // 얼마 주냐
  String dateWorkStart; // 근무 시작 날짜
  String dateWorkEnd; // 근무 종료 날짜
  num restTime; // 휴식 시간
  String insurance; // 4대보험 선택
  String workDay; // 언제 일할 요일
  num workTime; // 총 근무 시간
  num weekWorkTime; // 총 주 근무 시간
  String isWeekPay; // 주휴수당 여부
  num mealPay; // 식대
  String wagePayment; // 임금 지급날짜
  String wageAccountNumber; // 계좌번호
  String contractCopyImgUrl; // 계약서 사진


  ParttimerContractDetail(
      this.businessMemberName,
      this.dateContract,
      this.payType,
      this.payPrice,
      this.dateWorkStart,
      this.dateWorkEnd,
      this.restTime,
      this.insurance,
      this.workDay,
      this.workTime,
      this.weekWorkTime,
      this.isWeekPay,
      this.mealPay,
      this.wagePayment,
      this.wageAccountNumber,
      this.contractCopyImgUrl
  );

  factory ParttimerContractDetail.fromJson(Map<String, dynamic> json) {
    return ParttimerContractDetail(
      json['businessMemberName'],
      json['dateContract'],
      json['payType'],
      json['payPrice'],
      json['dateWorkStart'],
      json['dateWorkEnd'],
      json['restTime'],
      json['insurance'],
      json['workDay'],
      json['workTime'],
      json['weekWorkTime'],
      json['isWeekPay'],
      json['mealPay'],
      json['wagePayment'],
      json['wageAccountNumber'],
      json['contractCopyImgUrl'],
    );
  }
}