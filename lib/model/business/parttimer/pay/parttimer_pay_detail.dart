class ParttimerPayDetail {
  String name;
  String dateStartWork;
  String dateEndWork;
  String payType;
  int? payBasic;
  int? taxAfterPay;
  int? weekRestPay;
  String timeNightWork;
  int? plusPay;
  int? minusPay;
  int? taxBeforePay;
  int? taxPrice;

  ParttimerPayDetail (
      this.name,
      this.dateStartWork,
      this.dateEndWork,
      this.payType,
      this.payBasic,
      this.taxAfterPay,
      this.weekRestPay,
      this.timeNightWork,
      this.plusPay,
      this.minusPay,
      this.taxBeforePay,
      this.taxPrice,
  );

  factory ParttimerPayDetail.fromJson(Map<String, dynamic> json) {
    return ParttimerPayDetail(
      json['name'],
      json['dateStartWork'],
      json['dateEndWork'],
      json['payType'],
      json['payBasic'],
      json['taxAfterPay'],
      json['weekRestPay'],
      json['timeNightWork'],
      json['plusPay'],
      json['minusPay'],
      json['taxBeforePay'],
      json['taxPrice'],
    );
  }
}