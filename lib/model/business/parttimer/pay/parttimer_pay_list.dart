class ParttimerPayList {
  String name;
  String payType;
  String taxAfterPay;

  ParttimerPayList (
      this.name,
      this.payType,
      this.taxAfterPay
      );

  factory ParttimerPayList.fromJson(Map<String, dynamic> json) {
    return ParttimerPayList(
      json['name'],
      json['payType'],
      json['taxAfterPay'],
    );
  }
}