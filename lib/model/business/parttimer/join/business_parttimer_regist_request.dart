class BusinessParttimerRegistRequest {
  String phoneNumber;
  String position;
  String? etc;

  BusinessParttimerRegistRequest(this.phoneNumber, this.position, this.etc);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['phoneNumber'] = phoneNumber;
    data['position'] = position;
    data['etc'] = etc;

    return data;
  }
}