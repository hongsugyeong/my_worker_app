import 'package:my_worker_app/model/business/parttimer/schedule/parttimer_schedule_list.dart';

class ParttimerScheduleListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<ParttimerScheduleList>? list;

  ParttimerScheduleListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory ParttimerScheduleListResult.fromJson(Map<String, dynamic> json) {
    return ParttimerScheduleListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? (json['list'] as List).map((e) => ParttimerScheduleList.fromJson(e)).toList() : [],
    );
  }
}