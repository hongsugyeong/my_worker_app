class ParttimerScheduleList {
  num id;
  String businessMemberName;
  String businessMemberPosition;
  String startWork;
  String endWork;

  ParttimerScheduleList (
      this.id,
      this.businessMemberName,
      this.businessMemberPosition,
      this.startWork,
      this.endWork
  );

  factory ParttimerScheduleList.fromJson(Map<String, dynamic> json) {
    return ParttimerScheduleList(
      json['id'],
      json['businessMemberName'],
      json['businessMemberPosition'],
      json['startWork'],
      json['endWork'],
    );
  }

}