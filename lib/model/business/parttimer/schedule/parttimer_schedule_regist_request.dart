class ParttimerScheduleRegistRequest {
  String weekSchedule; // 근무 요일
  String timeScheduleStart; // 근무 시작 시간
  String timeScheduleEnd; // 근무 퇴근 시간
  String timeScheduleTotal; // 총 근무 시간

  ParttimerScheduleRegistRequest(
      this.weekSchedule,
      this.timeScheduleStart,
      this.timeScheduleEnd,
      this.timeScheduleTotal
      );

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['weekSchedule'] = weekSchedule;
    data['timeScheduleStart'] = timeScheduleStart;
    data['timeScheduleEnd'] = timeScheduleEnd;
    data['timeScheduleTotal'] = timeScheduleTotal;

    return data;
  }
}