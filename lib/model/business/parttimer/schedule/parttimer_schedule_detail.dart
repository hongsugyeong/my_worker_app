class ParttimerScheduleDetail {
  String name;
  String startWork;
  String endWork;
  String recess;
  String plusWorkTime;
  String outWorkTime;
  String totalWorkTime;
  String isLateness;
  String isOverTime;
  String etc;

  ParttimerScheduleDetail (
      this.name,
      this.startWork,
      this.endWork,
      this.recess,
      this.plusWorkTime,
      this.outWorkTime,
      this.totalWorkTime,
      this.isLateness,
      this.isOverTime,
      this.etc
  );

  factory ParttimerScheduleDetail.fromJson(Map<String, dynamic> json) {
    return ParttimerScheduleDetail(
      json['name'],
      json['startWork'],
      json['endWork'],
      json['recess'],
      json['plusWorkTime'],
      json['outWorkTime'],
      json['totalWorkTime'],
      json['isLateness'],
      json['isOverTime'],
      json['etc'],
    );
  }

}