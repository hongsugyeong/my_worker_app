import 'package:my_worker_app/model/business/parttimer/info/parttimer_info_detail.dart';

class ParttimerInfoDetailResult {
  String msg;
  num code;
  ParttimerInfoDetail? data;

  ParttimerInfoDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory ParttimerInfoDetailResult.fromJson(Map<String, dynamic> json) {
    return ParttimerInfoDetailResult(
      json['msg'],
      json['code'],
      ParttimerInfoDetail.fromJson(json['data'])
    );
  }
}