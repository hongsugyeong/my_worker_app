import 'package:my_worker_app/model/business/parttimer/info/parttimer_info_list.dart';

class ParttimerInfoListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<ParttimerInfoList>? list;

  ParttimerInfoListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory ParttimerInfoListResult.fromJson(Map<String, dynamic> json) {
    return ParttimerInfoListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? (json['list'] as List).map((e) => ParttimerInfoList.fromJson(e)).toList() : [],
    );
  }
}