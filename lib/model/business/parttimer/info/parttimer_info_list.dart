class ParttimerInfoList {
  num id;
  String memberName;
  String position;
  String isWork;
  String? weekSchedule;

  ParttimerInfoList(
      this.id,
      this.memberName,
      this.position,
      this.isWork,
      this.weekSchedule
  );

  factory ParttimerInfoList.fromJson(Map<String, dynamic> json) {
    return ParttimerInfoList(
      json['id'],
      json['memberName'],
      json['position'],
      json['isWork'],
      json['weekSchedule'],
    );
  }
}