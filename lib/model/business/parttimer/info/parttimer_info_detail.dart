import 'package:my_worker_app/model/business/parttimer/info/parttimer_week_schedule_list.dart';

class ParttimerInfoDetail {
  num id;
  String memberName;
  String position;
  String isWork;
  String memberDateBirth;
  String memberPhoneNumber;
  ParttimerWeekScheduleList? weekScheduleList;
  String dateIn;
  String? dateOut;
  String? etc;


  ParttimerInfoDetail(
      this.id,
      this.memberName,
      this.position,
      this.isWork,
      this.memberDateBirth,
      this.memberPhoneNumber,
      this.weekScheduleList,
      this.dateIn,
      this.dateOut,
      this.etc

      );

  factory ParttimerInfoDetail.fromJson(Map<String, dynamic> json) {
    return ParttimerInfoDetail(
      json['id'],
      json['memberName'],
      json['position'],
      json['isWork'],
      json['memberDateBirth'],
      json['memberPhoneNumber'],
      ParttimerWeekScheduleList.fromJson(json['weekScheduleList']),
      json['dateIn'],
      json['dateOut'],
      json['etc'],
    );
  }

}