class ParttimerWeekScheduleList {
  String? workingHour;

  ParttimerWeekScheduleList(this.workingHour);

  factory ParttimerWeekScheduleList.fromJson(Map<String, dynamic> json) {
    return ParttimerWeekScheduleList(
      json['workingHour']
    );
  }
}