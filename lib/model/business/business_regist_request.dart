class BusinessRegistRequest {
  String businessName;
  String businessNumber;
  String ownerName;
  String? businessImgUrl;
  String businessType;
  String businessLocation;
  String businessEmail;
  String businessPhoneNumber;
  String reallyLocation;

  BusinessRegistRequest(
      this.businessName,
      this.businessNumber,
      this.ownerName,
      this.businessImgUrl,
      this.businessType,
      this.businessLocation,
      this.businessEmail,
      this.businessPhoneNumber,
      this.reallyLocation,
      );

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['businessName'] = businessName;
    data['businessNumber'] = businessNumber;
    data['ownerName'] = ownerName;
    data['businessImgUrl'] = businessImgUrl;
    data['businessType'] = businessType;
    data['businessLocation'] = businessLocation;
    data['businessEmail'] = businessEmail;
    data['businessPhoneNumber'] = businessPhoneNumber;
    data['reallyLocation'] = reallyLocation;

    return data;
  }
}