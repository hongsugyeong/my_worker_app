import 'package:my_worker_app/model/business/product/business_product_list.dart';

class BusinessProductListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<BusinessProductList>? list;

  BusinessProductListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory BusinessProductListResult.fromJson(Map<String, dynamic> json) {
    return BusinessProductListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? (json['list'] as List).map((e) => BusinessProductList.fromJson(e)).toList() : [],
    );
  }
}