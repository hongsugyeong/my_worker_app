class BusinessProductList {
  num id;
  String memberName;
  String dateProduct;
  String productName;
  int? minQuantity;
  int? nowQuantity;

  BusinessProductList (
      this.id,
      this.memberName,
      this.dateProduct,
      this.productName,
      this.minQuantity,
      this.nowQuantity
  );

  factory BusinessProductList.fromJson(Map<String, dynamic> json) {
    return BusinessProductList(
      json['id'],
      json['memberName'],
      json['dateProduct'],
      json['productName'],
      json['minQuantity'],
      json['nowQuantity'],
    );
  }
}