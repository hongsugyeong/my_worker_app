import 'package:my_worker_app/model/business/product/business_product_detail.dart';

class BusinessProductDetailResult {
  String msg;
  num code;
  BusinessProductDetail data;

  BusinessProductDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory BusinessProductDetailResult.fromJson(Map<String, dynamic> json) {
    return BusinessProductDetailResult(
        json['msg'],
        json['code'],
        BusinessProductDetail.fromJson(json['data'])
    );
  }
}