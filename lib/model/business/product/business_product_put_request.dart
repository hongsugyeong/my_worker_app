class BusinessProductPutRequest {
  int? nowQuantity;

  BusinessProductPutRequest(this.nowQuantity);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['nowQuantity'] = nowQuantity;

    return data;
  }
}