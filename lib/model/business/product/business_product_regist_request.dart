class BusinessProductRegistRequest {
  String productName;
  int minQuantity;
  int nowQuantity;

  BusinessProductRegistRequest(this.productName, this.minQuantity, this.nowQuantity);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['productName'] = productName;
    data['minQuantity'] = minQuantity;
    data['nowQuantity'] = nowQuantity;

    return data;
  }
}