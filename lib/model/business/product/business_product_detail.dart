class BusinessProductDetail {
  num id;
  String productName;
  int minQuantity;
  int nowQuantity;

  BusinessProductDetail (
      this.id,
      this.productName,
      this.minQuantity,
      this.nowQuantity
      );

  factory BusinessProductDetail.fromJson (Map<String, dynamic> json) {
    return BusinessProductDetail(
      json['id'],
      json['productName'],
      json['minQuantity'],
      json['nowQuantity'],
    );
  }
}