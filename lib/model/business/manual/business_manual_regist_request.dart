class BusinessManualRegistRequest {
  String title;
  String content;
  String multipartFile;

  BusinessManualRegistRequest(this.title, this.content, this.multipartFile);

  Object? toJson() {
    Map<String, dynamic> data = Map<String, dynamic>();

    data['title'] = title;
    data['content'] = content;
    data['multipartFile'] = multipartFile;
    return data;
  }
}