class BusinessManualDetail {
  num id;
  String memberName;
  String dateManual;
  String title;
  String content;
  String manualImgUrl;

  BusinessManualDetail (
      this.id,
      this.memberName,
      this.dateManual,
      this.title,
      this.content,
      this.manualImgUrl
  );

  factory BusinessManualDetail.fromJson (Map<String, dynamic> json) {
    return BusinessManualDetail(
      json['id'],
      json['memberName'],
      json['dateManual'],
      json['title'],
      json['content'],
      json['manualImgUrl'],
    );
  }
}