import 'package:my_worker_app/model/business/manual/business_manual_detail.dart';

class BusinessManualDetailResult {
  String msg;
  num code;
  BusinessManualDetail data;

  BusinessManualDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory BusinessManualDetailResult.fromJson(Map<String, dynamic> json) {
    return BusinessManualDetailResult(
        json['msg'],
        json['code'],
        BusinessManualDetail.fromJson(json['data'])
    );
  }
}