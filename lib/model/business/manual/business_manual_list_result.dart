import 'package:my_worker_app/model/business/manual/business_manual_list.dart';

class BusinessManualListResult {
  num totalCount;
  num totalPage;
  num currentPage;
  List<BusinessManualList>? list;

  BusinessManualListResult(this.totalCount, this.totalPage, this.currentPage, this.list);

  factory BusinessManualListResult.fromJson(Map<String, dynamic> json) {
    return BusinessManualListResult(
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
      json['list'] != null ? (json['list'] as List).map((e) => BusinessManualList.fromJson(e)).toList() : [],
    );
  }
}