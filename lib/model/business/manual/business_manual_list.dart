class BusinessManualList {
  num id;
  String memberName;
  String title;
  String dateManual;

  BusinessManualList (
      this.id,
      this.memberName,
      this.title,
      this.dateManual,
  );

  factory BusinessManualList.fromJson(Map<String, dynamic> json) {
    return BusinessManualList(
      json['id'],
      json['memberName'],
      json['title'],
      json['dateManual'],
    );
  }
}