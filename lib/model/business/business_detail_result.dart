import 'package:my_worker_app/model/business/business_detail.dart';

class BusinessDetailResult {
  String msg;
  num code;
  BusinessDetail data;

  BusinessDetailResult(
      this.msg,
      this.code,
      this.data
      );

  factory BusinessDetailResult.fromJson(Map<String, dynamic> json) {
    return BusinessDetailResult(
        json['msg'],
        json['code'],
        BusinessDetail.fromJson(json['data'])
    );
  }
}