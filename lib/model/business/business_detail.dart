class BusinessDetail {

  String businessName; // 상호명
  String businessNumber; // 사업자 번호
  String ownerName; // 대표자명
  String businessImgUrl; // 사업장 등록증
  String businessType; // 업종
  String businessLocation; // 소재지
  String reallyLocation;
  String businessEmail; // 이메일
  String businessPhoneNumber; // 전화번호
  String isActivity; // 사업장 영업 여부
  String dateApprovalBusiness; // 가입 승인일
  String isApprovalBusiness; // 사업장 승인 여부

  BusinessDetail(
    this.businessName,
    this.businessNumber,
    this.ownerName,
    this.businessType,
    this.businessLocation,
    this.reallyLocation,
    this.businessEmail,
    this.businessPhoneNumber,
    this.isActivity,
    this.dateApprovalBusiness,
    this.isApprovalBusiness,
    this.businessImgUrl,
  );

  factory BusinessDetail.fromJson(Map<String, dynamic> json) {
    return BusinessDetail(
      json['businessName'],
      json['businessNumber'],
      json['ownerName'],
      json['businessType'],
      json['businessLocation'],
      json['reallyLocation'],
      json['businessEmail'],
      json['businessPhoneNumber'],
      json['isActivity'],
      json['dateApprovalBusiness'],
      json['isApprovalBusiness'],
      json['businessImgUrl'],
    );
  }

}