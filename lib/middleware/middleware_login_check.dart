import 'package:flutter/material.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/pages/login/page_login.dart';
import 'package:my_worker_app/pages/login/page_login.dart';
import 'package:my_worker_app/pages/page_index.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? memberToken = await TokenLib.getMemberToken();
    if (memberToken == null) {
      // 토큰 없으면 로그인 페이지로 이동
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageLogin()),
              (route) => false);
    } else {
      // 토큰 있으면 메인 페이지로 이동
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => PageIndex()),
              (route) => false);
    }
  }
}
