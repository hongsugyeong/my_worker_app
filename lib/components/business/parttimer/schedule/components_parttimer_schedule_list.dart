import 'package:flutter/material.dart';
import 'package:my_worker_app/model/business/parttimer/schedule/parttimer_schedule_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsParttimerScheduleList extends StatelessWidget {
  const ComponentsParttimerScheduleList({
    super.key,
    required this.callback,
    required this.parttimerScheduleList,
  });


  final VoidCallback callback;
  final ParttimerScheduleList parttimerScheduleList;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Row(
            children: [
              Container(
                child: Column(
                  children: [
                    Text(
                      '${parttimerScheduleList.businessMemberName}',
                      style: TextStyle(
                        fontSize: fontSizeMedium,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
              ),
              Text(
                '${parttimerScheduleList.businessMemberPosition}',
                style: TextStyle(
                    fontSize: fontSizeSmall,
                    color: Color.fromRGBO(64, 64, 64, 1.0)
                ),
              )
            ],
          ),
          margin: EdgeInsets.fromLTRB(20, 15, 0, 0),
        ),
        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text('출근시간'),
                margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
              ),
              Text('${parttimerScheduleList.startWork}')
            ],
          ),
          margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
        ),
        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text('퇴근시간'),
                margin: EdgeInsets.fromLTRB(0, 0, 15, 0),
              ),
              Text('${parttimerScheduleList.endWork}')
            ],
          ),
          margin: EdgeInsets.fromLTRB(20, 0, 0, 5),
        ),
        Divider(
          color: colorLight,
        ),
      ],
    );
  }
}
