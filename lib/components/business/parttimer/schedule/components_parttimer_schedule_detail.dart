import 'package:flutter/material.dart';
import 'package:my_worker_app/model/business/parttimer/schedule/parttimer_schedule_detail.dart';
import 'package:my_worker_app/config/size.dart';

class ComponentsParttimerScheduleDetail extends StatelessWidget {
  const ComponentsParttimerScheduleDetail({
    super.key,
    required this.callback,
    required this.parttimerScheduleDetail,
  });

  final VoidCallback callback;
  final ParttimerScheduleDetail parttimerScheduleDetail;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Center(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                      child: Text(
                          '홍수경',
                    style: TextStyle(
                      fontSize: fontSizeLarge,
                      fontWeight: FontWeight.bold
                    ),
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                    ),
                    Text(
                      '매니저',
                      style: TextStyle(
                          color: Color.fromRGBO(64, 64, 64, 1.0),
                          fontSize: fontSizeSmall
                      ),
                    ),
                  ],
                ),
                height: 40,
                margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
              ),
              Divider(),
              Container(
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Text('출근 시간'),
                          Container(
                            child: Text(
                              parttimerScheduleDetail.startWork,
                            ),
                            margin: EdgeInsets.fromLTRB(90, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('퇴근 시간'),
                          Container(
                            child: Text(
                              parttimerScheduleDetail.endWork,
                            ),
                            margin: EdgeInsets.fromLTRB(90, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('휴게 시간'),
                          Container(
                            child: Text(
                              parttimerScheduleDetail.recess,
                            ),
                            margin: EdgeInsets.fromLTRB(90, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('추가 근무 시간'),
                          Container(
                            child: Text(
                              parttimerScheduleDetail.plusWorkTime,
                            ),
                            margin: EdgeInsets.fromLTRB(60, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('제외 근무 시간'),
                          Container(
                            child: Text(
                              parttimerScheduleDetail.outWorkTime,
                            ),
                            margin: EdgeInsets.fromLTRB(60, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('총 근무 시간'),
                          Container(
                            child: Text(
                              parttimerScheduleDetail.totalWorkTime,
                            ),
                            margin: EdgeInsets.fromLTRB(75, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('지각 여부'),
                          Container(
                            child: Text(
                              parttimerScheduleDetail.isLateness,
                            ),
                            margin: EdgeInsets.fromLTRB(90, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('연장 근무 여부'),
                          Container(
                            child: Text(
                              parttimerScheduleDetail.isOverTime,
                            ),
                            margin: EdgeInsets.fromLTRB(60, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('비고'),
                          Container(
                            child: Text(parttimerScheduleDetail.etc),
                            margin: EdgeInsets.fromLTRB(120, 0, 0, 0),
                            width: 180,
                          )
                        ],
                      ),
                      height: 40,
                    ),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(15, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 1.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
