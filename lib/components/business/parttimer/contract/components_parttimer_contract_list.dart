import 'package:flutter/material.dart';
import 'package:my_worker_app/model/business/parttimer/contract/parttimer_contract_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsParttimerContractList extends StatelessWidget {
  const ComponentsParttimerContractList({
    super.key,
    required this.callback,
    required this.parttimerContractList,
  });

  final VoidCallback callback;
  final ParttimerContractList parttimerContractList;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            Text(
                              '${parttimerContractList.memberName}',
                              style: TextStyle(
                                fontSize: fontSizeMedium,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                      ),
                      Text(
                        '${parttimerContractList.position}',
                        style: TextStyle(
                            fontSize: fontSizeSmall,
                            color: Color.fromRGBO(64, 64, 64, 1.0)
                        ),
                      )
                    ],
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 0, 10),
            ),
            Divider(
              color: colorLight,
            ),
          ],
        ),
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
      ),
    );
  }
}
