import 'package:flutter/material.dart';
import 'package:my_worker_app/model/business/parttimer/info/parttimer_info_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/pages/business_management/PT_info_manage/page_parttimer_info_schedule_regist.dart';

enum SampleItem { itemOne, itemTwo }

class ComponentsParttimerInfoList extends StatelessWidget {
  const ComponentsParttimerInfoList({
    super.key,
    required this.parttimerInfoList,
    required this.callback,
  });

  final ParttimerInfoList parttimerInfoList;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Container(
                              child: Column(
                                children: [
                                  Text(
                                    '${parttimerInfoList.memberName}',
                                    style: TextStyle(
                                      fontSize: fontSizeMedium,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                              margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                            ),
                            Text(
                              '${parttimerInfoList.position}',
                              style: TextStyle(
                                  fontSize: fontSizeSmall,
                                  color: Color.fromRGBO(64, 64, 64, 1.0)
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              child: Text('재직 여부'),
                              margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                            ),
                            Text(
                              '${parttimerInfoList.isWork}',
                              style: TextStyle(
                                  color: Color.fromRGBO(63, 113, 175, 1.0)
                              ),
                            ),
                          ],
                        ),
                        _weekScheduleNull(),
                      ],
                    ),
                    PopupMenuButton(
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: Image.asset(
                            'assets/img/three_dots.png',
                            color: Colors.black,
                            width: 26,
                            height: 26,
                          )),
                      color: Colors.white,
                      onSelected: (value) {
                        if (value == "근무 등록하기") {
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => PageParttimerInfoScheduleRegist(id: parttimerInfoList.id,))
                          );
                        } else if (value == "재직 여부 수정") {

                        }
                      },
                      itemBuilder: (BuildContext context) =>
                      <PopupMenuEntry>[
                        PopupMenuItem(
                          value: "근무 등록하기",
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 8.0),
                                child: Image.asset(
                                  'assets/img/bell.png',
                                  color: colorDark,
                                  width: 25,
                                  height: 25,
                                ),
                              ),
                              const Text(
                                '근무 등록하기',
                                style: TextStyle(fontSize: fontSizeXSmall),
                              ),
                            ],
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                Divider(
                  color: colorLight,
                ),
              ],
            ),

          ],
        ),
        margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
      ),
    );
  }

  Widget _weekScheduleNull () {
    if (parttimerInfoList.weekSchedule != null) {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text('근무 요일'),
            margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
          ),
          Text(
            '${parttimerInfoList.weekSchedule}',
          ),
        ],
      );
    } else {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text('근무 요일'),
            margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
          ),
          Text('-',),
        ],
      );
    }
  }

}
