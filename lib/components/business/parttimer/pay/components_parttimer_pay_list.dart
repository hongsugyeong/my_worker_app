import 'package:flutter/material.dart';
import 'package:my_worker_app/model/business/parttimer/pay/parttimer_pay_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsParttimerPayList extends StatelessWidget {
  const ComponentsParttimerPayList({
    super.key,
    required this.callback,
    required this.parttimerPayList,
  });

  final VoidCallback callback;
  final ParttimerPayList parttimerPayList;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        child: Column(
                          children: [
                            Text(
                              parttimerPayList.name,
                              style: TextStyle(
                                fontSize: fontSizeMedium,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                      ),
                      Text(
                        '알바생',
                        style: TextStyle(
                            fontSize: fontSizeSmall, color: Color.fromRGBO(64, 64, 64, 1.0)),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text('급여 방식'),
                        margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                      ),
                      Text(
                        parttimerPayList.payType,
                        style: TextStyle(color: Color.fromRGBO(63, 113, 175, 1.0)),
                      ),
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text('급여'),
                        margin: EdgeInsets.fromLTRB(0, 0, 20, 0),
                      ),
                      Text(parttimerPayList.taxAfterPay)
                    ],
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
            ),
            Divider(
              color: colorLight,
            ),
          ],
        ),
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
      ),
    );
  }
}
