import 'package:flutter/material.dart';
import 'package:my_worker_app/model/business/parttimer/pay/parttimer_pay_detail.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsParttimerPayDetail extends StatelessWidget {
  const ComponentsParttimerPayDetail({
    super.key,
    required this.callback,
    required this.parttimerPayDetail,
  });

  final VoidCallback callback;
  final ParttimerPayDetail parttimerPayDetail;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Center(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        parttimerPayDetail.name,
                        style: TextStyle(
                            fontSize: fontSizeLarge,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                    ),
                    Text(
                      '매니저',
                      style: TextStyle(
                          color: Color.fromRGBO(64, 64, 64, 1.0),
                          fontSize: fontSizeSmall
                      ),
                    ),
                  ],
                ),
                height: 30,
                margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 1.0,
              ),
              Divider(
                color: colorLight,
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Text('근무 시작일'),
                          Container(
                            child: Text(
                              parttimerPayDetail.dateStartWork,
                            ),
                            margin: EdgeInsets.fromLTRB(85, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('근무 종료일'),
                          Container(
                            child: Text(
                              parttimerPayDetail.dateEndWork,
                            ),
                            margin: EdgeInsets.fromLTRB(85, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 50,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('급여 방식'),
                          Container(
                            child: Text(
                              parttimerPayDetail.payType,
                            ),
                            margin: EdgeInsets.fromLTRB(95, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 50,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Text('급여',),
                              _helpPopupafterPay(context),
                            ],
                          ),
                          Container(
                            child: Text(
                              '${parttimerPayDetail.taxAfterPay}',
                              style: TextStyle(
                                  color: colorNormal
                              ),
                            ),
                            margin: EdgeInsets.fromLTRB(75, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 50,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Text('주휴수당'),
                              _helpPopupWeek(context),
                            ],
                          ),
                          Container(
                            child: Text(
                              '${parttimerPayDetail.weekRestPay}',
                            ),
                            margin: EdgeInsets.fromLTRB(85, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 50,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Text('야간 근무 수당'),
                              _helpPopupNight(context),
                            ],
                          ),
                          Container(
                            child: Text(
                              parttimerPayDetail.timeNightWork,
                            ),
                            margin: EdgeInsets.fromLTRB(50, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 50,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Text('기타 추가 수당'),
                              _helpPopupextraplus(context),
                            ],
                          ),
                          Container(
                            child: Text(
                              '${parttimerPayDetail.plusPay}',
                            ),
                            margin: EdgeInsets.fromLTRB(50, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 50,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Text('기타 차감 금액'),
                              _helpPopupextraminus(context),
                            ],
                          ),
                          Container(
                            child: Text(
                              '${parttimerPayDetail.minusPay}',
                            ),
                            margin: EdgeInsets.fromLTRB(50, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 50,
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('세전 금액'),
                          Container(
                            child: Text(
                              '${parttimerPayDetail.taxBeforePay}'
                            ),
                            margin: EdgeInsets.fromLTRB(135, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 50,
                    ),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(15, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 1.0,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _helpPopupWeek (BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text(
                          '일 소정시간*소정 근무일수/5*시급 또는 총 소정 근무시간/40*8*시급으로 계산합니다.',
                          style: TextStyle(height: 1.5),
                        ),
                        const SizedBox(height: 15),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('닫기', style: TextStyle(color: colorDark),),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              child: const Icon(Icons.help, color: colorDark,),
            ),
          ],
        )
    );
  }

  Widget _helpPopupNight (BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text('1.5배로 계산합니다.'),
                        const SizedBox(height: 15),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('닫기', style: TextStyle(color: colorDark),),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              child: const Icon(Icons.help, color: colorDark,),
            ),
          ],
        )
    );
  }

  Widget _helpPopupextraplus (BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text('사업장별로 추가 지급되는 수당을 계산합니다.'),
                        const SizedBox(height: 15),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('닫기', style: TextStyle(color: colorDark),),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              child: const Icon(Icons.help, color: colorDark,),
            ),
          ],
        )
    );
  }

  Widget _helpPopupextraminus (BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text('사업장별로 별도 차감되는 수당을 계산합니다.'),
                        const SizedBox(height: 15),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('닫기', style: TextStyle(color: colorDark),),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              child: const Icon(Icons.help, color: colorDark,),
            ),
          ],
        )
    );
  }

  Widget _helpPopupafterPay (BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text('세전급여 - 세금으로 계산합니다.'),
                        const SizedBox(height: 15),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('닫기', style: TextStyle(color: colorDark),),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              child: const Icon(Icons.help, color: colorDark,),
            ),
          ],
        )
    );
  }

}
