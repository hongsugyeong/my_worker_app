import 'package:flutter/material.dart';
import 'package:my_worker_app/model/business/transition/business_transition_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/pages/business_management/business_transition_manage/page_business_transition_list.dart';
import 'package:my_worker_app/pages/business_management/business_transition_manage/page_business_transition_put.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_business_transition.dart';

class ComponentsBusinessTransitionList extends StatefulWidget {
  const ComponentsBusinessTransitionList({
    super.key,
    required this.businessTransitionList
  });

  final BusinessTransitionList businessTransitionList;

  @override
  State<ComponentsBusinessTransitionList> createState() =>
      _ComponentsBusinessTransitionListState();
}

class _ComponentsBusinessTransitionListState
    extends State<ComponentsBusinessTransitionList> {

  Future<void> _putButton() async {
    await RepoBusinessTransition().putButton(widget.businessTransitionList.id)
        .then((res) => {
      setState(() {
        changeText();
      })
    });
  }

  String _buttonState = '미처리';
  var _color = colorDark;
  var _textColor = Colors.black26;

  void changeText() {
    setState(() {
      // 미처리 == 미처리
      if (_buttonState == '미처리') {
        _buttonState = '처리완료';
        _color = colorNormal;
        _textColor = colorDark;
      }

      else {
        _buttonState = '미처리';
        _color = colorDark;
        _textColor = Colors.black26;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
          children: [
            Container(
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Column(
                              children: [
                                Text(
                                  '${widget.businessTransitionList.memberName}',
                                  style: TextStyle(
                                    fontSize: fontSizeMedium,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                            margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                          ),
                          Text(
                            '사장님',
                            style: TextStyle(
                                fontSize: fontSizeSmall,
                                color: Color.fromRGBO(64, 64, 64, 1.0)),
                          )
                        ],
                      ),
                      Container(
                        child: Row(
                          children: [
                            _putTransition(),
                            _isFinishButton(),
                          ],
                        ),
                        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                      ),

                    ],
                  ),
                  Row(
                    children: [
                      Text('${widget.businessTransitionList.content}'),
                    ],
                  )
                ],
              ),
              margin: EdgeInsets.fromLTRB(20, 15, 0, 5),
            ),
            Divider(
                color: colorLight
            ),
          ],
        ),
        width: MediaQuery
            .of(context)
            .size
            .width / 1.2,
    );
  }

  Widget _isFinishButton () {
    if (widget.businessTransitionList.isFinish == '미처리') {
      return Container(
        width: 80,
        child: ElevatedButton(
          child: Text('${widget.businessTransitionList.isFinish}',
            style: TextStyle(
                fontSize: 12, color: _textColor, height: 1.5),
          ),
          onPressed: _putButton,
          style: ElevatedButton.styleFrom(
              side: BorderSide(color: _color, width: 2,
              ),
              padding: EdgeInsets.zero
          ),
        ),
      );
    } else {
      return Container(
        width: 85,
        child: Container(
          child: ElevatedButton(
            child: Text('${widget.businessTransitionList.isFinish}',
              style: TextStyle(
                  fontSize: 12, color: colorDark, height: 1.5),
            ),
            onPressed: _putButton,
            style: ElevatedButton.styleFrom(
                side: const BorderSide(color: colorNormal, width: 2,
                ),
                padding: EdgeInsets.zero
            ),
          ),
        ),
      );
    }
  }

  Widget _putTransition () {
    if (widget.businessTransitionList.isFinish == '처리 완료') {
      return Container(
        width: 85,
        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
        child: ElevatedButton(
          child: Text('수정 불가',
            style: TextStyle(
                fontSize: 12, color: colorDark, height: 1.5),
          ),
          onPressed: null,
          style: ElevatedButton.styleFrom(
              side: const BorderSide(color: colorNormal, width: 2,
              ),
            padding: EdgeInsets.zero
          ),
        ),
      );
    } else {
      return Container(
        width: 80,
        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
        child: ElevatedButton(
          child: Text('수정',
            style: TextStyle(
                fontSize: 12, color: colorDark, height: 1.5),
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => PageBusinessTransitionPut(id: widget.businessTransitionList.id,))
            );
          },
          style: ElevatedButton.styleFrom(
              side: const BorderSide(color: colorNormal, width: 2,
              ),
              padding: EdgeInsets.zero
          ),
        ),
      );
    }


  }

}
