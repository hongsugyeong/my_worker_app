import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/model/business/product/business_product_detail.dart';
import 'package:my_worker_app/model/business/product/business_product_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/business/product/business_product_put_request.dart';
import 'package:my_worker_app/pages/business_management/business_product_manage/page_business_product_list.dart';
import 'package:my_worker_app/pages/business_management/business_product_manage/page_business_product_put.dart';
import 'package:my_worker_app/pages/page_index.dart';
import 'package:my_worker_app/repository/business/repo_product.dart';

class ComponentsBusinessProductPut extends StatefulWidget {
  const ComponentsBusinessProductPut(
      {super.key, required this.businessProductList, required this.id});

  final BusinessProductList businessProductList;
  final num id;

  @override
  State<ComponentsBusinessProductPut> createState() =>
      _ComponentsBusinessProductPutState();
}

class _ComponentsBusinessProductPutState
    extends State<ComponentsBusinessProductPut> {
  final _formKey = GlobalKey<FormBuilderState>();
  BusinessProductDetail? _detail;

  @override
  void initState() {
    // super: 부모
    // 부모 먼저 준비
    super.initState();

    _loadDetail();
  }

  Future<void> _loadDetail() async {
    await RepoProduct().getProduct(widget.id).then((res) => {
          setState(() {
            _detail = res.data;
          })
        });
  }

  Future<void> _putNowQuantity(BusinessProductPutRequest request) async {
    await RepoProduct().putNowQuantity(request, _detail!.id).then((value) {
      print("성공");
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PageIndex())
      );
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '제품명',
                        style: TextStyle(color: colorDark),
                      ),
                      Container(
                        child: Text(
                          '${widget.businessProductList.productName}',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: fontSizeMedium),
                          textAlign: TextAlign.center,
                        ),
                        width: MediaQuery.of(context).size.width / 2.8,
                        margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                      ),
                      Text(
                        '작성날짜:\n${widget.businessProductList.dateProduct}',
                        style: TextStyle(fontSize: fontSizeSmall),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                ),
                Container(
                  child: Column(
                    children: [
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                '최소 수량',
                                style: TextStyle(color: colorDark),
                              ),
                              Text(
                                '현재 수량',
                                style: TextStyle(color: colorDark),
                              )
                            ],
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                          '${widget.businessProductList.minQuantity}'),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      border:
                                          Border.all(color: Colors.black38)),
                                  width: 65,
                                ),
                                FormBuilder(
                                  key:_formKey,
                                  child: Container(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          height: 40,
                                          child: FormBuilderTextField(
                                            keyboardType: TextInputType.number,
                                            name: 'nowQuantity',
                                            initialValue: '${widget.businessProductList.nowQuantity}',
                                            textAlign: TextAlign.center,
                                            maxLines: null,
                                            decoration: InputDecoration(
                                              border: InputBorder.none,
                                              counterText: '',
                                              contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    decoration: BoxDecoration(
                                        border:
                                            Border.all(color: colorNormal, width: 2)),
                                    width: 65,
                                  ),
                                ),
                              ],
                            ),
                            margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 0, 0, 0),
                            child: Text(
                              '작성자: ${widget.businessProductList.memberName}',
                              style: TextStyle(fontSize: fontSizeSmall),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  width: MediaQuery.of(context).size.width / 2.2,
                ),
                Container(
                  child: Column(
                    children: [
                      Container(
                        child: ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.saveAndValidate())
                              showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text(
                                  '수정하시겠습니까?',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0)),
                                ),
                                // content: const Text('AlertDialog description'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text(
                                      '취소',
                                      style: TextStyle(color: colorNormal),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      BusinessProductPutRequest request =
                                      BusinessProductPutRequest(
                                          int.parse(_formKey.currentState?.fields['nowQuantity']?.value,)
                                      );
                                      _putNowQuantity(request);
                                    },
                                    child: const Text('확인',
                                        style: TextStyle(color: colorDark)),
                                  ),
                                ],
                                backgroundColor: Colors.white,
                              ),
                            );
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: colorNormal,
                            padding: EdgeInsets.fromLTRB(0, 22, 0, 22),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.zero),
                          ),
                          child: Text(
                            '수정',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: fontSizeSmall,
                            ),
                          ),
                        ),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 11,
                      ),
                      Container(
                        color: colorLight,
                        height: 2,
                      ),
                      Container(
                        child: ElevatedButton(
                          onPressed: null,
                          style: ElevatedButton.styleFrom(
                            backgroundColor: colorNormal,
                            padding: EdgeInsets.fromLTRB(0, 22, 0, 22),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.zero),
                          ),
                          child: Text(
                            '삭제',
                            style: TextStyle(
                                color: Colors.black38,
                                fontWeight: FontWeight.bold,
                                fontSize: fontSizeSmall),
                          ),
                        ),
                        width: MediaQuery.of(context).size.width,
                      ),
                    ],
                  ),
                  width: MediaQuery.of(context).size.width / 7.6,
                ),
              ],
            ),
            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
          ),
          Container(
            color: colorDark,
            height: 2,
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width / 2.0,
    );
  }
}
