import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/model/business/product/business_product_detail.dart';
import 'package:my_worker_app/model/business/product/business_product_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/business/product/business_product_put_request.dart';
import 'package:my_worker_app/pages/business_management/business_product_manage/page_business_product_list.dart';
import 'package:my_worker_app/pages/business_management/business_product_manage/page_business_product_put.dart';
import 'package:my_worker_app/pages/page_index.dart';
import 'package:my_worker_app/repository/business/repo_product.dart';

class ComponentsBusinessProductList extends StatefulWidget {
  const ComponentsBusinessProductList(
      {super.key, required this.businessProductList, required this.id});

  final BusinessProductList businessProductList;
  final num id;

  @override
  State<ComponentsBusinessProductList> createState() =>
      _ComponentsBusinessProductListState();
}

class _ComponentsBusinessProductListState
    extends State<ComponentsBusinessProductList> {
  final _formKey = GlobalKey<FormBuilderState>();
  BusinessProductDetail? _detail;

  @override
  void initState() {
    // super: 부모
    // 부모 먼저 준비
    super.initState();

    _loadDetail();
  }

  Future<void> _loadDelete() async {
    await RepoProduct()
        .deleteProduct(widget.id)
        .then((res) {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PageIndex())
          );
          print('성공했다리');
        })
        .catchError((err) {
      debugPrint(err);
      print('실패했다리');
    });
  }

  Future<void> _loadDetail() async {
    await RepoProduct().getProduct(widget.id).then((res) => {
          setState(() {
            _detail = res.data;
          })
        });
  }

  Future<void> _putNowQuantity(BusinessProductPutRequest request) async {
    await RepoProduct().putNowQuantity(request, widget.id).then((value) {
      print("성공");
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const PageBusinessProductList()));
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '제품명',
                        style: TextStyle(color: colorDark),
                      ),
                      Container(
                        child: Text(
                          '${widget.businessProductList.productName}',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: fontSizeMedium),
                          textAlign: TextAlign.center,
                        ),
                        margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                        width: MediaQuery.of(context).size.width / 2.8,
                      ),
                      Text(
                        '작성날짜:\n${widget.businessProductList.dateProduct}',
                        style: TextStyle(fontSize: fontSizeSmall),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                ),
                Container(
                  child: Column(
                    children: [
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                '최소 수량',
                                style: TextStyle(color: colorDark),
                              ),
                              Text(
                                '현재 수량',
                                style: TextStyle(color: colorDark),
                              )
                            ],
                          ),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                          '${widget.businessProductList.minQuantity}'),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      border:
                                          Border.all(color: Colors.black38)),
                                  width: 65,
                                ),
                                Container(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                          '${widget.businessProductList.nowQuantity}'),
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      border:
                                          Border.all(color: Colors.black38)),
                                  width: 65,
                                ),
                              ],
                            ),
                            margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(70, 10, 0, 0),
                            child: Text(
                              '작성자: ${widget.businessProductList.memberName}',
                              style: TextStyle(fontSize: fontSizeSmall),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  width: MediaQuery.of(context).size.width / 2.2,
                ),
                Container(
                  child: Column(
                    children: [
                      Container(
                        child: ElevatedButton(
                          onPressed: () {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text(
                                  '수정하시겠습니까?',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0)),
                                ),
                                // content: const Text('AlertDialog description'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text(
                                      '취소',
                                      style: TextStyle(color: colorNormal),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) => PageBusinessProductPut(id: widget.id,))
                                      );
                                    },
                                    child: const Text('확인',
                                        style: TextStyle(color: colorDark)),
                                  ),
                                ],
                                backgroundColor: Colors.white,
                              ),
                            );
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: colorNormal,
                            padding: EdgeInsets.fromLTRB(0, 22, 0, 22),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.zero),
                          ),
                          child: Text(
                            '수정',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: fontSizeSmall,
                            ),
                          ),
                        ),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 12,
                      ),
                      Container(
                        color: colorLight,
                        height: 2,
                      ),
                      Container(
                        child: ElevatedButton(
                          onPressed: () {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text(
                                  '삭제하시겠습니까?',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0)),
                                ),
                                // content: const Text('AlertDialog description'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text(
                                      '취소',
                                      style: TextStyle(color: colorNormal),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      _loadDelete();
                                    },
                                    child: const Text('확인',
                                        style: TextStyle(color: colorDark)),
                                  ),
                                ],
                                backgroundColor: Colors.white,
                              ),
                            );
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: colorNormal,
                            padding: EdgeInsets.fromLTRB(0, 22, 0, 22),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.zero),
                          ),
                          child: Text(
                            '삭제',
                            style: TextStyle(
                                color: Colors.amberAccent,
                                fontWeight: FontWeight.bold,
                                fontSize: fontSizeSmall),
                          ),
                        ),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 12,
                      ),
                    ],
                  ),
                  width: MediaQuery.of(context).size.width / 7.6,
                ),
              ],
            ),
            margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
          ),
          Container(
            color: colorDark,
            height: 2,
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width / 2.0,
    );
  }

  void _putProduct() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            backgroundColor: Colors.white,
            title: Text('제품 등록 창'),
            actions: [
              Center(
                  child: Column(
                children: [
                  FormBuilder(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                '현재 수량',
                                style: TextStyle(
                                    color: Color.fromRGBO(64, 64, 64, 1.0),
                                    fontSize: fontSizeMedium),
                              ),
                              Container(
                                child: FormBuilderTextField(
                                  initialValue: '${_detail?.nowQuantity}',
                                  keyboardType: TextInputType.number,
                                  name: 'nowQuantity',
                                  decoration: InputDecoration(
                                    hintText: '숫자만 입력해주세요.',
                                    hintStyle: TextStyle(height: 1.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(20),
                                      borderSide: BorderSide(
                                          color: colorDark, width: 2),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(20),
                                      borderSide: BorderSide(
                                          color: colorNormal, width: 2),
                                    ),
                                  ),
                                  textInputAction: TextInputAction.next,
                                  validator: (String? value) {
                                    if (value == null || value.isEmpty) {
                                      return '필수값입니다. 다시 확인해 주세요.';
                                    }
                                    return null;
                                  },
                                ),
                                width: MediaQuery.of(context).size.width / 2.6,
                                height: 45,
                                margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: ElevatedButton(
                              onPressed: () {
                                if (_formKey.currentState!.saveAndValidate()) {
                                  showDialog<String>(
                                    context: context,
                                    builder: (BuildContext context) =>
                                        AlertDialog(
                                      title: const Text(
                                        '등록하시겠습니까?',
                                        style: TextStyle(
                                            color: Color.fromRGBO(
                                                64, 64, 64, 1.0)),
                                      ),
                                      actions: <Widget>[
                                        TextButton(
                                          onPressed: () =>
                                              Navigator.pop(context, 'Cancel'),
                                          child: const Text(
                                            '취소',
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    63, 114, 175, 1.0)),
                                          ),
                                        ),
                                        TextButton(
                                          onPressed: () {
                                            BusinessProductPutRequest request =
                                                BusinessProductPutRequest(
                                              int.parse(_formKey.currentState!.fields['nowQuantity']!.value),
                                            );
                                            _putNowQuantity(request);
                                          },
                                          child: const Text('확인',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      17, 45, 78, 1.0))),
                                        ),
                                      ],
                                      backgroundColor: Colors.white,
                                    ),
                                  );
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.white,
                                  onPrimary: Color.fromRGBO(17, 45, 78, 1.0),
                                  side: BorderSide(
                                      color: Color.fromRGBO(17, 45, 78, 1.0),
                                      width: 2.0)),
                              child: const Text('등록'),
                            ),
                          ),
                          margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: ElevatedButton(
                            onPressed: () {
                              showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  title: const Text(
                                    '취소하시겠습니까?',
                                    style: TextStyle(
                                        color: Color.fromRGBO(64, 64, 64, 1.0)),
                                  ),
                                  // content: const Text('AlertDialog description'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'Cancel'),
                                      child: const Text(
                                        '취소',
                                        style: TextStyle(color: colorNormal),
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () {},
                                      child: const Text('확인',
                                          style: TextStyle(
                                              color: Color.fromRGBO(
                                                  17, 45, 78, 1.0))),
                                    ),
                                  ],
                                  backgroundColor: Colors.white,
                                ),
                              );
                            },
                            style: ElevatedButton.styleFrom(
                                primary: Colors.white,
                                onPrimary: colorNormal,
                                side:
                                    BorderSide(color: colorNormal, width: 2.0)),
                            child: const Text('취소'),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ))
            ],
          );
        });
  }
}
