import 'package:flutter/material.dart';
import 'package:my_worker_app/model/business/manual/business_manual_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsBusinessManualList extends StatelessWidget {
  const ComponentsBusinessManualList({
    super.key,
    required this.callback,
    required this.businessManualList,
  });

  final VoidCallback callback;
  final BusinessManualList businessManualList;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                    child: Image.asset(
                      'assets/img/logo_1.png',
                      width: 50,
                      height: 50,
                    ),
                    margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            '${businessManualList.title}',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                      Container(
                        child: Row(
                          children: [
                            Row(
                              children: [
                                Text(
                                  '작성 날짜: ',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                                Text(
                                  '${businessManualList.dateManual}',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width / 4.5,
                            ),
                            Row(
                              children: [
                                Text(
                                  '작성자: ',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                                Text(
                                  '${businessManualList.memberName}',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                        margin: EdgeInsets.fromLTRB(0, 4, 0, 0),
                      ),
                    ],
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(20, 0, 0, 5),
            ),
            Divider(
              color: colorLight,
            ),
          ],
        ),
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
        width: MediaQuery.of(context).size.width / 1.2,
      ),
    );
  }
}
