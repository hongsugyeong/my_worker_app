import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsLoading extends StatelessWidget {
  const ComponentsLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: CircularProgressIndicator(
              color: colorDark,
              strokeWidth: 10,
            ),
            width: 80,
            height: 80,
            margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
          ),
        ],
      ),
    );
  }
}
