import 'package:flutter/material.dart';

import '../../model/home/quickmenu_item.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsQuickmenu extends StatelessWidget {
  const ComponentsQuickmenu({
    super.key,
    required this.callback,
    required this.quickMenu,
  });

  final VoidCallback callback;
  final QuickMenu quickMenu;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Image.asset(quickMenu.imgUrl, width: 30, height: 30, color: colorDark),
              margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
            ),

            Text(quickMenu.menu, style: TextStyle(fontSize: 10, color: Colors.black))
          ],
        ),
        width: 80,
        height: 70,
        decoration: BoxDecoration(
            border: Border.all(color: colorDark, width: 2),
            borderRadius: BorderRadius.circular(30)),
        margin: EdgeInsets.fromLTRB(0, 30, 0, 0),
      ),

    );
  }
}
