import 'package:flutter/material.dart';
import 'package:my_worker_app/model/home/community/one_community_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsOneCommunityList extends StatelessWidget {
  const ComponentsOneCommunityList({
    super.key,
    required this.callback,
    required this.oneCommunityList,
  });

  final VoidCallback callback;
  final OneCommunityList oneCommunityList;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    oneCommunityList.title,
                    style: TextStyle(
                        color: colorNormal,
                        fontSize: fontSizeMedium
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 0, 0, 5),
            ),
            Container(
              color: colorLight,
              height: 1,
            ),
            _imgNull(),
            Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Text(
                      oneCommunityList.content,
                      style: TextStyle(
                          fontSize: fontSizeSmall
                      ),
                    ),
                  ),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
            ),
            Container(
              child: Row(
                children: [
                  Text('댓글 '),
                  Text('18개', style: TextStyle(color: colorDark, fontWeight: FontWeight.bold),),
                ],
              ),
              margin: EdgeInsets.fromLTRB(10, 20, 0, 0),
            ),
            Divider(
              color: colorDark
            ),
          ],
        ),
        margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
        width: MediaQuery.of(context).size.width / 1.0,
      ),
    );
  }


  Widget _imgNull () {
    if (oneCommunityList.boardImgUrl == 'null') {
      return SizedBox();
    } else {
      return Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Text(
                '${oneCommunityList?.boardImgUrl}',
                style: TextStyle(
                    fontSize: fontSizeSmall
                ),
              ),
            ),
          ],
        ),
        margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
      );
    }
  }
}
