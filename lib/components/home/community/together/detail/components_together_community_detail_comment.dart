import 'package:flutter/material.dart';
import 'package:my_worker_app/model/home/community/one_community_comment.dart';

enum SampleItem { itemOne, itemTwo, itemThree }

class ComponentsTogetherCommunityDetailComment extends StatelessWidget {
  const ComponentsTogetherCommunityDetailComment({
    super.key,
    required this.callback,
    required this.oneCommunityComment,
  });

  final VoidCallback callback;
  final OneCommunityComment oneCommunityComment;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Text('${oneCommunityComment?.content}'),
                        width: 300,
                      ),
                      Text(
                        '${oneCommunityComment?.dateComment}',
                        style: TextStyle(
                            color: Color.fromRGBO(64, 64, 64, 1.0),
                            fontSize: 9
                        ),
                      ),
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                ),
                _popup()
              ],
            ),
            Divider(
              color: Colors.grey,
            ),
          ],
        ),
        width: MediaQuery.of(context).size.width / 1.0,
      )
    );
  }

  Widget _popup() {
    return PopupMenuButton<SampleItem>(

      itemBuilder: (BuildContext context) => <PopupMenuEntry<SampleItem>>[
        const PopupMenuItem<SampleItem>(
          value: SampleItem.itemOne,
          child: Text('수정하기'),
        ),
        const PopupMenuItem<SampleItem>(
          value: SampleItem.itemTwo,
          child: Text('삭제하기'),
        ),
        const PopupMenuItem<SampleItem>(
          value: SampleItem.itemThree,
          child: Text('신고하기'),
        ),
      ],
    );
  }
}
