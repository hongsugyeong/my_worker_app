import 'package:flutter/material.dart';

import '../../model/home/business_item.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsBusinessItem extends StatelessWidget {
  const ComponentsBusinessItem({
    super.key,
    required this.callback,
    required this.businessItem,
  });

  final VoidCallback callback;
  final BusinessItem businessItem;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  child: Column(
                    children: [
                      Icon(Icons.business, size: 50, color: colorDark)
                    ],
                  ),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        businessItem.name,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(businessItem.ownerName),
                      Text(businessItem.businessType),
                      Text(businessItem.phoneNumber),
                      Text(businessItem.address),
                    ],
                  ),
                  width: 280,
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                ),
              ],
            ),
            Divider(
              color: colorLight,
            ),
          ],
        ),
        margin: EdgeInsets.only(top: 5)
      ),
    );
  }
}
