import 'package:flutter/material.dart';
import 'package:my_worker_app/config/color.dart';

class ComponentsBusinessList extends StatelessWidget implements PreferredSizeWidget {
  const ComponentsBusinessList({super.key, required this.menu});

  final String menu;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: BackButton(
        color: Colors.white,
      ),
      title: Text(
        menu,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
      centerTitle: true,
      actions: [
        Image.asset('assets/img/logo_2.png')
      ],
      backgroundColor: colorDark,
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(50);

}
