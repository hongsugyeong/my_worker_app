import 'package:flutter/material.dart';

class ComponentsAppbarMenu extends StatelessWidget implements PreferredSizeWidget {
  const ComponentsAppbarMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: BackButton(
        color: Colors.white,
      ),
      title: Text(
          '동대문 엽기 떡볶이 부곡점',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 18),
        ),
      centerTitle: true,
      actions: [
        Container(
          child: Image.asset(
            'assets/img/logo_2.png',
            width: 50,
            height: 50,
          ),
          margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
        ),
      ],
      backgroundColor: Color.fromRGBO(17, 45, 78, 1.0),
    );
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(50);

}
