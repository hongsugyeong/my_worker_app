import 'package:flutter/material.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/model/home/info/my_info.dart';
import 'package:my_worker_app/pages/business_management/business_manage/page_business_add.dart';
import 'package:my_worker_app/pages/login/page_login.dart';
import 'package:my_worker_app/pages/main_page/ask/page_ask_list.dart';
import 'package:my_worker_app/pages/main_page/board/page_community.dart';
import 'package:my_worker_app/pages/main_page/notice/page_notice_list.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/pages/page_index.dart';
import 'package:my_worker_app/repository/home/repo_my_page.dart';

class PageMyPage extends StatefulWidget {
  const PageMyPage({super.key});

  @override
  State<PageMyPage> createState() => _PageMyPageState();
}

class _PageMyPageState extends State<PageMyPage> {

  MyInfo? _myInfo;

  Future<void> _loadDetail() async {
    await RepoMyPage().getMyInfo().then((res) => {
      setState(() {
        _myInfo = res.data;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    if (_myInfo != null) {
      return Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              _myInfoDetail(),
              _myPageMenu(),
            ],
          ),
        ),
      );
    } else {
      return SizedBox(
          child: ComponentsLoading()
      );
    }

  }

  Widget _myInfoDetail() {
    return Container(
      child: Column(
        children: [
          Row(
            children: [
              Container(
                child: Column(
                  children: [
                    Image.asset('assets/img/people.png',
                        width: 50, height: 50, color: colorDark),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          '${_myInfo!.name}',
                          style: TextStyle(
                              fontSize: fontSizeLarge,
                              fontWeight: FontWeight.bold),
                        ),
                        Container(
                          child: Text(
                            '[ ${_myInfo!.memberType} ]',
                            style: TextStyle(
                                fontSize: 10,
                                color: Color.fromRGBO(64, 64, 64, 1.0)),
                          ),
                          margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              '아이디',
                              style: TextStyle(
                                  color: Color.fromRGBO(64, 64, 64, 1.0)),
                            ),
                            Text('생년월일',
                                style: TextStyle(
                                    color: Color.fromRGBO(64, 64, 64, 1.0))),
                            Text('전화번호',
                                style: TextStyle(
                                    color: Color.fromRGBO(64, 64, 64, 1.0))),
                          ],
                        ),
                        Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('${_myInfo!.username}',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0))),
                              Text('${_myInfo!.dateBirth}',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0))),
                              Text('${_myInfo!.phoneNumber}',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0))),
                            ],
                          ),
                          margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                SizedBox(
                  child: ElevatedButton(
                    onPressed: () {},
                    child: Text(
                      '수정',
                      style: TextStyle(color: colorDark),
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        onPrimary: colorDark,
                        side: BorderSide(color: colorDark, width: 2.0)),
                  ),
                  width: 80,
                  height: 30,
                ),
              ],
            ),
            margin: EdgeInsets.fromLTRB(0, 0, 5, 5),
          ),
          Container(
            color: colorLight,
            height: 2,
          ),
        ],
      ),
      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
    );
  }

  Widget _myPageMenu() {
    return Column(
      children: [
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const PageIndex()));
          },
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Column(children: [
                    Text(
                      '매장 관리',
                      style: TextStyle(height: 3),
                    ),
                  ]),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                ),
                Divider(
                  color: colorLight,
                ),
              ],
            ),
          ),
        ),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const PageCommunity()));
          },
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Column(children: [
                    Text(
                      '커뮤니티',
                      style: TextStyle(height: 3),
                    ),
                  ]),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                ),
                Divider(
                  color: colorLight,
                ),
              ],
            ),
          ),
        ),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const PageAskList()));
          },
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Column(children: [
                  Text(
                    '고객센터',
                    style: TextStyle(height: 3),
                  ),
                ]),
                margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              ),
              Divider(
                color: colorLight,
              ),
            ],
          ),
        ),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const PageNoticeList()));
          },
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Column(children: [
                    Text(
                      '공지사항',
                      style: TextStyle(height: 3),
                    ),
                  ]),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                ),
                Divider(
                  color: colorLight,
                ),
              ],
            ),
          ),
        ),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const PageBusinessAdd()));
          },
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Column(children: [
                    Text(
                      '사업장 등록',
                      style: TextStyle(height: 3),
                    ),
                  ]),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                ),
                Divider(
                  color: colorLight,
                ),
              ],
            ),
          ),
        ),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const PageLogin()));
          },
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Column(children: [
                    Text(
                      '로그아웃',
                      style: TextStyle(height: 3),
                    ),
                  ]),
                  margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                ),
                Divider(
                  color: colorLight,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
