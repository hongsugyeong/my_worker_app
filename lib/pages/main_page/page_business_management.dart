import 'package:flutter/material.dart';
import 'package:my_worker_app/components/home/components_business_item.dart';
import 'package:my_worker_app/model/home/business_item.dart';
import 'package:my_worker_app/pages/business_management/business_manage/page_business_detail.dart';
import 'package:my_worker_app/pages/business_management/page_business_menu_list.dart';
import 'package:my_worker_app/config/color.dart';

class PageBusinessManagement extends StatefulWidget {
  const PageBusinessManagement({super.key});

  @override
  State<PageBusinessManagement> createState() => _PageBusinessManagementState();
}

class _PageBusinessManagementState extends State<PageBusinessManagement> {
  // 스크롤 찍먹하려고 일부러 여러 개의 사업장 넣은 거
  List<BusinessItem> _list = [
    BusinessItem('동대문엽기떡볶이 부곡점', '김승희', '한식 일반 음식점', '031-485-2483',
        '경기도 안산시 상록구 부곡동 534-3번지 130호'),
    BusinessItem('정직유부 중앙점', '김승희', '한식 일반 음식점', '031-403-8629',
        '경기도 안산시 단원구 고잔동 534-3 안산중앙노블레스'),
    BusinessItem(
        '이리와 카페', '김승희', '커피 전문점', '053-111-2541', '대구광역시 동구 화랑로25길 43'),
  ];

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: SingleChildScrollView(
          child: Column(
            children: [
              _businessList(),
              // _plusButton(),
            ],
          ),
        )
    );
  }

  Widget _businessList() {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PageBusinessMenuList())
        );
      },
      child: Column(
        children: [
          ListView.builder(
            itemCount: _list.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext ctx, int idx) {
              return ComponentsBusinessItem(
                  callback: () {}, businessItem: _list[idx]);
            },
          ),
        ],
      ),
    );
      
  }

  Widget _plusButton() {
    return Container(
      child: TextButton(
          onPressed: () {},
          child: Icon(Icons.add, color: colorDark)
      ),
      width: 400,
      height: 50,
    );
  }
}
