import 'package:flutter/material.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class PageHome extends StatefulWidget {
  const PageHome({Key? key}) : super(key: key);

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _notice(),
          Container(
            child: SizedBox(
              child: Image.asset('assets/img/event_banner.png'),
            ),
            margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
          ),
          _quickMenu(),
        ],
      ),
    );
  }

  Widget _notice() {
    return Container(
      child: Row(
        children: [
          Container(
            child: Image.asset(
              'assets/img/megaphone.png',
              width: MediaQuery.of(context).size.width / 8,
              height: MediaQuery.of(context).size.height / 8,
            ),
            margin: EdgeInsets.fromLTRB(20, 0, 15, 0),
          ),
          Container(
            child: Text(
              '금일 15-16시 사이에 업데이트 합니다. 자세한 사항은 공지사항에서 확인해 주시고 이용에 불편을 끼쳐드려 죄송합니다.',
              style: TextStyle(fontSize: fontSizeSmall),
            ),
            width: MediaQuery.of(context).size.width / 2.0,
          ),
        ],
      ),
      width: MediaQuery.of(context).size.width / 1.2,
      height: 140,
      margin: EdgeInsets.fromLTRB(10, 20, 0, 20),
      decoration: BoxDecoration(
          border: Border.all(color: colorDark, width: 2),
          borderRadius: BorderRadius.circular(30)),
    );
  }

  Widget _quickMenu() {
    return Container(
      child: GridView(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: 0.9,
        mainAxisSpacing: 20,
        crossAxisSpacing: 20),
          children: [
            GestureDetector(
              onTap: () {},
                child: Container(
                  child: Column(
                    children: [
                      Container(
                        child: Image.asset('assets/img/people.png', width: 40, height: 40, color: colorDark),
                        margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                      ),
                      Text('메뉴 이름', style: TextStyle(fontSize: 10, color: Colors.black))
                    ],
                  ),
                  width: 80,
                  height: 90,
                  decoration: BoxDecoration(
                      border: Border.all(color: colorDark, width: 2),
                      borderRadius: BorderRadius.circular(35)),
                  margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
                ),
              ),
            GestureDetector(
              onTap: () {},
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Image.asset('assets/img/people.png', width: 40, height: 40, color: colorDark),
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                    ),
                    Text('메뉴 이름', style: TextStyle(fontSize: 10, color: Colors.black))
                  ],
                ),
                width: 80,
                height: 90,
                decoration: BoxDecoration(
                    border: Border.all(color: colorDark, width: 2),
                    borderRadius: BorderRadius.circular(35)),
                margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Image.asset('assets/img/people.png', width: 40, height: 40, color: colorDark),
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                    ),
                    Text('메뉴 이름', style: TextStyle(fontSize: 10, color: Colors.black))
                  ],
                ),
                width: 80,
                height: 90,
                decoration: BoxDecoration(
                    border: Border.all(color: colorDark, width: 2),
                    borderRadius: BorderRadius.circular(35)),
                margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Image.asset('assets/img/people.png', width: 40, height: 40, color: colorDark),
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                    ),
                    Text('메뉴 이름', style: TextStyle(fontSize: 10, color: Colors.black))
                  ],
                ),
                width: 80,
                height: 90,
                decoration: BoxDecoration(
                    border: Border.all(color: colorDark, width: 2),
                    borderRadius: BorderRadius.circular(35)),
                margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Image.asset('assets/img/people.png', width: 40, height: 40, color: colorDark),
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                    ),
                    Text('메뉴 이름', style: TextStyle(fontSize: 10, color: Colors.black))
                  ],
                ),
                width: 80,
                height: 90,
                decoration: BoxDecoration(
                    border: Border.all(color: colorDark, width: 2),
                    borderRadius: BorderRadius.circular(35)),
                margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Image.asset('assets/img/people.png', width: 40, height: 40, color: colorDark),
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                    ),
                    Text('메뉴 이름', style: TextStyle(fontSize: 10, color: Colors.black))
                  ],
                ),
                width: 80,
                height: 90,
                decoration: BoxDecoration(
                    border: Border.all(color: colorDark, width: 2),
                    borderRadius: BorderRadius.circular(35)),
                margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Image.asset('assets/img/people.png', width: 40, height: 40, color: colorDark),
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                    ),
                    Text('메뉴 이름', style: TextStyle(fontSize: 10, color: Colors.black))
                  ],
                ),
                width: 80,
                height: 90,
                decoration: BoxDecoration(
                    border: Border.all(color: colorDark, width: 2),
                    borderRadius: BorderRadius.circular(35)),
                margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Image.asset('assets/img/people.png', width: 40, height: 40, color: colorDark),
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                    ),
                    Text('메뉴 이름', style: TextStyle(fontSize: 10, color: Colors.black))
                  ],
                ),
                width: 80,
                height: 90,
                decoration: BoxDecoration(
                    border: Border.all(color: colorDark, width: 2),
                    borderRadius: BorderRadius.circular(35)),
                margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
              ),
            ),
            GestureDetector(
              onTap: () {},
              child: Container(
                child: Column(
                  children: [
                    Container(
                      child: Image.asset('assets/img/plus.png', width: 40, height: 40, color: colorNormal),
                      margin: EdgeInsets.fromLTRB(0, 8, 0, 5),
                    ),
                    Text('메뉴 이름', style: TextStyle(fontSize: 10, color: Colors.black))
                  ],
                ),
                width: 80,
                height: 90,
                decoration: BoxDecoration(
                    border: Border.all(color: colorDark, width: 2),
                    borderRadius: BorderRadius.circular(35)),
                margin: EdgeInsets.fromLTRB(5, 20, 5, 0),
              ),
            ),
            ]
      ),
    width: MediaQuery.of(context).size.width / 1.1,
      height: 380,
      margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
      decoration: BoxDecoration(
          border: Border.all(color: colorDark, width: 2),
          borderRadius: BorderRadius.circular(30)),
    );
  }
}
