import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/model/home/notice/notice_detail.dart';
import 'package:my_worker_app/repository/home/repo_notice.dart';
import 'package:my_worker_app/config/color.dart';

class PageNoticeDetail extends StatefulWidget {
  const PageNoticeDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageNoticeDetail> createState() => _PageNoticeDetailState();
}

class _PageNoticeDetailState extends State<PageNoticeDetail> {

  NoticeDetail? _detail;

  Future<void> _loadDetail() async {
    await RepoNotice().getProduct(widget.id)
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '공지사항',),
      body: SingleChildScrollView(
        child: _noticeDetail(context),
      ),
    );
  }

  Widget _noticeDetail(BuildContext context) {
    if (_detail == null) {
      return SizedBox(
          child: ComponentsLoading()
      );
    } else {
      return SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        '${_detail!.title}',
                        style: TextStyle(fontSize: 20,),
                      ),
                    ),
                  ],
                ),
                height: 30,
                margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
              ),
              Divider(
                color: colorLight,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text('작성 날짜: ', style: TextStyle(color: Color.fromRGBO(40, 40, 40, 1.0), fontSize: 13),),
                    Container(
                      child: Text('${_detail!.dateNotice}', style: TextStyle(color: Color.fromRGBO(40, 40, 40, 1.0), fontSize: 13),),
                      margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                    ),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(105, 0, 0, 0),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('${_detail!.noticeImgUrl}'),
                    Text('${_detail!.content}'),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              ),
            ],
          ),
          width: MediaQuery.of(context).size.width / 1.0,
        ),
      );
    }
  }
}
