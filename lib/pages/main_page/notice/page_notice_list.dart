import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/components/home/notice/components_notice_list.dart';
import 'package:my_worker_app/model/home/notice/notice_list.dart';
import 'package:my_worker_app/pages/main_page/notice/page_notice_detail.dart';
import 'package:my_worker_app/repository/home/repo_notice.dart';

class PageNoticeList extends StatefulWidget {
  const PageNoticeList({super.key});

  @override
  State<PageNoticeList> createState() => _PageNoticeListState();
}

class _PageNoticeListState extends State<PageNoticeList> {

  final _scrollController = ScrollController();

  List<NoticeList> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  @override
  void initState() {
    // super: 부모
    // 부모 먼저 준비
    super.initState();

    // 해당 페이지 죽기 전까지 계속 감시하기
    _scrollController.addListener(() {
      // 스크롤이 바닥 쳤을 때 _loadItems 실행
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems(reFresh: true);
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      await RepoNotice().getList(page: _currentPage)
          .then((res) {
        setState(() {
          _totalPage = res.totalPage.toInt();
          _totalItemCount = res.totalCount.toInt();
          _list = [..._list,...res.list!];

          _currentPage++;
        });
      }).catchError((err) {
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollController.animateTo(
          0,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '공지사항',),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            _loadItems(reFresh: true);
          });
        },
        child: ListView(
          controller: _scrollController,
          children: [_noticeList(),],
        ),
      ),
    );
  }

  Widget _noticeList() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            itemCount: _list.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext ctx, int idx) {
              return ComponentsNoticeList(
                callback: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageNoticeDetail(id: _list[idx].id,)));
                }, noticeList: _list[idx],);
            },
          ),
        ],
      );
    } else {
      return SizedBox(
        child: ComponentsLoading()
      );
    }
  }
}
