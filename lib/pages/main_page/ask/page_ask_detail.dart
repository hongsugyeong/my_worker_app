import 'package:flutter/material.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/model/home/ask/ask_detail.dart';
import 'package:my_worker_app/pages/main_page/ask/page_ask_list.dart';
import 'package:my_worker_app/repository/home/repo_ask.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';

class PageAskDetail extends StatefulWidget {
  const PageAskDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageAskDetail> createState() => _PageAskDetailState();
}

class _PageAskDetailState extends State<PageAskDetail> {
  AskDetail? _detail;

  Future<void> _loadDetail() async {
    await RepoAsk().getProduct(widget.id).then((res) =>
    {
      setState(() {
        _detail = res.data;
      })
    });
  }

  Future<void> _loadDelete() async {
    await RepoAsk()
        .deleteAsk(widget.id)
        .then((res) =>
    {
      print('성공했다리')
    })
        .catchError((err) {
      debugPrint(err);
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(
          color: Colors.white,
        ),
        title: Text(
          '고객센터',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: [
          PopupMenuButton(
            child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Image.asset(
                  'assets/img/three_dots.png',
                  color: Colors.white,
                  width: 26,
                  height: 26,
                )),
            color: Colors.white,
            onSelected: (value) {
              if (value == "수정하기") {
                // add desired output
              } else if (value == "삭제하기") {
                showDialog<String>(
                  context: context,
                  builder: (BuildContext context) =>
                      AlertDialog(
                        title: const Text(
                          '삭제하시겠습니까?',
                          style: TextStyle(color: Color.fromRGBO(64, 64, 64,
                              1.0)),
                        ),
                        // content: const Text('AlertDialog description'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text(
                              '취소',
                              style: TextStyle(color: colorNormal),
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              _loadDelete();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (
                                          context) => const PageAskList()));
                            },
                            child: const Text('확인',
                                style: TextStyle(color: colorDark)),
                          ),
                        ],
                        backgroundColor: Colors.white,
                      ),
                );
                // add desired output
              }
            },
            itemBuilder: (BuildContext context) =>
            <PopupMenuEntry>[
              PopupMenuItem(
                value: "수정하기",
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Image.asset(
                        'assets/img/pencil.png',
                        color: colorDark,
                        width: 25,
                        height: 25,
                      ),
                    ),
                    const Text(
                      '수정하기',
                      style: TextStyle(fontSize: fontSizeXSmall),
                    ),
                  ],
                ),
              ),
              PopupMenuItem(
                value: "삭제하기",
                child: Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.only(right: 8.0),
                        child: Image.asset(
                          'assets/img/delete.png',
                          color: colorDark,
                          width: 25,
                          height: 25,
                        )),
                    const Text(
                      '삭제하기',
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
              ),
            ],
          )
        ],
        backgroundColor: colorDark,
      ),
      body: SingleChildScrollView(
          child: Column(
            children: [_askDetail(context), _askAnswerDetail()],
          )),
    );
  }

  // 고객이 물어본 부분
  Widget _askDetail(BuildContext context) {
    if (_detail == null) {
      return SizedBox(child: ComponentsLoading());
    } else {
      return SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        '${_detail!.title}',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ],
                ),
                height: 30,
                margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
              ),
              Divider(
                color: colorLight,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      '작성 날짜: ',
                      style: TextStyle(
                          color: Color.fromRGBO(40, 40, 40, 1.0), fontSize: 13),
                    ),
                    Container(
                      child: Text(
                        '${_detail!.dateMemberAsk}',
                        style: TextStyle(
                            color: Color.fromRGBO(40, 40, 40, 1.0),
                            fontSize: 13),
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                    ),
                  ],
                ),
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    _imgNull(),
                    Text('${_detail!.content}'),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
              ),
            ],
          ),
          width: MediaQuery
              .of(context)
              .size
              .width / 1.0,
        ),
      );
    }
  }

  // 관리자가 답변한 부분
  Widget _askAnswerDetail() {
    if (_detail?.comment == null) {
      return SizedBox();
    } else {
      return SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Row(
                children: [
                  Image.asset(
                    'assets/img/enter.png',
                    width: 50,
                    height: 50,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            '답변: ',
                            style: TextStyle(fontSize: fontSizeMedium),
                          ),
                          Text(
                            '${_detail?.comment?.answerTitle}',
                            style: TextStyle(fontSize: fontSizeMedium),
                          ),
                        ],
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text(
                              '작성날짜: ',
                              style: TextStyle(
                                  color: Color.fromRGBO(40, 40, 40, 1.0),
                                  fontSize: 13),
                            ),
                            Container(
                              child: Text(
                                '${_detail?.comment?.dateAnswer}',
                                style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: 13),
                              ),
                              margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                              width: 190,
                            ),
                          ],
                        ),
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      ),
                    ],
                  )
                ],
              ),
              Container(
                child: Column(
                  children: [
                    _imgAnswerNull(),
                    Text('${_detail?.comment?.answerContent}')
                  ],
                ),
                width: 310,
                margin: EdgeInsets.fromLTRB(50, 0, 0, 0),
              ),
            ],
          ),
          margin: EdgeInsets.fromLTRB(10, 50, 10, 0),
          width: MediaQuery
              .of(context)
              .size
              .width / 1.0,
        ),
      );
    }
  }

  Widget _imgNull() {
    if (_detail?.questionImgUrl == 'null') {
      return SizedBox();
    } else {
      return Text('${_detail?.questionImgUrl}');
    }
  }

  Widget _imgAnswerNull() {
    if (_detail?.comment?.answerImgUrl == 'null') {
      return SizedBox();
    } else {
      return Text('${_detail?.comment?.answerImgUrl}');
    }
  }
}
