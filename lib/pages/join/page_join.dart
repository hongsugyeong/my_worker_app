import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/button/components_text_form.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/model/login/join_request.dart';
import 'package:my_worker_app/pages/login/page_login.dart';
import 'package:my_worker_app/repository/login/repo_login.dart';

class PageJoin extends StatefulWidget {
  const PageJoin({Key? key}) : super(key: key);

  @override
  State<PageJoin> createState() => _PageJoinState();
}

class _PageJoinState extends State<PageJoin> {
  final _formKey = GlobalKey<FormBuilderState>();

  final String memberType = "ROLE_BOSS";
  List<String> genderOptions = ['남자', '여자'];

  Future<void> sendJoin(JoinRequest request) async {
    await RepoLogin().doJoin(request).then((value) {
      print('성공');
      request.memberType = '';
      request.name = '';
      request.username = '';
      request.password = '';
      request.passwordRe = '';
      request.isMan = '';
      request.dateBirth = '';
      request.phoneNumber = '';
      request.address = '';
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PageLogin())
      );
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _login(context),
    );
  }

  Widget _login(BuildContext context) {
    return Scaffold(
      backgroundColor: colorDark,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 4,
                    alignment: Alignment.center,
                    child: Image.asset(
                      'assets/img/logo_2.png',
                      width: 200,
                    ),
                  ),
                ],
              ),
              SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                        BorderRadius.only(topRight: Radius.circular(80)),
                  ),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.all(30),
                        child: Text(
                          '회원가입',
                          style: TextStyle(
                              fontSize: 30,
                              color: colorDark,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Column(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 1.5,
                            padding: EdgeInsets.only(bottom: 10),
                            child: FormBuilder(
                              key: _formKey,
                              autovalidateMode: AutovalidateMode.disabled,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  ComponentTextForm(
                                    title: '아이디',
                                    maxLength: 40,
                                    name: 'username',
                                    hintText: '아이디를 입력해주세요.',
                                    obscureText: false,
                                    lines: 20,
                                  ),
                                  ComponentTextForm(
                                    title: '비밀번호',
                                    maxLength: 20,
                                    name: 'password',
                                    hintText: '비밀번호를 입력해주세요.',
                                    obscureText: true,
                                    lines: 20,
                                  ),
                                  ComponentTextForm(
                                    title: '비밀번호 확인',
                                    maxLength: 20,
                                    name: 'passwordRe',
                                    hintText: '비밀번호를 한번 더 입력해주세요.',
                                    obscureText: true,
                                    lines: 20,
                                  ),
                                  ComponentTextForm(
                                    title: '이름',
                                    maxLength: 20,
                                    name: 'name',
                                    hintText: '이름을 입력해주세요.',
                                    obscureText: false,
                                    lines: 20,
                                  ),
                                  ComponentTextForm(
                                    title: '생년월일',
                                    maxLength: 10,
                                    name: 'dateBirth',
                                    hintText: '2000-11-05',
                                    obscureText: false,
                                    lines: 20,
                                  ),
                                  ComponentTextForm(
                                    title: '전화번호',
                                    maxLength: 13,
                                    name: 'phoneNumber',
                                    hintText: '010-0000-0000',
                                    obscureText: false,
                                    lines: 20,
                                  ),
                                  ComponentTextForm(
                                      title: '주소',
                                      name: 'address',
                                      obscureText: false,
                                      hintText: '주소를 입력해주세요.',
                                      lines: 20,
                                      maxLength: 40
                                  ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '성별',
                                      style: const TextStyle(
                                        fontSize: fontSizeMedium,
                                      ),
                                    ),
                                    const SizedBox(height: 5.0,),
                                    Container(
                                      width: MediaQuery.of(context).size.width / 1.5,
                                      height: MediaQuery.of(context).size.height / 13,
                                      child: DropdownButtonHideUnderline(
                                        child: FormBuilderDropdown<String>(
                                          name: '성별',
                                          initialValue: '남자',
                                          decoration: InputDecoration(
                                            enabledBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(30),
                                              borderSide:
                                              BorderSide(color: colorDark, width: 2),
                                            ),
                                            focusedBorder: OutlineInputBorder(
                                              borderRadius: BorderRadius.circular(30),
                                              borderSide:
                                              BorderSide(color: colorNormal, width: 2),
                                            ),
                                            suffix: IconButton(
                                              icon: const Icon(Icons.close),
                                              onPressed: () {
                                                _formKey.currentState!.fields['isMan']
                                                    ?.reset();
                                              },
                                            ),
                                            hintText: '성별을 선택해주세요',
                                          ),
                                          items: genderOptions
                                              .map((isMan) => DropdownMenuItem(
                                            alignment: AlignmentDirectional.center,
                                            value: isMan,
                                            child: Text(isMan),
                                          ))
                                              .toList(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 1.2,
                            height: MediaQuery.of(context).size.height / 9,
                            padding: EdgeInsets.all(20),
                            child: ElevatedButton(
                              onPressed: () {
                                  JoinRequest request = JoinRequest(
                                    memberType: memberType,
                                    name: _formKey.currentState!.fields['name']!.value,
                                    username: _formKey.currentState!.fields['username']!.value,
                                    password: _formKey.currentState!.fields['password']!.value,
                                    passwordRe: _formKey.currentState!.fields['passwordRe']!.value,
                                    isMan: _formKey.currentState!.fields['isMan']!.value,
                                    dateBirth: _formKey.currentState!.fields['dateBirth']!.value,
                                    phoneNumber: _formKey.currentState!.fields['phoneNumber']!.value,
                                    address: _formKey.currentState!.fields['address']!.value,
                                  );
                                  sendJoin(request);
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: colorDark,
                                  onPrimary: colorDark,
                                  side:
                                      BorderSide(color: colorDark, width: 2.0)),
                              child: Text(
                                '가입하기',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 1.2,
                            height: MediaQuery.of(context).size.height / 9,
                            padding: EdgeInsets.all(20),
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => PageLogin()));
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: colorDark,
                                  onPrimary: colorDark,
                                  side:
                                      BorderSide(color: colorDark, width: 2.0)),
                              child: Text(
                                '로그인하러 가기',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
