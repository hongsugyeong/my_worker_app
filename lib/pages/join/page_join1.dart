import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/button/components_text_form.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/login/join_request.dart';
import 'package:my_worker_app/pages/join/page_join2.dart';

class PageJoin1 extends StatefulWidget {
  const PageJoin1({super.key});

  @override
  State<PageJoin1> createState() => _PageJoin1State();
}

class _PageJoin1State extends State<PageJoin1> {
  final _formKey = GlobalKey<FormBuilderState>();

  final memberType = "ROLE_BOSS";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorDark,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 3,
                    alignment: Alignment.center,
                    child: Image.asset(
                      'assets/img/logo_2.png',
                      width: 200,
                    ),
                  ),
                ],
              ),
              SingleChildScrollView(
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                        BorderRadius.only(topRight: Radius.circular(80)),
                  ),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(30, 30, 30, 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '회원가입',
                              style: TextStyle(
                                  fontSize: 30,
                                  color: colorDark,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '1단계',
                              style: TextStyle(
                                  fontSize: 30,
                                  color: colorDark,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width / 1.5,
                            padding: EdgeInsets.only(bottom: 10),
                            child: FormBuilder(
                              key: _formKey,
                              autovalidateMode: AutovalidateMode.disabled,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  ComponentTextForm(
                                    title: '아이디',
                                    maxLength: 40,
                                    name: 'username',
                                    hintText: '아이디를 입력해주세요.',
                                    obscureText: false,
                                    lines: 20,
                                  ),
                                  ComponentTextForm(
                                    title: '비밀번호',
                                    maxLength: 20,
                                    name: 'password',
                                    hintText: '비밀번호를 입력해주세요.',
                                    obscureText: true,
                                    lines: 20,
                                  ),
                                  ComponentTextForm(
                                    title: '비밀번호 확인',
                                    maxLength: 20,
                                    name: 'passwordRe',
                                    hintText: '비밀번호를 한번 더 입력해주세요.',
                                    obscureText: true,
                                    lines: 20,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width / 1.2,
                            height: MediaQuery.of(context).size.height / 9,
                            padding: EdgeInsets.all(20),
                            child: ElevatedButton(
                              onPressed: () {
                                JoinRequest request = JoinRequest(
                                  memberType: memberType,
                                  username: _formKey.currentState!.fields['username']!.value,
                                  password: _formKey.currentState!.fields['password']!.value,
                                  passwordRe: _formKey.currentState!.fields['passwordRe']!.value,
                                );
                                if (request.username != null) {
                                  if (request.password == request.passwordRe) {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => PageJoin2(joinRequest: request,))
                                    );
                                  }

                                }
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: colorDark,
                                  onPrimary: colorDark,
                                  side:
                                      BorderSide(color: colorDark, width: 2.0)),
                              child: Text(
                                '다음 단계',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
