import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> _list = ['Apple', 'Banana', "Strawberry", "Watermelon"];

  // 페이지에 적용했을 때는 인피니티 스크롤이 가능한만큼 리스트를 갖고 있어야 새로고침도 적용이 됨
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('RefreshIndicator')),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            _list = [..._list, ..._list];
          });
        },
        child: ListView(
          children: [
            Text('울라'),
            Text('울라'),
            Text('울라'),
            Text('울라'),
            Text('울라'),
            Text('울라'),
            Text('울라'),
            Text('울라'),
          ],
        ),
      ),
    );
  }
}