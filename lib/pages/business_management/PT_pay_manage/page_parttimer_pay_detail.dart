import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/business/business_pay_manage/components_appbar_pay_manage_detail.dart';
import 'package:my_worker_app/components/business/parttimer/pay/components_parttimer_pay_detail.dart';
import 'package:my_worker_app/model/business/parttimer/pay/parttimer_pay_detail.dart';

class PageParttimerPayDetail extends StatefulWidget {
  const PageParttimerPayDetail({super.key});

  @override
  State<PageParttimerPayDetail> createState() => _PageParttimerPayDetailState();
}

class _PageParttimerPayDetailState extends State<PageParttimerPayDetail> {

  List<ParttimerPayDetail> _list = [
    ParttimerPayDetail('홍수경', '2024.03.01', '2024.03.31', '시급', 1300000, 1000000, 0, '0', 0, 0, 0, 0)
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsAppbarPayManageDetail(),
      body: Column(
        children: [
          _parttimerPayDetail(),
        ],
      ),
    );
  }

  Widget _parttimerPayDetail() {
    return ListView.builder(
      itemCount: _list.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext cts, int idx) {
        return ComponentsParttimerPayDetail(
          callback: () {}, parttimerPayDetail: _list[idx],
        );
      }
    );
  }

}
