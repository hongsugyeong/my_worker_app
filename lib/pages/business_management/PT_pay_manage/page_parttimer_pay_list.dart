import 'package:bottom_picker/bottom_picker.dart';
import 'package:bottom_picker/resources/arrays.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/business/parttimer/pay/components_parttimer_pay_list.dart';
import 'package:my_worker_app/model/business/parttimer/pay/parttimer_pay_list.dart';
import 'package:my_worker_app/pages/business_management/PT_pay_manage/page_parttimer_pay_detail.dart';
import 'package:my_worker_app/config/color.dart';

class PageParttimerPayList extends StatefulWidget {
  const PageParttimerPayList({super.key});

  @override
  State<PageParttimerPayList> createState() => _PageParttimerPayListState();
}

class _PageParttimerPayListState extends State<PageParttimerPayList> {

  DateTime? _selectedDate;

  @override
  void initState() {
    super.initState();
  }
  
  List<ParttimerPayList> _list = [
    ParttimerPayList('홍수경', '시급', '1,300,000'),
    ParttimerPayList('이윤진', '일급', '100,000'),
    ParttimerPayList('김기범', '주급', '250,000'),
    ParttimerPayList('노지은', '월급', '2,500,000'),
    ParttimerPayList('홍수경', '시급', '1,300,000'),
    ParttimerPayList('이윤진', '일급', '100,000'),
    ParttimerPayList('김기범', '주급', '250,000'),
    ParttimerPayList('노지은', '월급', '2,500,000'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '급여 관리',),
      body: Column(
        children: [
          _monthPicker(),
          Expanded(
            child: SingleChildScrollView(
              child: _parttimerPayList()
            ),
          )
        ],
      )
    );
  }

  Widget _monthPicker() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            icon: const Icon(
              Icons.keyboard_double_arrow_left,
              color: colorDark,
              size: 30,
            ),
            onPressed: () {
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.keyboard_arrow_left,
              color: colorDark,
              size: 30,
            ),
            onPressed: () {},
          ),
          TextButton(
            style: TextButton.styleFrom(
              textStyle: const TextStyle(
                fontSize: 25,
              ),
            ),
            onPressed: () => _opendDatePicker(context),
            child: const Text('3월', style: TextStyle(color: colorDark),),
          ),
          IconButton(
            icon: const Icon(
              Icons.keyboard_arrow_right,
              color: colorDark,
              size: 30,
            ),
            onPressed: () {
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.keyboard_double_arrow_right,
              color: colorDark,
              size: 30,
            ),
            onPressed: () {
            },
          ),
        ],
      ),
    );
  }

  void _opendDatePicker(BuildContext context) {
    BottomPicker.date(
      dateOrder: DatePickerDateOrder.ymd,
      buttonWidth: 200,
      buttonSingleColor: colorNormal,
      pickerTextStyle: const TextStyle(
        color: Colors.black,
        fontSize: 18
      ),
      onSubmit: (value) {
        setState(() {
          _selectedDate = value;
        });
      },
      bottomPickerTheme: BottomPickerTheme.plumPlate,
      height: 300,
    ).show(context);
  }

  Widget _parttimerPayList () {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
            MaterialPageRoute(builder: (context) => PageParttimerPayDetail())
        );
      },
      child: Column(
        children: [
          ListView.builder(
            itemCount: _list.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext ctx, int idx) {
              return ComponentsParttimerPayList(
                callback: () {}, parttimerPayList: _list[idx],);
            },
          ),
        ],
      ),
    );
  }
}
