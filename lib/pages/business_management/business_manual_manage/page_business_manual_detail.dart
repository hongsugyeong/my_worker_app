import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/business/business_manual_manage/components_appbar_manual_manage_detail.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/model/business/manual/business_manual_detail.dart';
import 'package:my_worker_app/repository/business/repo_business_manual.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class PageBusinessManualDetail extends StatefulWidget {
  const PageBusinessManualDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageBusinessManualDetail> createState() => _PageBusinessManualDetailState();
}

class _PageBusinessManualDetailState extends State<PageBusinessManualDetail> {

  BusinessManualDetail? _detail;

  Future<void> _loadDetail() async {
    await RepoBusinessManual().getProduct(widget.id)
        .then((res) =>
    {
      setState(() {
        _detail = res.data;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    if (_detail == null) {
      return ComponentsLoading();
    } else {
      return Scaffold(
          appBar: ComponentsAppbarManualManageDetail(),
          body: SingleChildScrollView(
              child: _businessManualDetail(context)
          )
      );
    }

  }

  Widget _businessManualDetail(BuildContext context) {
    if (_detail == null) {
      return SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          '${_detail!.title}',
                          style: TextStyle(fontSize: fontSizeLarge,),
                        ),
                      ),
                    ],
                  ),
                  height: 30,
                  margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                ),
                Divider(
                  color: colorLight,
                ),
                Column(
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  '작성자: ',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                                Text(
                                  '${_detail!.memberName}',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                              ],
                            ),
                            margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          ),
                          Container(
                            child: Text(
                              '${_detail!.dateManual}',
                              style: TextStyle(
                                color: Color.fromRGBO(40, 40, 40, 1.0),
                                fontSize: fontSizeSmall,
                              ),
                            ),
                            margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${_detail!.content}')
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    ),
                  ],
                ),
              ],
            ),
          )
      );
    } else {
      return SingleChildScrollView(
          child: Center(
            child: Column(
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        child: Text(
                          '${_detail!.title}',
                          style: TextStyle(fontSize: fontSizeLarge,),
                        ),
                      ),
                    ],
                  ),
                  height: 30,
                  margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
                ),
                Divider(
                  color: colorLight,
                ),
                Column(
                  children: [
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                Text(
                                  '작성자: ',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                                Text(
                                  '${_detail!.memberName}',
                                  style: TextStyle(
                                    color: Color.fromRGBO(40, 40, 40, 1.0),
                                    fontSize: fontSizeSmall,
                                  ),
                                ),
                              ],
                            ),
                            margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                          ),
                          Container(
                            child: Text(
                              '${_detail!.dateManual}',
                              style: TextStyle(
                                color: Color.fromRGBO(40, 40, 40, 1.0),
                                fontSize: fontSizeSmall,
                              ),
                            ),
                            margin: EdgeInsets.fromLTRB(0, 0, 5, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
                    ),
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('${_detail!.content}')
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                    ),
                    Container(
                      child: _imgNull(),
                      width: 150,
                      height: 150,
                    ),
                  ],
                ),
              ],
            ),
          )
      );
    }
  }

  Widget _imgNull() {
    if (_detail?.manualImgUrl == 'null') {
      return SizedBox();
    } else {
      return Image.network(_detail!.manualImgUrl);
      // return Text('${_detail?.manualImgUrl}');
    }
  }

}