import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/business/manual/components_business_manual_list.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/model/business/manual/business_manual_list.dart';
import 'package:my_worker_app/pages/business_management/business_manual_manage/page_business_manual_detail.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/pages/business_management/business_manual_manage/page_business_manual_regist.dart';
import 'package:my_worker_app/repository/business/repo_business_manual.dart';

class PageBusinessManualList extends StatefulWidget {
  const PageBusinessManualList({super.key});

  @override
  State<PageBusinessManualList> createState() => _PageBusinessManualListState();
}

class _PageBusinessManualListState extends State<PageBusinessManualList> {

  final _scrollController = ScrollController();

  List<BusinessManualList> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  @override
  void initState() {
    // super: 부모
    // 부모 먼저 준비
    super.initState();

    // 해당 페이지 죽기 전까지 계속 감시하기
    _scrollController.addListener(() {
      // 스크롤이 바닥 쳤을 때 _loadItems 실행
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems(reFresh: true);
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      await RepoBusinessManual().getList(page: _currentPage)
          .then((res) {
            setState(() {
              _totalPage = res.totalPage.toInt();
              _totalItemCount = res.totalCount.toInt();
              _list = [..._list,...res.list!];

              _currentPage++;
            });
      }).catchError((err) {
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollController.animateTo(
          0,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '매뉴얼 관리',),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            _loadItems(reFresh: true);
          });
        },
        child: ListView(
          controller: _scrollController,
          children: [ _businessManualList(), ]
        ),
      ),
      floatingActionButton: FloatingActionButton.small(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const PageBusinessManualRegist())
          );
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: colorNormal,
        shape: (RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      ),
    );
  }

  Widget _businessManualList() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            itemCount: _list.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext ctx, int idx) {
              return ComponentsBusinessManualList(
                callback: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageBusinessManualDetail(id: _list[idx].id,)));
                }, businessManualList: _list[idx],);
            },
          ),
        ],
      );
    } else {
      return SizedBox(
        child: ComponentsLoading()
      );
    }
  }
}
