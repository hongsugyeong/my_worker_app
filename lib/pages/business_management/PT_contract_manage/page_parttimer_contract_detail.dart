import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/business/PT_contract_manage/components_appbar_contract_manage_detail.dart';
import 'package:my_worker_app/components/button/components_text_detail.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/model/business/parttimer/contract/parttimer_contract_detail.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_parttimer_contract.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class PageParttimerContractDetail extends StatefulWidget {
  const PageParttimerContractDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageParttimerContractDetail> createState() =>
      _PageParttimerContractDetailState();
}

class _PageParttimerContractDetailState
    extends State<PageParttimerContractDetail> {
  ParttimerContractDetail? _detail;

  Future<void> _loadDetail() async {
    await RepoParttimerContract().getProduct(widget.id).then((res) => {
          setState(() {
            _detail = res.data;
          })
        });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsAppbarContractManageDetail(),
      body: SingleChildScrollView(
        child: _contractImg(context),
      ),
    );
  }

  Widget _contractImg(BuildContext context) {
    if (_detail == null) {
      return SizedBox(child: ComponentsLoading());
    } else {
      return SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        '${_detail!.businessMemberName}',
                        style: TextStyle(
                            fontSize: fontSizeLarge,
                            fontWeight: FontWeight.bold),
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                    ),
                    Text(
                      // 나중에 DB 올라가면 값 받아오기!!!
                      '알바생',
                      style: TextStyle(
                          color: Color.fromRGBO(64, 64, 64, 1.0),
                          fontSize: fontSizeSmall),
                    ),
                  ],
                ),
                height: 30,
                margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
              ),
              Divider(
                color: colorLight,
              ),
              Container(
                child: Column(
                  children: [
                    ComponentsTextDetail(
                      title: '근로 계약 일자',
                      data: '${_detail!.dateContract}',
                      margin: EdgeInsets.fromLTRB(35, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '급여 책정 방식',
                      data: '${_detail!.payType}',
                      margin: EdgeInsets.fromLTRB(35, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '급여 금액',
                      data: '${_detail!.payPrice}',
                      margin: EdgeInsets.fromLTRB(65, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '근로 시작일',
                      data: '${_detail!.dateWorkStart}',
                      margin: EdgeInsets.fromLTRB(50, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '근로 종료일',
                      data: '${_detail!.dateWorkEnd}',
                      margin: EdgeInsets.fromLTRB(50, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '휴게 시간',
                      data: '${_detail!.restTime}',
                      margin: EdgeInsets.fromLTRB(65, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '세금공제 유형',
                      data: '${_detail!.insurance}',
                      margin: EdgeInsets.fromLTRB(35, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '근무 요일',
                      data: '${_detail!.workDay}',
                      margin: EdgeInsets.fromLTRB(65, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '일 근무 시간',
                      data: '${_detail!.workTime}',
                      margin: EdgeInsets.fromLTRB(45, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '주 근무 시간',
                      data: '${_detail!.weekWorkTime}',
                      margin: EdgeInsets.fromLTRB(45, 0, 0, 0),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('주휴수당 여부'),
                          Container(
                            child: Text('${_detail!.isWeekPay}'),
                            margin: EdgeInsets.fromLTRB(35, 0, 0, 0),
                          )
                        ],
                      ),
                      height: 40,
                    ),
                    ComponentsTextDetail(
                      title: '식대',
                      data: '${_detail!.mealPay}',
                      margin: EdgeInsets.fromLTRB(90, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '임금 지급일',
                      data: '${_detail!.wagePayment}',
                      margin: EdgeInsets.fromLTRB(50, 0, 0, 0),
                    ),
                    ComponentsTextDetail(
                      title: '임금 계좌번호',
                      data: '${_detail!.wageAccountNumber}',
                      margin: EdgeInsets.fromLTRB(40, 0, 0, 0),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('근로계약서 사본'),
                          Container(
                            child: _imgPopup(context),
                            margin: EdgeInsets.fromLTRB(25, 0, 0, 0),
                            width: 130,
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(15, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 1.0,
              ),
            ],
          ),
        ),
      );
    }
  }

  Widget _imgPopup(BuildContext context) {
    return GestureDetector(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const SizedBox(height: 10),
        ElevatedButton(
          onPressed: () => showDialog<String>(
            context: context,
            builder: (BuildContext context) => Dialog.fullscreen(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _imgNull(),
                  const SizedBox(
                    height: 15,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(
                      '닫기',
                      style: TextStyle(color: colorDark),
                    ),
                  )
                ],
              ),
            ),
          ),
          style: ElevatedButton.styleFrom(
              primary: Colors.white,
              onPrimary: colorDark,
              side: BorderSide(color: colorDark, width: 2.0)),
          child: const Text(
            '이미지로 보기',
            style: TextStyle(fontSize: 13),
          ),
        ),
      ],
    ));
  }

  Widget _imgNull () {
    if (_detail?.contractCopyImgUrl == 'null') {
      return Text('등록된 이미지 없음');
    } else {
      return Image.network(_detail!.contractCopyImgUrl);
    }
  }

}
