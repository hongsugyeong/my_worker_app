import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/button/components_camera.dart';
import 'package:my_worker_app/components/button/components_dropdown.dart';
import 'package:my_worker_app/components/button/components_text_form_title.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/business/parttimer/contract/parttimer_contract_regist_request.dart';
import 'package:my_worker_app/pages/business_management/PT_contract_manage/page_parttimer_contract_list.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_parttimer_contract.dart';

class PageParttimerContractRegist extends StatefulWidget {
  const PageParttimerContractRegist({super.key,});

  @override
  State<PageParttimerContractRegist> createState() =>
      _PageParttimerContractRegistState();
}

class _PageParttimerContractRegistState
    extends State<PageParttimerContractRegist> {

  final _formKey = GlobalKey<FormBuilderState>();
  List<String> payType = ['TIME_WAGE', 'DAY_WAGE', 'WEEK_WAGE', 'MONTH_WAGE', 'YEAR_WAGE',];
  List<String> insurance = ['BASIC_INSURANCE', 'FOUR_INSURANCE',];

  Future<void> sendContract(ParttimerContractRegistRequest request) async {
    await RepoParttimerContract().setContract(request, 1)
    // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      request.dateContract = '';
      request.payType = '';
      request.payPrice = '' as num;
      request.dateWorkStart = '';
      request.dateWorkEnd = '';
      request.restTime = '' as num;
      request.insurance = '';
      request.workDay = '';
      request.workTime = '' as num;
      request.weekWorkTime = '' as num;
      request.isWeekPay = '' as bool;
      request.mealPay = '' as num;
      request.wagePayment = '';
      request.wageAccountNumber = '';
      request.contractCopyImgUrl = '';
      print("성공");
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
              const PageParttimerContractList()));
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '근로계약서 등록',),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ComponentsTextFormTitle(title: '근로 계약 일자', name: 'dateContract', hintText: '2000-11-05', maxLength: 20, margin: EdgeInsets.fromLTRB(20, 10, 0, 10), height: 60,),
                    ComponentsDropdown(
                      marginTitle: EdgeInsets.fromLTRB(20, 0, 0, 0),
                      width: MediaQuery.of(context).size.width / 1.7,
                      title: '급여 책정 방식',
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      name: 'payType', initialValue: 'TIME_WAGE',
                      suffix: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          _formKey.currentState!.fields['payType']
                              ?.reset();
                        },
                      ),
                      hintText: '급여 책정 방식을 선택해주세요.',
                      items: payType
                          .map((gender) => DropdownMenuItem(
                        alignment: AlignmentDirectional.center,
                        value: gender,
                        child: Text(gender),
                      ))
                          .toList(),
                    ),
                    ComponentsTextFormTitle(title: '급여 금액', name: 'payPrice', hintText: '급여금액을 입력해주세요.', maxLength: 20, margin: EdgeInsets.fromLTRB(50, 20, 0, 0), height: 60,),
                    ComponentsTextFormTitle(title: '근로 시작일', name: 'dateWorkStart', hintText: '2000-11-05', maxLength: 20, margin: EdgeInsets.fromLTRB(35, 10, 0, 0), height: 60,),
                    ComponentsTextFormTitle(title: '근로 종료일', name: 'dateWorkEnd', hintText: '2000-11-05', maxLength: 20, margin: EdgeInsets.fromLTRB(35, 10, 0, 0), height: 60,),
                    ComponentsTextFormTitle(title: '휴게 시간', name: 'restTime', hintText: '휴게 시간을 입력해주세요.', maxLength: 20, margin: EdgeInsets.fromLTRB(50, 10, 0, 10), height: 60,),
                    ComponentsDropdown(
                      marginTitle: EdgeInsets.fromLTRB(20, 0, 0, 0),
                      width: MediaQuery.of(context).size.width / 1.6,
                      title: '세금 공제 유형',
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      name: 'insurance', initialValue: 'BASIC_INSURANCE',
                      suffix: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          _formKey.currentState!.fields['insurance']
                              ?.reset();
                        },
                      ),
                      hintText: '세금공제 유형을 선택해주세요.',
                      items: insurance
                          .map((gender) => DropdownMenuItem(
                        alignment: AlignmentDirectional.center,
                        value: gender,
                        child: Text(gender),
                      ))
                          .toList(),
                    ),
                    ComponentsTextFormTitle(title: '근무 요일', name: 'workDay', hintText: '월 수 금', maxLength: 20, margin: EdgeInsets.fromLTRB(50, 20, 0, 0), height: 60,),
                    ComponentsTextFormTitle(title: '일 근무 시간', name: 'workTime', hintText: '일 근무 시간을 입력해주세요.', maxLength: 20, margin: EdgeInsets.fromLTRB(35, 10, 0, 0), height: 60,),
                    ComponentsTextFormTitle(title: '주 근무 시간', name: 'weekWorkTime', hintText: '주 근무 시간을 입력해주세요.', maxLength: 20, margin: EdgeInsets.fromLTRB(35, 10, 0, 0), height: 60,),
                    FormBuilderCheckbox(name: 'isWeekPay', title: Text('주휴수당 여부', style: TextStyle(fontSize: fontSizeMedium),), ),
                    ComponentsTextFormTitle(title: '식대', name: 'mealPay', hintText: '식대를 입력해주세요.', maxLength: 20, margin: EdgeInsets.fromLTRB(85, 10, 0, 0), height: 60,),
                    ComponentsTextFormTitle(title: '임금 지급일', name: 'wagePayment', hintText: '2000-11-05', maxLength: 20, margin: EdgeInsets.fromLTRB(35, 10, 0, 0), height: 60,),
                    ComponentsTextFormTitle(title: '임금 계좌번호', name: 'wageAccountNumber', hintText: '농협 000-0000-0000-00', maxLength: 20, margin: EdgeInsets.fromLTRB(20, 10, 0, 0), height: 60,),
                    Container(
                      child: Row(
                        children: [
                          Container(
                            child: Text(
                              '근로계약서',
                              style: TextStyle(color: Color.fromRGBO(64, 64, 64, 1.0), fontSize: fontSizeMedium),
                            ),
                            margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
                    ),
                    ComponentsCamera(name: 'contractCopyImgUrl',),
                  ],
                ),
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('등록하시겠습니까?', style: TextStyle(color: Color.fromRGBO(64, 64, 64, 1.0)),),
                        // content: const Text('AlertDialog description'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'Cancel'),
                            child: const Text('취소', style: TextStyle(color: colorNormal),),
                          ),
                          TextButton(
                            onPressed: () {
                              ParttimerContractRegistRequest request =
                                  ParttimerContractRegistRequest(
                                    _formKey.currentState!.fields['dateContract']!.value,
                                    _formKey.currentState!.fields['payType']!.value,
                                    _formKey.currentState!.fields['payPrice']!.value,
                                    _formKey.currentState!.fields['dateWorkStart']!.value,
                                    _formKey.currentState!.fields['dateWorkEnd']!.value,
                                    _formKey.currentState!.fields['restTime']!.value,
                                    _formKey.currentState!.fields['insurance']!.value,
                                    _formKey.currentState!.fields['workDay']!.value,
                                    _formKey.currentState!.fields['workTime']!.value,
                                    _formKey.currentState!.fields['weekWorkTime']!.value,
                                    _formKey.currentState!.fields['isWeekPay']!.value,
                                    _formKey.currentState!.fields['mealPay']!.value,
                                    _formKey.currentState!.fields['wagePayment']!.value,
                                    _formKey.currentState!.fields['wageAccountNumber']!.value,
                                    _formKey.currentState!.fields['contractCopyImgUrl']?.value.toString(),
                                  );
                              sendContract(request);
                            },
                            child: const Text('확인', style: TextStyle(color: colorDark)),
                          ),
                        ],
                        backgroundColor: Colors.white,
                      ),
                    );
                  }
                },
                style:
                ElevatedButton.styleFrom(
                    primary: Colors.white,
                    onPrimary: colorDark,
                    side: BorderSide(color: colorDark, width: 2.0)
                ),
                child: const Text('등록'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
