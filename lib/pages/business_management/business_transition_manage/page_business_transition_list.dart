import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/business/transition/components_business_transition_list.dart';
import 'package:my_worker_app/model/business/transition/business_transition_list.dart';
import 'package:my_worker_app/model/business/transition/business_transition_year_month.dart';
import 'package:my_worker_app/pages/business_management/business_transition_manage/page_business_transition_regist.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_business_transition.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/config/color.dart';

class PageBusinessTransitionList extends StatefulWidget {
  const PageBusinessTransitionList({super.key});

  @override
  State<PageBusinessTransitionList> createState() =>
      _PageBusinessTransitionListState();
}

class _PageBusinessTransitionListState
    extends State<PageBusinessTransitionList> {

  List<BusinessTransitionList> _detail = [];

  List<BusinessTransitionYearMonth> _month = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedDay = _focusedDay;
    _selectedEvents = ValueNotifier(_getEventsForDay(_selectedDay!));

    _loadItems();
  }

  String month = DateFormat('yyyy').format(DateTime.now());
  String year = DateFormat('MM').format(DateTime.now());

  Future<void> _loadItems() async {
    await RepoBusinessTransition().getList(
        int.parse(month),
        int.parse(year)
    )
        .then((res) {
          setState(() {
            _detail = res.list!;
            print('성공 축하');
          });
    }).catchError((err) {
      debugPrint(err);
      print('실패함');
    });
  }

  // 날짜 원하는 형식으로 바꿈
  String date = DateFormat('yyyy-MM-dd').format(DateTime.now()).toString();

  Future<void> _loadDetail() async {
    // 선택한 날짜를 받아야됨 => 셀렉트데이트가 필요한가?
    await RepoBusinessTransition().getDetail(
        DateFormat('yyyy-MM-dd').format(_selectedDay!)
    )
        .then((res) {
          setState(() {
            _detail = res.list!;
            addDialog(context);
            print('공성');
          });
    }).catchError((err) {
      debugPrint(err);
      print('패실');
    });
  }

  DateTime selectedDate = DateTime.utc(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
  );

  late final ValueNotifier<List> _selectedEvents;
  CalendarFormat _calendarFormat = CalendarFormat.month;
  DateTime _focusedDay = DateTime.now();
  DateTime? _selectedDay;
  int selectMarker = 0;

  Map<DateTime, List<Event>> events = {
    DateTime.utc(2024, 5, 1): [ Event('title'),],
    DateTime.utc(2024, 5, 2): [ Event('title2')],
    DateTime.utc(2024, 5, 3): [ Event('title3')],
    DateTime.utc(2024, 5, 4): [ Event('title'),],
    DateTime.utc(2024, 5, 5): [ Event('title2')],
    DateTime.utc(2024, 5, 6): [ Event('title3')],
    DateTime.utc(2024, 5, 7): [ Event('title3')],
    DateTime.utc(2024, 5, 8): [ Event('title3')],
  };

  List<Event> _getEventsForDay(DateTime day) {
    return events[day] ?? [];
  }

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: ComponentsBusinessList(menu: '인수인계 관리',),
      body: Builder(
        builder: (context) =>
            Center(
              child: TableCalendar(
                firstDay: DateTime.utc(2001, 07, 23),
                lastDay: DateTime.utc(2030, 09, 09),
                focusedDay: DateTime.now(),
                headerStyle: HeaderStyle(
                  titleCentered: true,
                  titleTextStyle: TextStyle(
                      color: colorDark,
                      fontSize: fontSizeMedium
                  ),
                  formatButtonVisible: false,
                ),
                calendarStyle: CalendarStyle(
                  todayDecoration: BoxDecoration(
                      color: colorNormal,
                      borderRadius: BorderRadius.circular(20)
                  ),
                  selectedDecoration: BoxDecoration(
                      color: colorLight,
                      borderRadius: BorderRadius.circular(20)
                  ),
                  markerSize: 10,
                  markerDecoration: BoxDecoration(
                      color: colorNormal,
                      shape: BoxShape.circle
                  ),
                ),
                calendarBuilders: CalendarBuilders(
                  dowBuilder: (context, day) {
                    switch (day.weekday) {
                      case 1:
                        return Center(child: Text('월'),);
                      case 2:
                        return Center(child: Text('화'),);
                      case 3:
                        return Center(child: Text('수'),);
                      case 4:
                        return Center(child: Text('목'),);
                      case 5:
                        return Center(child: Text('금'),);
                      case 6:
                        return Center(child: Text('토'),);
                      case 7:
                        return Center(
                          child: Text('일', style: TextStyle(color: Colors
                              .red),),);
                    }
                  },
                ),
                selectedDayPredicate: (day) {
                  return isSameDay(_selectedDay, day);
                },
                onDaySelected: (selectedDay, focusedDay) {
                    setState(() {
                      _selectedDay = selectedDay;
                      _focusedDay = focusedDay;
                    });
                    _selectedEvents.value = _getEventsForDay(selectedDay);
                    // 날짜 선택시 보이는 거 여기 추가
                    _loadDetail();
                },
                onFormatChanged: (format) {
                  if (_calendarFormat != format) {
                    setState(() {
                      _calendarFormat = format;
                    });
                  }
                },
                onPageChanged: (focusedDay) {
                  _focusedDay = focusedDay;
                },
                eventLoader: _getEventsForDay,
              ),
            ),
      ),
    );
  }
  void addDialog(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (context) {
        return SingleChildScrollView(
          child: Column(
            children: [
              Container(
                color: Color(0xFF737373),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: ListView.builder(
                    itemCount: _detail.length,
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext ctx, int idx) {
                      return ComponentsBusinessTransitionList(
                        businessTransitionList: _detail[idx],);
                    },
                  ),
                ),
              ),
              _registButton(),
            ],
          ),
        );
      },
    );
  }

  Widget _registButton() {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => (PageBusinessTransitionRegist())));
      },
      child: Container(
        child: Icon(Icons.add, color: colorNormal,),
        height: 40,
      ),
    );
  }

}

class Event {
  String title;

  Event(this.title);
}
