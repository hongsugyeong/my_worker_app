import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/business/transition/business_transition_detail.dart';
import 'package:my_worker_app/model/business/transition/business_transition_put_request.dart';
import 'package:my_worker_app/pages/business_management/business_transition_manage/page_business_transition_list.dart';
import 'package:my_worker_app/pages/page_index.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_business_transition.dart';

class PageBusinessTransitionPut extends StatefulWidget {
  const PageBusinessTransitionPut({super.key, required this.id});

  final num id;

  @override
  State<PageBusinessTransitionPut> createState() => _PageBusinessTransitionPutState();
}

class _PageBusinessTransitionPutState extends State<PageBusinessTransitionPut> {

  final _formKey = GlobalKey<FormBuilderState>();
  BusinessTransitionDetail? _putDetail;

  @override
  void initState() {
    super.initState();

    _loadDetail();
  }

  Future<void> _loadDetail() async {
    await RepoBusinessTransition().getTransition(widget.id)
        .then((res) => {
      setState(() {
        _putDetail = res.data;
      })
    });
  }

  Future<void> putTransition (BusinessTransitionPutRequest request) async {
    await RepoBusinessTransition().putTransition(request, widget.id)
        .then((value) {
      request.content = '';
      print("성공");
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PageIndex())
      );
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_putDetail != null) {
      return Scaffold(
        body: _regist(context),
      );
    } else {
      return ComponentsLoading();
    }
  }

  Widget _regist(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '인수인계 수정',),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              child: Container(
                child: FormBuilderTextField(
                  initialValue: '${_putDetail!.content}',
                  name: 'content',
                  maxLines: null,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: '인수인계를 작성해주세요.',
                  ),
                ),
                margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                height: 450,
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.saveAndValidate()) {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text(
                                  '등록하시겠습니까?',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0)),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text(
                                      '취소',
                                      style: TextStyle(
                                          color:
                                          Color.fromRGBO(63, 114, 175, 1.0)),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      BusinessTransitionPutRequest request =
                                      BusinessTransitionPutRequest(
                                        _formKey.currentState!.fields['content']!.value,
                                      );
                                      putTransition(request);
                                    },
                                    child: const Text('확인',
                                        style: TextStyle(
                                            color:
                                            Color.fromRGBO(17, 45, 78, 1.0))),
                                  ),
                                ],
                                backgroundColor: Colors.white,
                              ),
                            );
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            onPrimary: Color.fromRGBO(17, 45, 78, 1.0),
                            side: BorderSide(
                                color: Color.fromRGBO(17, 45, 78, 1.0),
                                width: 2.0)),
                        child: const Text('등록'),
                      ),
                    ),
                    margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      onPressed: () {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              '취소하시겠습니까?',
                              style: TextStyle(
                                  color: Color.fromRGBO(64, 64, 64, 1.0)),
                            ),
                            // content: const Text('AlertDialog description'),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'Cancel'),
                                child: const Text(
                                  '취소',
                                  style: TextStyle(color: colorNormal),
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => (PageBusinessTransitionList())));
                                },
                                child: const Text('확인',
                                    style: TextStyle(
                                        color: Color.fromRGBO(17, 45, 78, 1.0))),
                              ),
                            ],
                            backgroundColor: Colors.white,
                          ),
                        );
                      },
                      style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          onPrimary: colorNormal,
                          side: BorderSide(color: colorNormal, width: 2.0)),
                      child: const Text('취소'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

}
