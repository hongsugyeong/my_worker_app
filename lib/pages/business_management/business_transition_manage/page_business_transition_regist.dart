import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/model/business/transition/business_transition_regist_request.dart';
import 'package:my_worker_app/pages/business_management/business_transition_manage/page_business_transition_list.dart';
import 'package:my_worker_app/pages/page_index.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_business_transition.dart';

class PageBusinessTransitionRegist extends StatefulWidget {
  const PageBusinessTransitionRegist({super.key});

  @override
  State<PageBusinessTransitionRegist> createState() => _PageBusinessTransitionRegistState();
}

class _PageBusinessTransitionRegistState extends State<PageBusinessTransitionRegist> {

  final _formKey = GlobalKey<FormBuilderState>();

  // request 받아서 값 넣어주기
  Future<void> sendManual(BusinessTransitionRegistRequest request) async {
    await RepoBusinessTransition().setTransition(request)
    // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      request.content = '';
      showToast();
      print("등록 성공");
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PageIndex())
      );
    }).catchError((onError) {
      print("등록 실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _regist(context),
    );
  }

  Widget _regist(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '인수인계 등록',),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              child: Container(
                    child: FormBuilderTextField(
                      keyboardType: TextInputType.text,
                      name: 'content',
                      maxLines: null,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: '내용을 입력해 주세요',
                      ),
                    ),
                    margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
                    height: 450,
                  ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.saveAndValidate()) {
                            showDialog<String>(
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                title: const Text(
                                  '등록하시겠습니까?',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0)),
                                ),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () =>
                                        Navigator.pop(context, 'Cancel'),
                                    child: const Text(
                                      '취소',
                                      style: TextStyle(
                                          color:
                                          Color.fromRGBO(63, 114, 175, 1.0)),
                                    ),
                                  ),
                                  TextButton(
                                    onPressed: () {
                                      BusinessTransitionRegistRequest request =
                                      BusinessTransitionRegistRequest(
                                        _formKey.currentState!.fields['content']!.value,
                                      );
                                      sendManual(request);
                                    },
                                    child: const Text('확인',
                                        style: TextStyle(
                                            color:
                                            Color.fromRGBO(17, 45, 78, 1.0))),
                                  ),
                                ],
                                backgroundColor: Colors.white,
                              ),
                            );
                          }
                        },
                        style: ElevatedButton.styleFrom(
                            primary: Colors.white,
                            onPrimary: Color.fromRGBO(17, 45, 78, 1.0),
                            side: BorderSide(
                                color: Color.fromRGBO(17, 45, 78, 1.0),
                                width: 2.0)),
                        child: const Text('등록'),
                      ),
                    ),
                    margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: ElevatedButton(
                      onPressed: () {
                        showDialog<String>(
                          context: context,
                          builder: (BuildContext context) => AlertDialog(
                            title: const Text(
                              '취소하시겠습니까?',
                              style: TextStyle(
                                  color: Color.fromRGBO(64, 64, 64, 1.0)),
                            ),
                            // content: const Text('AlertDialog description'),
                            actions: <Widget>[
                              TextButton(
                                onPressed: () => Navigator.pop(context, 'Cancel'),
                                child: const Text(
                                  '취소',
                                  style: TextStyle(color: colorNormal),
                                ),
                              ),
                              TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => (PageBusinessTransitionList())));
                                },
                                child: const Text('확인',
                                    style: TextStyle(
                                        color: Color.fromRGBO(17, 45, 78, 1.0))),
                              ),
                            ],
                            backgroundColor: Colors.white,
                          ),
                        );
                      },
                      style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          onPrimary: colorNormal,
                          side: BorderSide(color: colorNormal, width: 2.0)),
                      child: const Text('취소'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

void showToast() {
  Fluttertoast.showToast(
      msg: '성공했습니다.',
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.black38,
      fontSize: fontSizeMedium,
      textColor: Colors.black,
      toastLength: Toast.LENGTH_SHORT,
      webBgColor: "linear-gradient(to right, #dbe2ef, #dbe2ef)"
  );
}
