import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/business/product/components_business_product_list.dart';
import 'package:my_worker_app/components/button/components_text_form_title_maxLength.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/model/business/product/business_product_list.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/model/business/product/business_product_regist_request.dart';
import 'package:my_worker_app/pages/page_index.dart';
import 'package:my_worker_app/repository/business/repo_product.dart';

class PageBusinessProductList extends StatefulWidget {
  const PageBusinessProductList({super.key});

  @override
  State<PageBusinessProductList> createState() =>
      _PageBusinessProductListState();
}

class _PageBusinessProductListState extends State<PageBusinessProductList> {
  final _scrollController = ScrollController();

  final _formKey = GlobalKey<FormBuilderState>();

  List<BusinessProductList> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  @override
  void initState() {
    // super: 부모
    // 부모 먼저 준비
    super.initState();

    // 해당 페이지 죽기 전까지 계속 감시하기
    _scrollController.addListener(() {
      // 스크롤이 바닥 쳤을 때 _loadItems 실행
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems(reFresh: true);
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      await RepoProduct().getList(page: _currentPage).then((res) {
        setState(() {
          _totalPage = res.totalPage.toInt();
          _totalItemCount = res.totalCount.toInt();
          _list = [..._list, ...res.list!];

          _currentPage++;
        });
      }).catchError((err) {
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollController.animateTo(0,
          duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  Future<void> sendProduct(BusinessProductRegistRequest request) async {
    await RepoProduct()
        .setProduct(request)
        // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      print("성공함");
      Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => PageIndex())
      );
    }).catchError((onError) {
      print(onError);
      print("실패함");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(
        menu: '제품 관리',
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            _loadItems(reFresh: true);
          });
        },
        child: ListView(
          controller: _scrollController,
          children: [_businessProductList(),],
        ),
      ),
      floatingActionButton: FloatingActionButton.small(
        // 사업장 추가 버튼 어떻게 할지 좀 더 고민해보기
        onPressed: () {
          _test(context);
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: colorLight,
        shape: (RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      ),
    );
  }

  Widget _registButton() {
    return GestureDetector(
      onTap: () => _test(context),
      child: Container(
        child: Icon(
          Icons.add,
          color: colorNormal,
        ),
        height: 40,
      ),
    );
  }

  Widget _businessProductList() {
    if (_totalItemCount > 0) {
      return Column(
        children: [
          ListView.builder(
            itemCount: _list.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext ctx, int idx) {
              return ComponentsBusinessProductList(
                  businessProductList: _list[idx], id: _list[idx].id);
            },
          ),
        ],
      );
    } else {
      return SizedBox(child: ComponentsLoading());
    }
  }

  void _test(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return SingleChildScrollView(
            child: AlertDialog(
              backgroundColor: Colors.white,
              title: Text('제품 등록'),
              actions: [
                Center(
                    child: Column(
                  children: [
                    FormBuilder(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  '제품명',
                                  style: TextStyle(
                                      color: Color.fromRGBO(64, 64, 64, 1.0),
                                      fontSize: fontSizeMedium),
                                ),
                                Container(
                                  child: FormBuilderTextField(
                                    name: 'productName',
                                    decoration: InputDecoration(
                                      hintText: '불닭볶음면',
                                      hintStyle: TextStyle(height: 1.0),
                                      enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide: BorderSide(
                                            color: colorDark, width: 2),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide: BorderSide(
                                            color: colorNormal, width: 2),
                                      ),
                                    ),
                                    textInputAction: TextInputAction.next,
                                    validator: (String? value) {
                                      if (value == null || value.isEmpty) {
                                        return '필수값입니다. 다시 확인해 주세요.';
                                      }
                                      return null;
                                    },
                                  ),
                                  width: MediaQuery.of(context).size.width / 2.6,
                                  height: 45,
                                  margin: EdgeInsets.fromLTRB(40, 0, 0, 0),
                                ),
                              ],
                            ),
                          ),
                          ComponentsTextFormTitleNumber(
                            title: '최소 수량',
                            name: 'minQuantity',
                            hintText: '숫자만 입력해주세요.',
                            margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          ),
                          ComponentsTextFormTitleNumber(
                            title: '현재 수량',
                            name: 'nowQuantity',
                            hintText: '숫자만 입력해주세요.',
                            margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 16.0),
                              child: ElevatedButton(
                                onPressed: () {
                                  if (_formKey.currentState!.saveAndValidate()) {
                                    showDialog<String>(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          AlertDialog(
                                            title: const Text(
                                              '등록하시겠습니까?',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      64, 64, 64, 1.0)),
                                            ),
                                            actions: <Widget>[
                                              TextButton(
                                                onPressed: () =>
                                                    Navigator.pop(context, 'Cancel'),
                                                child: const Text(
                                                  '취소',
                                                  style: TextStyle(
                                                      color: Color.fromRGBO(
                                                          63, 114, 175, 1.0)),
                                                ),
                                              ),
                                                TextButton(
                                                  onPressed: () {
                                                    BusinessProductRegistRequest
                                                        request =
                                                        BusinessProductRegistRequest(
                                                      _formKey.currentState!.fields['productName']!.value,
                                                      int.parse(_formKey.currentState!.fields['minQuantity']!.value),
                                                      int.parse(_formKey.currentState!.fields['nowQuantity']!.value,)
                                                    );
                                                    sendProduct(request);
                                                  },
                                                  child: const Text('확인',
                                                      style: TextStyle(
                                                          color: Color.fromRGBO(
                                                              17, 45, 78, 1.0))),
                                                ),
                                              ],
                                        backgroundColor: Colors.white,
                                      ),
                                    );
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.white,
                                    onPrimary: Color.fromRGBO(17, 45, 78, 1.0),
                                    side: BorderSide(
                                        color: Color.fromRGBO(17, 45, 78, 1.0),
                                        width: 2.0)),
                                child: const Text('등록'),
                              ),
                            ),
                            margin: EdgeInsets.fromLTRB(0, 0, 60, 0),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: ElevatedButton(
                              onPressed: () {
                                showDialog<String>(
                                  context: context,
                                  builder: (BuildContext context) => AlertDialog(
                                    title: const Text(
                                      '취소하시겠습니까?',
                                      style: TextStyle(
                                          color: Color.fromRGBO(64, 64, 64, 1.0)),
                                    ),
                                    // content: const Text('AlertDialog description'),
                                    actions: <Widget>[
                                      TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'Cancel'),
                                        child: const Text(
                                          '취소',
                                          style: TextStyle(color: colorNormal),
                                        ),
                                      ),
                                      TextButton(
                                        onPressed: () {
                                          Navigator.pop(context, 'OK');
                                        },
                                        child: const Text('확인',
                                            style: TextStyle(
                                                color: Color.fromRGBO(
                                                    17, 45, 78, 1.0))),
                                      ),
                                    ],
                                    backgroundColor: Colors.white,
                                  ),
                                );
                              },
                              style: ElevatedButton.styleFrom(
                                  primary: Colors.white,
                                  onPrimary: colorNormal,
                                  side:
                                      BorderSide(color: colorNormal, width: 2.0)),
                              child: const Text('취소'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ))
              ],
            ),
          );
        });
  }
}
