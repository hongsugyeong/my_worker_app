import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/business/product/components_business_product_put.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/model/business/product/business_product_list.dart';
import 'package:my_worker_app/model/business/product/business_product_put_request.dart';
import 'package:my_worker_app/pages/business_management/business_product_manage/page_business_product_list.dart';
import 'package:my_worker_app/repository/business/repo_product.dart';

class PageBusinessProductPut extends StatefulWidget {
  const PageBusinessProductPut({super.key, required this.id});

  final num id;

  @override
  State<PageBusinessProductPut> createState() =>
      _PageBusinessProductPutState();
}

class _PageBusinessProductPutState extends State<PageBusinessProductPut> {
  final _scrollController = ScrollController();

  final _formKey = GlobalKey<FormBuilderState>();

  List<BusinessProductList> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  @override
  void initState() {
    // super: 부모
    // 부모 먼저 준비
    super.initState();

    // 해당 페이지 죽기 전까지 계속 감시하기
    _scrollController.addListener(() {
      // 스크롤이 바닥 쳤을 때 _loadItems 실행
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems(reFresh: true);
  }

  Future<void> _loadItems({bool reFresh = false}) async {
    if (reFresh) {
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      await RepoProduct().getList(page: _currentPage).then((res) {
        setState(() {
          _totalPage = res.totalPage.toInt();
          _totalItemCount = res.totalCount.toInt();
          _list = [..._list, ...res.list!];

          _currentPage++;
        });
      }).catchError((err) {
        debugPrint(err);
      });
    }

    if (reFresh) {
      _scrollController.animateTo(0,
          duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  Future<void> _putNowQuantity(BusinessProductPutRequest request) async {
    await RepoProduct().putNowQuantity(request, widget.id).then((value) {
      print("성공");
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const PageBusinessProductList()));
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(
        menu: '제품 관리',
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            _loadItems(reFresh: true);
          });
        },
        child: ListView(
          controller: _scrollController,
          children: [_businessProductList(),],
        ),
      ),
    );
  }

  Widget _businessProductList() {
    if (_totalItemCount > 0) {
      return Column(
        children: [
          ListView.builder(
            itemCount: _list.length,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext ctx, int idx) {
              return ComponentsBusinessProductPut(
                  businessProductList: _list[idx], id: _list[idx].id);
            },
          ),
        ],
      );
    } else {
      return SizedBox(child: ComponentsLoading());
    }
  }
}
