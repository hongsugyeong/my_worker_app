import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/button/components_dropdown.dart';
import 'package:my_worker_app/components/button/components_text_form_title.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/model/business/parttimer/join/business_parttimer_regist_request.dart';
import 'package:my_worker_app/pages/business_management/PT_info_manage/page_parttimer_info_list.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_parttimer_join.dart';

class PageParttimerJoin extends StatefulWidget {
  const PageParttimerJoin({super.key});

  @override
  State<PageParttimerJoin> createState() => _PageParttimerJoinState();
}

class _PageParttimerJoinState extends State<PageParttimerJoin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> sendManual(BusinessParttimerRegistRequest request) async {
    // id 값 억지로 넣어준 거임 바꿔주기
    await RepoParttimerJoin().setParttimer(request)
        // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      request.phoneNumber = '';
      request.position = '';
      request.etc = '';
      print("성공");
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const PageParttimerInfoList()));
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(
        menu: '알바생 등록',
      ),
      body: SingleChildScrollView(
          child: Container(
        margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
        child: Column(
          children: [
            FormBuilder(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '전화번호',
                            style: TextStyle(
                                color: Color.fromRGBO(64, 64, 64, 1.0),
                                fontSize: fontSizeMedium),
                          ),
                          Container(
                            child: FormBuilderTextField(
                              maxLength: 13,
                              keyboardType: TextInputType.number,
                              name: 'phoneNumber',
                              decoration: InputDecoration(
                                hintText: '010-0000-0000',
                                hintStyle: TextStyle(height: 1.0),
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide:
                                      BorderSide(color: colorDark, width: 2),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(20),
                                  borderSide:
                                      BorderSide(color: colorNormal, width: 2),
                                ),
                              ),
                              textInputAction: TextInputAction.next,
                              validator: (String? value) {
                                if (value == null || value.isEmpty) {
                                  return '필수값입니다. 다시 확인해 주세요.';
                                }
                                return null;
                              },
                            ),
                            width: MediaQuery.of(context).size.width / 1.7,
                            height: 70,
                            margin: EdgeInsets.fromLTRB(40, 10, 0, 0),
                          ),
                        ],
                      ),
                    ),
                    ComponentsDropdown(
                      marginTitle: EdgeInsets.fromLTRB(70, 0, 0, 0),
                      width: MediaQuery.of(context).size.width / 1.6,
                      title: '직책',
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      name: 'position',
                      initialValue: 'STAFF',
                      hintText: '직책을 선택해주세요.',
                      items: const [
                        // List<String> positionOptions = ['MANAGER', 'TEAM_LEADER', 'SENIOR_STAFF', 'STAFF', 'TEAM_TOP', 'YOU_FIRED'];
                        DropdownMenuItem(
                          child: Text('매니저'),
                          value: 'MANAGER',
                        ),
                        DropdownMenuItem(
                          child: Text('팀리더'),
                          value: 'TEAM_LEADER',
                        ),
                        DropdownMenuItem(
                          child: Text('월급제 알바생'),
                          value: 'SENIOR_STAFF',
                        ),
                        DropdownMenuItem(
                          child: Text('시급제 알바생'),
                          value: 'STAFF',
                        ),
                        DropdownMenuItem(
                          child: Text('공동 대표'),
                          value: 'TEAM_TOP',
                        ),
                      ],
                      suffix: null,
                    ),
                    ComponentsTextFormTitle(
                      title: '특이사항',
                      name: 'etc',
                      hintText: '기타 사항을 작성해주세요.',
                      maxLength: 20,
                      margin: EdgeInsets.fromLTRB(40, 20, 0, 0),
                      height: 70,
                    ),
                  ],
                )),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        '등록하시겠습니까?',
                        style:
                            TextStyle(color: Color.fromRGBO(64, 64, 64, 1.0)),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'Cancel'),
                          child: const Text(
                            '취소',
                            style: TextStyle(
                                color: Color.fromRGBO(63, 114, 175, 1.0)),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            BusinessParttimerRegistRequest request =
                                BusinessParttimerRegistRequest(
                              _formKey.currentState!.fields['phoneNumber']!.value,
                              _formKey.currentState!.fields['position']!.value,
                              _formKey.currentState?.fields['etc']!.value,
                            );
                            sendManual(request);
                          },
                          child: const Text('확인',
                              style: TextStyle(
                                  color: Color.fromRGBO(17, 45, 78, 1.0))),
                        ),
                      ],
                      backgroundColor: Colors.white,
                    ),
                  );
                },
                style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                    onPrimary: Color.fromRGBO(17, 45, 78, 1.0),
                    side: BorderSide(
                        color: Color.fromRGBO(17, 45, 78, 1.0), width: 2.0)),
                child: const Text('등록'),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
