import 'package:flutter/material.dart';
import 'package:my_worker_app/pages/business_management/PT_contract_manage/page_parttimer_contract_list.dart';
import 'package:my_worker_app/pages/business_management/PT_info_manage/page_parttimer_info_list.dart';
import 'package:my_worker_app/pages/business_management/PT_join_manage/page_parttimer_join.dart';
import 'package:my_worker_app/pages/business_management/PT_pay_manage/page_parttimer_pay_list.dart';
import 'package:my_worker_app/pages/business_management/PT_schedule_manage/page_parttimer_schedule_list.dart';
import 'package:my_worker_app/pages/business_management/business_manage/page_business_detail.dart';
import 'package:my_worker_app/pages/business_management/business_manual_manage/page_business_manual_list.dart';
import 'package:my_worker_app/pages/business_management/business_product_manage/page_business_product_list.dart';
import 'package:my_worker_app/pages/business_management/business_transition_manage/page_business_transition_list.dart';
import 'package:my_worker_app/config/color.dart';

class PageBusinessMenuList extends StatefulWidget {
  const PageBusinessMenuList({super.key});

  @override
  State<PageBusinessMenuList> createState() => _PageBusinessMenuListState();
}

class _PageBusinessMenuListState extends State<PageBusinessMenuList> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Card(
            child: ListTile(
              tileColor: Colors.white,
              leading: Icon(Icons.business, color: colorDark,),
              title: Text('사업장 정보'),
              trailing: Icon(Icons.keyboard_arrow_right, color: colorDark,),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => (PageBusinessDetail(id: 1,))));
              },
            ),
          ),
          Card(
            child: ListTile(
              tileColor: Colors.white,
              leading: Icon(Icons.people, color: colorDark,),
              title: Text('직원 관리'),
              trailing: Icon(Icons.keyboard_arrow_right, color: colorDark,),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => (PageParttimerInfoList())));
              },
            ),
          ),
          Card(
            child: ListTile(
              tileColor: Colors.white,
              leading: Icon(Icons.add_alert, color: colorDark,),
              title: Text('직원 등록'),
              trailing: Icon(Icons.keyboard_arrow_right, color: colorDark,),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => (PageParttimerJoin())));
              },
            ),
          ),
          Card(
            child: ListTile(
              tileColor: Colors.white,
              leading: Icon(Icons.calendar_month, color: colorDark,),
              title: Text('출퇴근 캘린더'),
              trailing: Icon(Icons.keyboard_arrow_right, color: colorDark,),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                        (PageParttimerScheduleList())));
              },
            ),
          ),
          Card(
            child: ListTile(
              tileColor: Colors.white,
              leading: Icon(Icons.monetization_on_outlined, color: colorDark,),
              title: Text('급여 관리'),
              trailing: Icon(Icons.keyboard_arrow_right, color: colorDark,),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => (PageParttimerPayList())));
              },
            ),
          ),
          Card(
            child: ListTile(
              tileColor: Colors.white,
              leading: Icon(Icons.assignment_outlined, color: colorDark,),
              title: Text('근로 계약 관리'),
              trailing: Icon(Icons.keyboard_arrow_right, color: colorDark,),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => (PageParttimerContractList())));
              },
            ),
          ),
          Card(
            child: ListTile(
              tileColor: Colors.white,
              leading: Icon(Icons.book_outlined, color: colorDark,),
              title: Text('매뉴얼 관리'),
              trailing: Icon(Icons.keyboard_arrow_right, color: colorDark,),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => (PageBusinessManualList())));
              },
            ),
          ),
          Card(
            child: ListTile(
              tileColor: Colors.white,
              leading: Icon(Icons.border_color, color: colorDark,),
              title: Text('인수인계 캘린더'),
              trailing: Icon(Icons.keyboard_arrow_right, color: colorDark,),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => (PageBusinessTransitionList())));
              },
            ),
          ),
          Card(
            child: ListTile(
              tileColor: Colors.white,
              leading: Icon(Icons.local_grocery_store_outlined, color: colorDark,),
              title: Text('상품 재고 관리'),
              trailing: Icon(Icons.keyboard_arrow_right, color: colorDark,),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => (PageBusinessProductList())));
              },
            ),
          ),
        ],
      ),
    );
  }

}
