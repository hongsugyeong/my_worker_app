import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:intl/intl.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/model/business/parttimer/schedule/parttimer_schedule_regist_request.dart';
import 'package:my_worker_app/pages/business_management/PT_info_manage/page_parttimer_info_list.dart';
import 'package:my_worker_app/pages/business_management/PT_schedule_manage/page_parttimer_schedule_list.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_parttimer_schedule.dart';

class PageParttimerInfoScheduleRegist extends StatefulWidget {
  const PageParttimerInfoScheduleRegist({super.key, required this.id});

  final num id;

  @override
  State<PageParttimerInfoScheduleRegist> createState() =>
      _PageParttimerInfoScheduleRegistState();
}

class _PageParttimerInfoScheduleRegistState
    extends State<PageParttimerInfoScheduleRegist> {
  final _formKey = GlobalKey<FormBuilderState>();

  DateTime timeScheduleStart = DateTime(00, 00);
  DateTime timeScheduleEnd = DateTime(00, 00);
  List<String> timeScheduleTotal = ['1시간', '2시간', '3시간', '4시간', '5시간', '6시간', '7시간', '8시간', '9시간', '10시간', '11시간', '12시간'];

  Future<void> sendSchedule(ParttimerScheduleRegistRequest request) async {
    await RepoParttimerSchedule().putSchedule(request, widget.id as int)
    // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      request.weekSchedule = '';
      request.timeScheduleStart = '';
      request.timeScheduleEnd = '';
      request.timeScheduleTotal = '';
      print("성공");
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
              const PageParttimerInfoList()));
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(
        menu: '근무 등록',
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
          child: Column(
            children: [
              // 값 넣는 부분임
              FormBuilder(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '근무 요일',
                            style: TextStyle(
                                color: Color.fromRGBO(64, 64, 64, 1.0),
                                fontSize: fontSizeMedium),
                          ),
                          Container(
                            child: DropdownButtonHideUnderline(
                              child: FormBuilderDropdown<String>(
                                name: 'weekSchedule',
                                initialValue: '월',
                                decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20),
                                    borderSide:
                                    BorderSide(color: colorDark, width: 2),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20),
                                    borderSide:
                                    BorderSide(color: colorNormal, width: 2),
                                  ),
                                  suffix: null,
                                  hintText: '근무 요일을 선택해주세요.',
                                ),
                                items: const [
                                  // List<String> weekSchedule = ['월', '화', '수', '목', '금', '토', '일'];
                                  DropdownMenuItem(
                                    child: Text('월요일'),
                                    value: '월',
                                  ),
                                  DropdownMenuItem(
                                    child: Text('화요일'),
                                    value: '화',
                                  ),
                                  DropdownMenuItem(
                                    child: Text('수요일'),
                                    value: '수',
                                  ),
                                  DropdownMenuItem(
                                    child: Text('목요일'),
                                    value: '목',
                                  ),
                                  DropdownMenuItem(
                                    child: Text('금요일'),
                                    value: '금',
                                  ),
                                  DropdownMenuItem(
                                    child: Text('토요일'),
                                    value: '토',
                                  ),
                                  DropdownMenuItem(
                                    child: Text('일요일'),
                                    value: '일',
                                  ),
                                ]
                              ),
                            ),
                            width: MediaQuery.of(context).size.width / 1.6,
                            height: 60,
                            margin: EdgeInsets.fromLTRB(45, 10, 0, 10),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '출근 시간',
                            style: TextStyle(
                                color: Color.fromRGBO(64, 64, 64, 1.0),
                                fontSize: fontSizeMedium),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(45, 0, 0, 0),
                            child: _DatePickerItem(
                              children: <Widget>[
                                CupertinoButton(
                                  onPressed: () {
                                    _showDialog(
                                      CupertinoDatePicker(
                                        initialDateTime: timeScheduleStart,
                                        mode: CupertinoDatePickerMode.time,
                                        use24hFormat: true,
                                        onDateTimeChanged: (DateTime newTime) {
                                          setState(() {
                                            timeScheduleStart = newTime;
                                            DateFormat('hh:mm').format(timeScheduleStart).toString();
                                          });
                                        },
                                      ),);
                                    },
                                  child: Text(
                                    '${timeScheduleStart.hour}:${timeScheduleStart.minute}',
                                    style: const TextStyle(
                                      fontSize: 22.0,
                                      color: Colors.black
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 20),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '퇴근 시간',
                            style: TextStyle(
                                color: Color.fromRGBO(64, 64, 64, 1.0),
                                fontSize: fontSizeMedium),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(45, 0, 0, 0),
                            child: _DatePickerItem(
                              children: <Widget>[
                                CupertinoButton(
                                  onPressed: () => _showDialog(
                                    CupertinoDatePicker(
                                      initialDateTime: timeScheduleEnd,
                                      mode: CupertinoDatePickerMode.time,
                                      use24hFormat: true,
                                      onDateTimeChanged: (DateTime newTime) {
                                        setState(() {
                                          timeScheduleEnd = newTime;
                                        });
                                      },
                                    ),
                                  ),
                                  child: Text(
                                    '${timeScheduleEnd.hour}:${timeScheduleEnd.minute}',
                                    style: const TextStyle(
                                        fontSize: 22.0,
                                        color: Colors.black
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            '총 근무 시간',
                            style: TextStyle(
                                color: Color.fromRGBO(64, 64, 64, 1.0),
                                fontSize: fontSizeMedium),
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(25, 0, 0, 0),
                            width: MediaQuery.of(context).size.width / 1.6,
                            height: 60,
                            child: DropdownButtonHideUnderline(
                              child: FormBuilderDropdown<String>(
                                name: 'timeScheduleTotal',
                                initialValue: '1시간',
                                decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20),
                                    borderSide:
                                    BorderSide(color: colorDark, width: 2),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(20),
                                    borderSide:
                                    BorderSide(color: colorNormal, width: 2),
                                  ),
                                  suffix: null,
                                  hintText: '총 근무시간을 선택해주세요.',
                                ),
                                items: timeScheduleTotal
                                    .map((timeScheduleTotal) => DropdownMenuItem(
                                  alignment: AlignmentDirectional.topStart,
                                  value: timeScheduleTotal,
                                  child: Text(timeScheduleTotal),
                                ))
                                    .toList(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      showDialog<String>(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                          title: const Text(
                            '등록하시겠습니까?',
                            style: TextStyle(
                                color: Color.fromRGBO(64, 64, 64, 1.0)),
                          ),
                          // content: const Text('AlertDialog description'),
                          actions: <Widget>[
                            TextButton(
                              onPressed: () => Navigator.pop(context, 'Cancel'),
                              child: const Text(
                                '취소',
                                style: TextStyle(color: colorNormal),
                              ),
                            ),
                            TextButton(
                              onPressed: () {
                                ParttimerScheduleRegistRequest request =
                                ParttimerScheduleRegistRequest(
                                  _formKey.currentState!.fields['weekSchedule']!.value + '@',
                                  DateFormat('hh:mm').format(timeScheduleStart).toString(),
                                  DateFormat('hh:mm').format(timeScheduleEnd).toString(),
                                  _formKey.currentState!.fields['timeScheduleTotal']!.value + '@',
                                );
                                sendSchedule(request);
                              },
                              child: const Text('확인',
                                  style: TextStyle(color: colorDark)),
                            ),
                          ],
                          backgroundColor: Colors.white,
                        ),
                      );
                    }
                  },
                  style: ElevatedButton.styleFrom(
                      primary: Colors.white,
                      onPrimary: colorDark,
                      side: BorderSide(color: colorDark, width: 2.0)),
                  child: const Text('등록'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _showDialog(Widget child) {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) => Container(
        height: 216,
        padding: const EdgeInsets.only(top: 6.0),
        margin: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        color: CupertinoColors.systemBackground.resolveFrom(context),
        child: SafeArea(
          top: false,
          child: child,
        ),
      ),
    );
  }
}

class _DatePickerItem extends StatelessWidget {
  const _DatePickerItem({required this.children});

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width / 1.6,
      height: 55,
      child: DecoratedBox(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
            border: Border.all(
              width: 2,
              color: colorDark,
            ),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: children,
          ),
        ),
      ),
    );
  }
}

