import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/business/parttimer/info/components_parttimer_info_list.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/model/business/parttimer/info/parttimer_info_list.dart';
import 'package:my_worker_app/pages/business_management/PT_info_manage/page_parttimer_info_detail.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_parttimer_info.dart';

class PageParttimerInfoList extends StatefulWidget {
  const PageParttimerInfoList({super.key});

  @override
  State<PageParttimerInfoList> createState() => _PageParttimerInfoListState();
}

// 찐 부모: State
class _PageParttimerInfoListState extends State<PageParttimerInfoList> {

  // 스크롤 조작 컨트롤러
  final _scrollController = ScrollController();

  List<ParttimerInfoList> _list = [];
  int _currentPage = 1;
  int _totalPage = 1;
  int _totalItemCount = 0;

  // 라이프사이클 존재 => 페이지 변화 감시
  // initState: 탄생 부분
  @override
  void initState() {
    // super: 부모
    // 부모 먼저 준비
    super.initState();

    // 해당 페이지 죽기 전까지 계속 감시하기
    _scrollController.addListener(() {
      // 스크롤이 바닥 쳤을 때 _loadItems 실행
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });

    _loadItems(reFresh: true);
  }

  // 새로고침 부분
  Future<void> _loadItems({bool reFresh = false}) async {
    // 새로고침 할 경우
    if (reFresh) {
      // 바뀐 상황 알려주기
      setState(() {
        _list = [];
        _currentPage = 1;
        _totalPage = 1;
        _totalItemCount = 0;
      });
    }

    if (_currentPage <= _totalPage) {
      // 레포지토리에게 전화 걸기
      // 레포에서 만들었던 getList 불러오기
      // 페이지는 첫페지이
      // 페이지값 억지로 넣어줌 나중에 수정하기
      await RepoParttimerInfo().getList(page: _currentPage)
          .then((res) {
        setState(() {
          _totalPage = res.totalPage.toInt();
          _totalItemCount = res.totalCount.toInt();
          // ...: 이어가기
          // ,를 기준으로 _list와 list를 합치겠다
          // 나한테 데이터 이어 붙이기
          // 여기가 데이터 이어지는 부분인듯
          _list = [..._list, ...res.list!];

            _currentPage++;

            print('성공');
        });
      }).catchError((err) {
        debugPrint(err);
        print('실패');
      });
    }

    if (reFresh) {
      // 스크롤에 애니메이션 추가
      _scrollController.animateTo(
          0, // 스크롤 위치 0으로 이동
          duration: const Duration(milliseconds: 300), // 간격
          curve: Curves.easeOut // 애니메이션 모션의 종류
      );
    }
  }

  // build는 라이프사이클에서 부착단계
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '알바생 정보 관리',),
      body: RefreshIndicator(
        onRefresh: () async {
          setState(() {
            _loadItems(reFresh: true);
          });
        },
        child: ListView(
            controller: _scrollController,
            children: [_parttimerInfoList(),]
        ),
      ),
    );
  }


  Widget _parttimerInfoList() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _list.length,
            itemBuilder: (_, int idx) => ComponentsParttimerInfoList(parttimerInfoList: _list[idx], callback: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageParttimerInfoDetail(id: _list[idx].id)));
            }),
          )
        ],
      );
    } else {
      return SizedBox(
          child: ComponentsLoading()
      );
    }
  }
}
