import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:my_worker_app/components/appbar/business/PT_info_manage/components_appbar_info_manage_detail.dart';
import 'package:my_worker_app/components/button/components_text_detail.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/model/business/parttimer/info/parttimer_info_detail.dart';
import 'package:my_worker_app/pages/business_management/PT_info_manage/page_parttimer_info_list.dart';
import 'package:my_worker_app/repository/business/parttimer/repo_parttimer_info.dart';

class PageParttimerInfoDetail extends StatefulWidget {
  const PageParttimerInfoDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageParttimerInfoDetail> createState() => _PageParttimerInfoDetailState();
}

class _PageParttimerInfoDetailState extends State<PageParttimerInfoDetail> {

  ParttimerInfoDetail? _detail;

  final _formKey = GlobalKey<FormBuilderState>();

  String _buttonState = '재직중';
  var _color = colorDark;
  var _textColor = Colors.black26;

  void changeText() {
    setState(() {
      if (_buttonState == '퇴사') {
        _buttonState = '재직중';
        _color = colorNormal;
        _textColor = colorDark;
      }

      else {
        _buttonState = '퇴사';
        _color = colorDark;
        _textColor = Colors.black26;
      }
    });
  }

  Future<void> _loadDetail() async {
    await RepoParttimerInfo().getProduct(widget.id)
        .then((res) => {
          setState(() {
            _detail = res.data;
            print('성공함');
          })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  Future<void> _putIsWork() async {
    await RepoParttimerInfo().putIsWork(widget.id)
        .then((res) => {
          changeText(),
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => PageParttimerInfoList())),
          print('성공함')
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_detail == null) {
      return ComponentsLoading();
    } else {
      return Scaffold(
        appBar: ComponentsAppbarInfoManageDetail(id: widget.id,),
        body: SingleChildScrollView(
          child: Column(
            children: [
              _parttimerInfoDetail(context),
            ],
          ),
        ),
      );
    }

  }

  Widget _parttimerInfoDetail(BuildContext context) {
    if (_detail?.weekScheduleList?.workingHour == null) {
      return Scaffold(
        body: Center(
          child: Text('근무 등록을 먼저 해주세요.'),
        ),
      );
    } else {
      return SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        '${_detail!.memberName}',
                        style: TextStyle(
                            fontSize: fontSizeLarge,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                    ),
                    Text(
                      '${_detail!.position}',
                      style: TextStyle(
                          color: Color.fromRGBO(64, 64, 64, 1.0),
                          fontSize: fontSizeSmall
                      ),
                    ),
                  ],
                ),
                height: 30,
                margin: EdgeInsets.fromLTRB(10, 10, 0, 0),
              ),
              Divider(
                color: colorLight,
              ),
              Container(
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Text('재직 여부'),
                          Container(
                            child: Text(
                              '${_detail!.isWork}',
                              style: TextStyle(
                                  color: colorNormal
                              ),
                            ),
                            margin: EdgeInsets.fromLTRB(110, 0, 40, 0),
                          ),
                          _isWorkButton(),
                        ],
                      ),
                      height: 60,
                    ),
                    ComponentsTextDetail(title: '생년월일', data: '${_detail!.memberDateBirth}', margin: EdgeInsets.fromLTRB(115, 0, 0, 0),),
                    ComponentsTextDetail(title: '휴대전화 번호', data: '${_detail!.memberPhoneNumber}', margin: EdgeInsets.fromLTRB(85, 0, 0, 0),),
                    _weekScheduleNull(),
                    ComponentsTextDetail(title: '입사일', data: '${_detail!.dateIn}', margin: EdgeInsets.fromLTRB(130, 0, 0, 0),),
                    _dateOutNull(),
                    _etcNull(),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(15, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 1.0,
              ),
            ],
          ),
        ),
      );
    }
  }

  Widget _weekScheduleNull () {
    if (_detail?.weekScheduleList?.workingHour != null ) {
      return Container(
        child: Row(
          children: [
            Text('근무'),
            Container(
              child: Text('${_detail?.weekScheduleList?.workingHour}'.split(',').toString()),
              width: 150,
              margin: EdgeInsets.fromLTRB(140, 5, 0, 5),
            )
          ],
        ),
      );
    } else {
      return Container(
        child: Row(
          children: [
            Text('근무'),
            Container(
              child: Text('-'),
              width: 150,
              margin: EdgeInsets.fromLTRB(140, 5, 0, 5),
            )
          ],
        ),
      );
    }
  }

  Widget _dateOutNull () {
    if (_detail?.dateOut != null ) {
      return ComponentsTextDetail(title: '퇴사일', data: '${_detail?.dateOut}', margin: EdgeInsets.fromLTRB(130, 0, 0, 0),);
    } else {
      return ComponentsTextDetail(title: '퇴사일', data: '-', margin: EdgeInsets.fromLTRB(130, 0, 0, 0),);
    }
  }

  Widget _etcNull () {
    if (_detail?.etc != null ) {
      return Container(
        child: Row(
          children: [
            Text('비고'),
            Container(
              child: Text('${_detail?.etc}'),
              margin: EdgeInsets.fromLTRB(145, 0, 0, 0),
              width: 180,
            )
          ],
        ),
        height: 40,
      );
    } else {
      return Container(
        child: Row(
          children: [
            Text('비고'),
            Container(
              child: Text('-'),
              margin: EdgeInsets.fromLTRB(145, 0, 0, 0),
              width: 180,
            )
          ],
        ),
        height: 40,
      );
    }
  }

  Widget _isWorkButton () {
    if (_detail?.isWork == '재직중') {
      return Container(
        width: 85,
        child: Container(
          child: ElevatedButton(
            child: Text('${_detail?.isWork}',
              style: TextStyle(
                  fontSize: 12, color: colorDark, height: 1.5),
            ),
            onPressed: () {
              _putIsWork();
              showToast();
            },
            style: ElevatedButton.styleFrom(
                side: const BorderSide(color: colorNormal, width: 2,
                ),
                padding: EdgeInsets.zero
            ),
          ),
        ),
      );
    } else {
      return Container(
        width: 80,
        child: ElevatedButton(
          child: Text('${_detail?.isWork}',
            style: TextStyle(
                fontSize: 12, color: _textColor, height: 1.5),
          ),
          onPressed: () {
            _putIsWork();
          },
          style: ElevatedButton.styleFrom(
              side: BorderSide(color: _color, width: 2,
              ),
              padding: EdgeInsets.zero
          ),
        ),
      );
    }
  }

}

void showToast() {
  Fluttertoast.showToast(
      msg: '성공했습니다.',
      gravity: ToastGravity.BOTTOM,
      backgroundColor: Colors.black38,
      fontSize: fontSizeMedium,
      textColor: Colors.black,
      toastLength: Toast.LENGTH_SHORT,
      webBgColor: "linear-gradient(to right, #dbe2ef, #dbe2ef)"
  );
}
