import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/button/components_camera.dart';
import 'package:my_worker_app/components/button/components_text_form_title.dart';
import 'package:my_worker_app/model/business/business_regist_request.dart';
import 'package:my_worker_app/config/size.dart';
import 'package:my_worker_app/pages/page_index.dart';
import 'package:my_worker_app/repository/business/repo_business.dart';

enum SampleItem { itemOne, itemTwo }

class PageBusinessAdd extends StatefulWidget {
  const PageBusinessAdd({super.key});

  @override
  State<PageBusinessAdd> createState() => _PageBusinessAddState();
}

class _PageBusinessAddState extends State<PageBusinessAdd> {
  SampleItem? selectedItem;
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> sendManual(BusinessRegistRequest request) async {
    // id 값 억지로 넣어준 거임 바꿔주기
    await RepoBusiness()
        .setBusiness(request)
        // 성공했을시에만 페이지 이동되도록 하기
        .then((value) {
      request.businessName = '';
      request.businessNumber = '';
      request.ownerName = '';
      request.businessImgUrl = '';
      request.businessType = '';
      request.businessLocation = '';
      request.businessEmail = '';
      request.businessPhoneNumber = '';
      request.reallyLocation = '';
      print("성공");
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => const PageIndex()));
    }).catchError((onError) {
      print(onError);
      print("실패");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '사업장 등록',),
      body: _businessForm(),
    );
  }

  Widget _businessForm() {
    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [
            FormBuilder(
              key: _formKey,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ComponentsTextFormTitle(
                      title: '상호명',
                      name: 'businessName',
                      hintText: '상호명을 입력해주세요.',
                      maxLength: 20,
                      margin: EdgeInsets.fromLTRB(60, 10, 0, 0),
                      height: 60,
                    ),
                    ComponentsTextFormTitle(
                      title: '사업자 번호',
                      name: 'businessNumber',
                      hintText: '000-00-00000',
                      maxLength: 20,
                      margin: EdgeInsets.fromLTRB(30, 10, 0, 0),
                      height: 60,
                    ),
                    ComponentsTextFormTitle(
                      title: '업종',
                      name: 'businessType',
                      hintText: '업종을 입력해주세요.',
                      maxLength: 20,
                      margin: EdgeInsets.fromLTRB(80, 10, 0, 0),
                      height: 60,
                    ),
                    ComponentsTextFormTitle(
                      title: '대표자명',
                      name: 'ownerName',
                      hintText: '대표자명을 입력해주세요.',
                      maxLength: 20,
                      margin: EdgeInsets.fromLTRB(50, 10, 0, 0),
                      height: 60,
                    ),
                    ComponentsTextFormTitle(
                      title: '소재지',
                      name: 'businessLocation',
                      hintText: '소재지를 입력해주세요.',
                      maxLength: 20,
                      margin: EdgeInsets.fromLTRB(60, 10, 0, 0),
                      height: 60,
                    ),
                    ComponentsTextFormTitle(
                      title: '전화번호',
                      name: 'businessPhoneNumber',
                      hintText: '010-0000-0000',
                      maxLength: 20,
                      margin: EdgeInsets.fromLTRB(50, 10, 0, 0),
                      height: 60,
                    ),
                    ComponentsTextFormTitle(
                      title: '이메일',
                      name: 'businessEmail',
                      hintText: '이메일을 입력해 주세요.',
                      maxLength: 20,
                      margin: EdgeInsets.fromLTRB(60, 10, 0, 0),
                      height: 60,
                    ),
                    ComponentsTextFormTitle(
                      title: '실근무지주소',
                      name: 'reallyLocation',
                      hintText: '실근무지주소를 입력해 주세요.',
                      maxLength: 20,
                      margin: EdgeInsets.fromLTRB(20, 10, 0, 0),
                      height: 60,
                    ),
                    Row(
                      children: [
                        Text(
                          '사업장 등록',
                          style: TextStyle(
                              color: Color.fromRGBO(64, 64, 64, 1.0),
                              fontSize: fontSizeMedium),
                        ),
                        ComponentsCamera(name: 'businessImgUrl',),
                      ],
                    )
                  ],
                ),
                margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: ElevatedButton(
                onPressed: () {
                  showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text(
                        '등록하시겠습니까?',
                        style:
                            TextStyle(color: Color.fromRGBO(64, 64, 64, 1.0)),
                      ),
                      actions: <Widget>[
                        TextButton(
                          onPressed: () => Navigator.pop(context, 'Cancel'),
                          child: const Text(
                            '취소',
                            style: TextStyle(
                                color: Color.fromRGBO(63, 114, 175, 1.0)),
                          ),
                        ),
                        TextButton(
                          onPressed: () {
                            BusinessRegistRequest request =
                                BusinessRegistRequest(
                              _formKey
                                  .currentState!.fields['businessName']!.value,
                              _formKey.currentState?.fields['businessNumber']!
                                  .value,
                              _formKey.currentState!.fields['ownerName']!.value,
                              _formKey.currentState!.fields['businessImgUrl']!
                                  .value,
                              _formKey
                                  .currentState!.fields['businessType']!.value,
                              _formKey.currentState!.fields['businessLocation']!
                                  .value,
                              _formKey
                                  .currentState!.fields['businessEmail']!.value,
                              _formKey.currentState!
                                  .fields['businessPhoneNumber']!.value,
                              _formKey.currentState!.fields['reallyLocation']!
                                  .value,
                            );
                            sendManual(request);
                          },
                          child: const Text('확인',
                              style: TextStyle(
                                  color: Color.fromRGBO(17, 45, 78, 1.0))),
                        ),
                      ],
                      backgroundColor: Colors.white,
                    ),
                  );
                },
                style: ElevatedButton.styleFrom(
                    primary: Colors.white,
                    onPrimary: Color.fromRGBO(17, 45, 78, 1.0),
                    side: BorderSide(
                        color: Color.fromRGBO(17, 45, 78, 1.0), width: 2.0)),
                child: const Text('등록'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
