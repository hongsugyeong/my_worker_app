import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/components_business_list.dart';
import 'package:my_worker_app/components/components_loading.dart';
import 'package:my_worker_app/model/business/business_detail.dart';
import 'package:my_worker_app/repository/business/repo_business.dart';
import 'package:my_worker_app/config/color.dart';

class PageBusinessDetail extends StatefulWidget {
  const PageBusinessDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageBusinessDetail> createState() => _PageBusinessDetailState();
}

class _PageBusinessDetailState extends State<PageBusinessDetail> {

  BusinessDetail? _detail;

  Future<void> _loadDetail() async {
    await RepoBusiness().getProduct(widget.id)
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsBusinessList(menu: '사업장 상세보기',),
      body: SingleChildScrollView(
          child: _businessDetail(context)
      )
    );
  }

  Widget _businessDetail(BuildContext context) {
    if (_detail == null) {
      return SizedBox(
        child: ComponentsLoading()
      );
    } else {
      return SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                child: Column(
                  children: [
                    Container(
                      child: Row(
                        children: [
                          Text('상호명'),
                          Container(
                            child: Text('${_detail!.businessName}'),
                            margin: EdgeInsets.fromLTRB(75, 0, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('사업자 번호'),
                          Container(
                            child: Text('${_detail!.businessNumber}'),
                            margin: EdgeInsets.fromLTRB(45, 0, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('대표자명'),
                          Container(
                            child: Text('${_detail!.ownerName}'),
                            margin: EdgeInsets.fromLTRB(63, 0, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('업종'),
                          Container(
                            child: Text('${_detail!.businessType}'),
                            margin: EdgeInsets.fromLTRB(90, 0, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('소재지'),
                          Container(
                            child: Text('${_detail!.businessLocation}'),
                            margin: EdgeInsets.fromLTRB(78, 0, 0, 0),
                            width: 220,
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('실근무지 주소'),
                          Container(
                            child: Text('${_detail!.reallyLocation}'),
                            margin: EdgeInsets.fromLTRB(38, 0, 0, 0),
                            width: 200,
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('이메일'),
                          Container(
                            child: Text('${_detail!.businessEmail}'),
                            margin: EdgeInsets.fromLTRB(79, 0, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('전화번호'),
                          Container(
                            child: Text('${_detail!.businessPhoneNumber}'),
                            margin: EdgeInsets.fromLTRB(66, 0, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Text('사업장 승인 여부'),
                              _helpPopupApproval(context),
                            ],
                          ),
                          Container(
                            child: Text(
                              '${_detail!.isApprovalBusiness}',
                              style: TextStyle(
                                  color: colorNormal),
                            ),
                            margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Text('사업장 영업 여부'),
                              _helpPopupOpen(context),
                            ],
                          ),
                          Container(
                            child: Text(
                              '${_detail!.isActivity}',
                              style: TextStyle(
                                  color: colorNormal),
                            ),
                            margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Row(
                            children: [
                              Text('가입 승인일'),
                              _helpPopupJoin(context),
                            ],
                          ),
                          Container(
                            child: Text('${_detail!.dateApprovalBusiness}'),
                            margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                    Container(
                      child: Row(
                        children: [
                          Text('사업장 등록증'),
                          Container(
                            child: _imgPopup(context),
                            margin: EdgeInsets.fromLTRB(25, 0, 0, 0),
                            width: 130,
                          ),
                        ],
                      ),
                      margin: EdgeInsets.fromLTRB(0, 0, 0, 20),
                    ),
                  ],
                ),
                margin: EdgeInsets.fromLTRB(20, 10, 0, 0),
                width: MediaQuery.of(context).size.width / 1.0,
              ),
            ],
          ),
        ),
      );
    }
  }

  Widget _helpPopupApproval(BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text('미승인시 관리자 반려 사유가 나옵니다.'),
                        const SizedBox(height: 15),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('닫기', style: TextStyle(color: colorDark),),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              child: const Icon(Icons.help, color: colorDark,),
            ),
          ],
        )
    );
  }

  Widget _helpPopupOpen(BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text('영업 상태 변경 요청시 원하시는 상태로 변경됩니다.'),
                        const SizedBox(height: 15),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('닫기', style: TextStyle(color: colorDark),),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              child: const Icon(Icons.help, color: colorDark,),
            ),
          ],
        )
    );
  }

  Widget _helpPopupJoin(BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        const Text('승인 신청 날짜가 아닌 관리자가 승인한 날짜로 나타납니다.'),
                        const SizedBox(height: 15),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: const Text('닫기', style: TextStyle(color: colorDark),),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              child: const Icon(Icons.help, color: colorDark,),
            ),
          ],
        )
    );
  }

  Widget _imgPopup (BuildContext context) {
    return GestureDetector(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const SizedBox(height: 10),
            ElevatedButton(
              onPressed: () => showDialog<String>(
                context: context,
                builder: (BuildContext context) => Dialog.fullscreen(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('${_detail!.businessImgUrl}'),
                      const SizedBox(height: 15,),
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('닫기', style: TextStyle(color: colorDark),),
                      )
                    ],
                  ),
                ),
              ),
              style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                  onPrimary: colorDark,
                  side: BorderSide(
                      color: colorDark,
                      width: 2.0)),
              child: const Text('이미지로 보기', style: TextStyle(fontSize: 13),),
            ),
          ],
        )
    );
  }

}