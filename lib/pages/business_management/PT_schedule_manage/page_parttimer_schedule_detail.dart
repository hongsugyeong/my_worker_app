import 'package:flutter/material.dart';
import 'package:my_worker_app/components/appbar/business/PT_schedule_manage/components_appbar_schedule_manage_detail.dart';
import 'package:my_worker_app/components/business/parttimer/schedule/components_parttimer_schedule_detail.dart';
import 'package:my_worker_app/model/business/parttimer/schedule/parttimer_schedule_detail.dart';

class PageParttimerScheduleDetail extends StatefulWidget {
  const PageParttimerScheduleDetail({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<PageParttimerScheduleDetail> createState() => _PageParttimerScheduleDetailState();
}

class _PageParttimerScheduleDetailState extends State<PageParttimerScheduleDetail> {

  List<ParttimerScheduleDetail> _list = [
    ParttimerScheduleDetail('홍수경', '09:00', '18:00', '1', '0', '0', '9', '아니오', '아니오', '-')
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsAppbarScheduleManageDetail(),
      body: Column(
        children: [
          _parttimerScheduleDetail(),
        ],
      ),
    );
  }

  Widget _parttimerScheduleDetail() {
    return ListView.builder(
      itemCount: _list.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (BuildContext cts, int idx) {
        return ComponentsParttimerScheduleDetail(
          callback: () {}, parttimerScheduleDetail: _list[idx],);
      },
    );
  }

}
