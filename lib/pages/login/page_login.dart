import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:my_worker_app/components/button/components_text_form.dart';
import 'package:my_worker_app/config/color.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/middleware/middleware_login_check.dart';
import 'package:my_worker_app/model/login/login_request.dart';
import 'package:my_worker_app/pages/join/page_join1.dart';
import 'package:my_worker_app/repository/login/repo_login.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    await RepoLogin().doLogin(loginRequest).then((res) {
      print('성공임');
      // api에서 받아온 결과값을 token에 넣는다.
      // name을 먼저 받아야 로그인 오류 발생하지 않고 한번에 로그인 됨
      TokenLib.setMemberName(res.data.name);
      TokenLib.setMemberToken(res.data.token);
      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);
    }).catchError((err) {
      print('실패임');
      debugPrint(err);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _login(context),
    );
  }

  Widget _login (BuildContext context) {
    return Scaffold(
      backgroundColor: colorDark,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 4,
                    alignment: Alignment.center,
                    child: Image.asset(
                      'assets/img/logo_2.png',
                      width: 200,
                    ),
                  ),
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:
                      BorderRadius.only(topRight: Radius.circular(80)),
                ),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.all(50),
                      child: Text(
                        '로그인',
                        style: TextStyle(
                            fontSize: 30,
                            color: colorDark,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 1.5,
                          padding: EdgeInsets.only(bottom: 10),
                          child: FormBuilder(
                            key: _formKey,
                            autovalidateMode: AutovalidateMode.disabled,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                ComponentTextForm(
                                  title: '아이디',
                                  maxLength: 40,
                                  name: 'username',
                                  hintText: '아이디를 입력해주세요.',
                                  obscureText: false,
                                  lines: 20,
                                ),
                                ComponentTextForm(
                                  title: '비밀번호',
                                  maxLength: 20,
                                  name: 'password',
                                  hintText: '비밀번호를 입력해주세요.',
                                  obscureText: true,
                                  lines: 20,
                                ),
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.2,
                          height: MediaQuery.of(context).size.height / 9,
                          padding: EdgeInsets.all(20),
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.saveAndValidate()) {
                                LoginRequest loginRequest = LoginRequest(
                                  _formKey.currentState!.fields['username']!.value,
                                  _formKey.currentState!.fields['password']!.value,
                                );
                                _doLogin(loginRequest);
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                primary: colorDark,
                                onPrimary: colorDark,
                                side: BorderSide(color: colorDark, width: 2.0)),
                            child: Text(
                              '로그인',
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                        Container(
                          child: TextButton(
                            onPressed: () {},
                            child: Text('아이디 / 비밀번호 찾기', style: TextStyle(color: colorDark),),
                          ),
                        ),
                        Container(
                            child: TextButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const PageJoin1())
                                );
                              },
                              child: Text('회원가입', style: TextStyle(color: colorDark),),
                            ))

                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
