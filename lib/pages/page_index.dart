import 'package:flutter/material.dart';
import 'package:my_worker_app/pages/business_management/PT_schedule_manage/page_parttimer_schedule_list.dart';
import 'package:my_worker_app/pages/business_management/page_business_menu_list.dart';
import 'package:my_worker_app/pages/main_page/board/page_community.dart';
import 'package:my_worker_app/pages/main_page/page_home.dart';
import 'package:my_worker_app/pages/main_page/page_my_page.dart';
import 'package:my_worker_app/config/color.dart';

// 로그인 후 처음 보이는 화면 (= 홈화면)
class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => PageIndexState();
}

class PageIndexState extends State<PageIndex> {
  // 바텀 네비게이션 바 인덱스
  int _selectedIndex = 1;

  final List<Widget> _navIndex = [
    PageCommunity(),
    PageBusinessMenuList(),
    PageHome(),
    PageMyPage(),
  ];

  void _onNavTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '내알바야',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        actions: [
          Container(
            child: Image.asset(
              'assets/img/logo_2.png',
              width: 50,
              height: 50,
            ),
            margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
          ),
        ],
        backgroundColor: colorDark,
      ),
      body: _navIndex.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        fixedColor: colorDark,
        unselectedItemColor: colorNormal,
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.comment),
            label: '커뮤니티',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: '사업장 관리',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '홈',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: '마이 페이지',
          ),
        ],
        currentIndex: _selectedIndex,
        onTap: _onNavTapped,
      ),
      floatingActionButton: FloatingActionButton.small(
        // 사업장 추가 버튼 어떻게 할지 좀 더 고민해보기
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const PageParttimerScheduleList())
          );
        },
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: colorNormal,
        shape: (RoundedRectangleBorder(borderRadius: BorderRadius.circular(20))),
      ),
    );
  }
}

Widget _searchbar() {
  return SearchBar(
    trailing: [
      Icon(
        Icons.search,
        color: Colors.white,
      )
    ],
    elevation: MaterialStatePropertyAll(15),
    backgroundColor:
        MaterialStatePropertyAll(colorNormal),
    textStyle: MaterialStateProperty.all(TextStyle(color: Colors.white)),
    constraints: BoxConstraints(maxWidth: 320, minHeight: 48),
    hintText: "검색어를 입력해 주세요.",
  );
}
