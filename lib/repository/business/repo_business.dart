import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/business/business_detail_result.dart';
import 'package:my_worker_app/model/business/business_regist_request.dart';
import 'package:my_worker_app/model/common_result.dart';

class RepoBusiness {

  // 단수 R 구현
  Future<BusinessDetailResult> getProduct (num id) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUri/v1/business/detail/my/business';
    // /v1/business/detail/my/business

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return BusinessDetailResult.fromJson(response.data);
  }

  // C
  Future<CommonResult> setBusiness(BusinessRegistRequest request) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/business/join';
    // /v1/business/join

    final response = await dio.post(
        _baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

}