import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/business/parttimer/schedule/parttimer_schedule_list_result.dart';
import 'package:my_worker_app/model/business/parttimer/schedule/parttimer_schedule_regist_request.dart';
import 'package:my_worker_app/model/common_result.dart';

class RepoParttimerSchedule {

  // 날짜 선택하면 리스트 보이기
  Future<ParttimerScheduleListResult> getDetail(String today) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUri/v1/schedule/calendar/day?today={today}';
    // /v1/schedule/calendar/day
    // /v1/schedule/calendar/day?today=2024-04-29

    final response = await dio.get(
        _baseUrl.replaceAll('{today}', today),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    print(response.data);
    return ParttimerScheduleListResult.fromJson(response.data);

  }

  // C
  Future<CommonResult> putSchedule (ParttimerScheduleRegistRequest request, int memberId) async {
    Dio dio = Dio();

    // 주소 설정
    String _baseUrl = '$apiUri/v1/business-member/change/schedule/business-member-id/{businessMemberId}';
    // /v1/business-member/change/schedule/business-member-id/{businessMemberId}

    final response = await dio.put(
        _baseUrl.replaceAll('{businessMemberId}', memberId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

}