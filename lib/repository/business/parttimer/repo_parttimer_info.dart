import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/business/parttimer/info/parttimer_info_detail.dart';
import 'package:my_worker_app/model/business/parttimer/info/parttimer_info_detail_result.dart';
import 'package:my_worker_app/model/business/parttimer/info/parttimer_info_list_result.dart';

class RepoParttimerInfo {

  Future<ParttimerInfoListResult> getList({int page = 1}) async {
    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUri/v1/business-member/all/my-business/{pageNum}'.replaceAll('{pageNum}', page.toString());
    // /v1/business-member/all/my-business/{pageNum}

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    print(response.data);
    return ParttimerInfoListResult.fromJson(response.data);

  }

  // 단수 R 구현
  Future<ParttimerInfoDetailResult> getProduct (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/business-member/detail/business-member-id/{businessMemberId}';
    // /v1/business-member/detail/business-member-id/{businessMemberId}

    final response = await dio.get(
      _baseUrl.replaceAll('{businessMemberId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    print(response.data);
    return ParttimerInfoDetailResult.fromJson(response.data);
  }

  // 버튼 눌러서 재직상태 변경하기
  Future<ParttimerInfoDetail> putIsWork (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/business-member/change/work/business-member-id/{businessMemberId}';
    // /v1/business-member/change/work/business-member-id/{businessMemberId}

    final response = await dio.put(
        _baseUrl.replaceAll('{businessMemberId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return ParttimerInfoDetail.fromJson(response.data);
  }

}