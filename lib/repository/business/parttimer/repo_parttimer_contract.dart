import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/business/parttimer/contract/parttimer_contract_detail_result.dart';
import 'package:my_worker_app/model/business/parttimer/contract/parttimer_contract_list_result.dart';
import 'package:my_worker_app/model/business/parttimer/contract/parttimer_contract_regist_request.dart';
import 'package:my_worker_app/model/common_result.dart';

class RepoParttimerContract {

  // 복수 R 구현
  // 재직중인 사람들만 보여주고 이 사람들 타고 등록으로 이어지기
  Future<ParttimerContractListResult> getList({int page = 1}) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUri/v1/business-member/all/is-work/{pageNum}'.replaceAll('{pageNum}', page.toString());
    // /v1/business-member/all/is-work/{pageNum}

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    print(response.data);
    return ParttimerContractListResult.fromJson(response.data);

  }

  // 단수 R 구현
  Future<ParttimerContractDetailResult> getProduct (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/contract/detail/contract-id/{contractId}';
    // /v1/contract/detail/contract-id/{contractId}

    final response = await dio.get(
        _baseUrl.replaceAll('{contractId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return ParttimerContractDetailResult.fromJson(response.data);
  }

  // C
  Future<CommonResult> setContract(ParttimerContractRegistRequest request, int memberId) async {
    Dio dio = Dio();

    // 주소 설정
    String _baseUrl = '$apiUri/v1/contract/join/business-memberId/{businessMemberId}';
    // /v1/contract/join/business-memberId/{businessMemberId}

    final response = await dio.post(
        _baseUrl.replaceAll('{businessMemberId}', memberId.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

}