import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/business/parttimer/join/business_parttimer_regist_request.dart';
import 'package:my_worker_app/model/common_result.dart';

class RepoParttimerJoin {

  // C
  Future<CommonResult> setParttimer (BusinessParttimerRegistRequest request) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/business-member/join';
    // /v1/business-member/join

    final response = await dio.post(
        _baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

}