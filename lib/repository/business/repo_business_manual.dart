import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/business/manual/business_manual_detail_result.dart';
import 'package:my_worker_app/model/business/manual/business_manual_list_result.dart';
import 'package:my_worker_app/model/business/manual/business_manual_regist_request.dart';
import 'package:my_worker_app/model/common_result.dart';

class RepoBusinessManual {

  // 복수 R 구현
  Future<BusinessManualListResult> getList({int page = 1}) async {
    Dio dio = Dio();

    String? token = await TokenLib.getMemberToken();

    String _baseUrl = '$apiUri/v1/manual/all/boss/{pageNum}'.replaceAll('{pageNum}', page.toString());
    // /v1/manual/all/boss/{pageNum}

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    return BusinessManualListResult.fromJson(response.data);

  }

  // 단수 R 구현
  Future<BusinessManualDetailResult> getProduct (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/manual/detail/manual-id/{manualId}';
    // /v1/manual/detail/manual-id/{manualId}

    final response = await dio.get(
        _baseUrl.replaceAll('{manualId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    print(response.data);
    return BusinessManualDetailResult.fromJson(response.data);
  }

  // C
  Future<CommonResult> setManual(BusinessManualRegistRequest request) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.contentType = 'multipart/form-data';
    dio.options.maxRedirects.isFinite;
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/manual/new/';
    // /v1/manual/new/

    final response = await dio.post(
        _baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);

  }

}