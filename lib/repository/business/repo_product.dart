import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/business/product/business_product_detail_result.dart';
import 'package:my_worker_app/model/business/product/business_product_list_result.dart';
import 'package:my_worker_app/model/business/product/business_product_put_request.dart';
import 'package:my_worker_app/model/business/product/business_product_regist_request.dart';
import 'package:my_worker_app/model/common_result.dart';

class RepoProduct {

  // 복수 R 구현
  Future<BusinessProductListResult> getList({int page = 1}) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    String _baseUrl = '$apiUri/v1/product/all/boss/{pageNum}'.replaceAll('{pageNum}', page.toString());
    // /v1/product/all/boss/{pageNum}

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    print(response.data);
    return BusinessProductListResult.fromJson(response.data);

  }

  // D
  Future<BusinessProductListResult> deleteProduct (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/product/product-id/{productId}';
    // /v1/product/product-id/{productId}

    final response = await dio.delete(
        _baseUrl.replaceAll('{productId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return BusinessProductListResult.fromJson(response.data);
  }

  // C: 제품 등록하기
  Future<CommonResult> setProduct(BusinessProductRegistRequest request) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/product/new/boss';
    // /v1/product/new/boss

    final response = await dio.post(
        _baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  // U 최소수량 수정
  Future<CommonResult> putNowQuantity (BusinessProductPutRequest request, num id) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/product-record/boss/product-id/{productId}';
    // /v1/product-record/boss/product-id/{productId}

    final response = await dio.post(
        _baseUrl.replaceAll('{productId}', id.toString()),
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }

  // 단수 R 구현
  Future<BusinessProductDetailResult> getProduct (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/product/detail/product-id/{productId}';
    // /v1/product/detail/product-id/{productId}

    final response = await dio.get(
        _baseUrl.replaceAll('{productId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    print(response.data);
    return BusinessProductDetailResult.fromJson(response.data);
  }

}