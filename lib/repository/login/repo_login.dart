import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/model/common_result.dart';
import 'package:my_worker_app/model/login/join_request.dart';
import 'package:my_worker_app/model/login/login_request.dart';
import 'package:my_worker_app/model/login/login_result.dart';

class RepoLogin {

  // 로그인 하기
  Future<LoginResult> doLogin(LoginRequest request) async {

    String _baseUrl = '$apiUri/v1/login/app/boss';
        // /v1/login/app/boss

    Dio dio = Dio();

    final response = await dio.post(
      _baseUrl,
      data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return LoginResult.fromJson(response.data);
  }

  // 회원가입 하기
  Future<CommonResult> doJoin(JoinRequest request) async {

    String _baseUrl = '$apiUri/v1/member/join';
    // /v1/member/join

    Dio dio = Dio();

    final response = await dio.post(
        _baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResult.fromJson(response.data);
  }
}