import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/model/home/notice/notice_detail_result.dart';
import 'package:my_worker_app/model/home/notice/notice_list_result.dart';

class RepoNotice {

  // 복수 R 구현
  Future<NoticeListResult> getList({int page = 1}) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/notice/all/{pageNum}'.replaceAll('{pageNum}', page.toString());
    // /v1/notice/all/{pageNum}

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    return NoticeListResult.fromJson(response.data);

  }

  // 단수 R 구현
  Future<NoticeDetailResult> getProduct (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/notice/detail/notice-id/{noticeId}';
    // /v1/notice/detail/notice-id/{noticeId}

    final response = await dio.get(
        _baseUrl.replaceAll('{noticeId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return NoticeDetailResult.fromJson(response.data);
  }
}