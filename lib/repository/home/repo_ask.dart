import 'package:dio/dio.dart';
import 'package:my_worker_app/config/config_api.dart';
import 'package:my_worker_app/function/token_lib.dart';
import 'package:my_worker_app/model/common_result.dart';
import 'package:my_worker_app/model/home/ask/ask_answer_detail_result.dart';
import 'package:my_worker_app/model/home/ask/ask_create_request.dart';
import 'package:my_worker_app/model/home/ask/ask_detail_result.dart';
import 'package:my_worker_app/model/home/ask/ask_list_result.dart';

class RepoAsk {

  // C
  // 받을 거: 사용자 입력값, 사용자 id
  Future<CommonResult> setAsk(AskCreateRequest request) async {

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    // 주소 설정
    String _baseUrl = '$apiUri/v1/member-ask/new';
    // /v1/member-ask/new

    // await 꼭 붙여주기 제발
    final response = await dio.post(
      _baseUrl,
      data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    print('나와라 ');
    return CommonResult.fromJson(response.data);
  }

  // 복수 R
  Future<AskListResult> getList({int page = 1}) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/member-ask/all/{pageNum}'.replaceAll('{pageNum}', page.toString());
    // /v1/member-ask/all/{pageNum}

    final response = await dio.get(
        _baseUrl.replaceAll('{pageNum}', page.toString()),
        options: Options(
          // 데이터 없을 경우 다른 곳으로 안내 => false
            followRedirects: false,
            validateStatus: (status) {
              // 성공했을 경우 처리
              return status == 200;
            }
        )
    );

    return AskListResult.fromJson(response.data);

  }

  // 단수 R 구현
  Future<AskDetailResult> getProduct (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/member-ask/detail/member-ask-id/{memberAskId}';
    // /v1/member-ask/detail/member-ask-id/{memberAskId}

    final response = await dio.get(
        _baseUrl.replaceAll('{memberAskId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    print(response.data);
    return AskDetailResult.fromJson(response.data);
  }

  // D
  Future<AskAnswerDetailResult> deleteAsk (num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/v1/member-ask/del/member-ask-id/{memberAskId}';
    // /v1/member-ask/del/member-ask-id/{memberAskId}

    final response = await dio.delete(
        _baseUrl.replaceAll('{memberAskId}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return AskAnswerDetailResult.fromJson(response.data);
  }
}