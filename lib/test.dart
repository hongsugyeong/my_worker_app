import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_worker_app/components/appbar/business/components_appbar_menu.dart';

class Test extends StatefulWidget {
  const Test({super.key});

  @override
  State<Test> createState() => _TestState();
}

class _TestState extends State<Test> {

  File? _image;
  final ImagePicker _picker = ImagePicker();
  final Dio _dio = Dio();

  // 갤러리에서 이미지 선택
  Future<void> _pickImage() async {
    final XFile? pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });
    }
  }

  // 이미지 선택 후 서버에 업로드
  Future<void> _uploadImage() async {
    if (_image != null) {
      String fileName = _image!.path.split('/').last;
      FormData formData = FormData.fromMap({
        "file": await MultipartFile.fromFile(_image!.path, filename: fileName),
      });

      try {
        var response = await _dio.post('http://versuss.store:8080/v1/manual/new/' ,data: formData);
        if (response.statusCode == 200) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('성공')));
        } else {
          throw Exception(Text('실패'));
        }
      } catch (e) {
        print(e);
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('실패')));
      }

    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentsAppbarMenu(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _image != null ? Image.file(_image!) : Text('이미지 선택해주세요.'),
            ElevatedButton(onPressed: _pickImage, child: Text('이미지 선택')),
            ElevatedButton(onPressed: _uploadImage, child: Text('이미지 업로드')),
          ],
        ),
      ),
    );
  }
}
