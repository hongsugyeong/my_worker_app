const double fontSizeLarge = 20;
const double fontSizeMedium = 16;
const double fontSizeXSmall = 15;
const double fontSizeSmall = 13;
const double fontSizeMicro = 9;