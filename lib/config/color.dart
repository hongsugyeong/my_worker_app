import 'dart:ui';

const Color colorDark = Color.fromRGBO(17, 45, 78, 1.0);
const Color colorNormal = Color.fromRGBO(63, 114, 175, 1.0);
const Color colorLight = Color.fromRGBO(219, 226, 239, 1.0);
const Color colorMoreLight = Color.fromRGBO(245, 248, 255, 1.0);