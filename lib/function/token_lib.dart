import 'package:flutter/material.dart';
import 'package:my_worker_app/pages/login/page_login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TokenLib {
  // SharedPreference: 영구저장소

  static Future<String?> getMemberToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberToken');
  }
  static Future<String?> getMemberName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberName');
  }
  static void setMemberToken(String memberToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberToken', memberToken);
  }
  static void setMemberName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

  static void logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.clear();

    // pushAndRemoveUntil: 지금까지 쌓은 거 지우고 페이지 새로 집어넣어라
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => const PageLogin()),
            (route) => false
    );
  }
}
