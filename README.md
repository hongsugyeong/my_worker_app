![header](https://capsule-render.vercel.app/api?type=waving&text=MyWorker&color=0:DBE2EF,100:3F72AF&fontColor=FFFFFF&fontSize=90&fontAlign=70&animation=scaleIn&height=220&fontAlignY=40)

# 내알바야


#### 사장님과 알바생을 아우르는 단 하나의 플랫폼

### 내알바야 주요 기능
* 사업장 관리
* 직원 관리


## 사용 기술
### Android
![AndroidStudio](https://img.shields.io/badge/AndroidStudio-3DDC84?style=for-the-badge&logo=android&logoColor=white)

![Dart](https://img.shields.io/badge/Dart-0175C2?style=for-the-badge&logo=dart&logoColor=white)

![Flutter](https://img.shields.io/badge/Flutter-02569B?style=for-the-badge&logo=flutter&logoColor=white)

### 그 외
![LINUX](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=white)

![docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)

![Springboot](	https://img.shields.io/badge/Springboot-6DB33F?style=for-the-badge&logo=spring&logoColor=white)

![POSTGRESQL](https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white)

![GOOGLE CLOUD](https://img.shields.io/badge/Google_Cloud-4285F4?style=for-the-badge&logo=google-cloud&logoColor=white)


### 사용한 패키지

🗓 [table_calendar](https://pub.dev/packages/table_calendar)

📌 [bottom_picker](https://pub.dev/packages/bottom_picker)

📷 [image_picker](https://pub.dev/packages/image_picker)

📞 [dio](https://pub.dev/packages/dio)

📤 [shared_preferences](https://pub.dev/packages/shared_preferences)

📝 [flutter_form_builder](https://pub.dev/packages/flutter_form_builder)

📑 [intl](https://pub.dev/packages/intl)

📥 [hive_flutter](https://pub.dev/packages/hive_flutter)

🗃 [form_builder_image_picker](https://pub.dev/packages/form_builder_image_picker)

🔄 [liquid_pull_to_refresh](https://pub.dev/packages/liquid_pull_to_refresh)

🍞 [fluttertoast](https://pub.dev/packages/fluttertoast)

📱 [cupertino_icons](https://pub.dev/packages/cupertino_icons)

### 피드백 후 수정 사항
